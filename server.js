const express = require('express');
const next = require('next');
const proxy = require('http-proxy-middleware');
const config = require('./src/config/index');
const routes = require('./src/routes');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({dir: `./src`, dev});
const handle = routes.getRequestHandler(app);

app.prepare().then(() => {
	const server = express();
	server.use('/api', proxy({target: dev ? config.api_dev : config.api_prod, changeOrigin: true, logLevel: 'debug'}));
	server.use(handle);
	server.listen(port, (err) => {
		if (err) throw err
		console.log(`> Ready on http://localhost:${port}`)
	})
});

'use strict';

window.CTEScript = function () {
    var _this = {};

    _this.init = function () {
        _this.fixIE();
        _this.initCustomer();
        _this.initWOW();
        _this.scrollNav();
        _this.initEvents();
    };

    _this.initEvents = function () {
        $('.lrxhclli-menu,.lrxhme-close').click(function (e) {
            e.preventDefault();
            var menu = $('.lrxh-menu-expand');
            if (menu.hasClass('active')) {
                menu.removeClass('active');
            } else {
                menu.addClass('active');
            }
        });

        $('.lrxhmecli-login,.llf-close').click(function (e) {
            e.preventDefault();
            var isShowLogin = $('.luxury-login-form');
            if (isShowLogin.hasClass('active')) {
                isShowLogin.removeClass('active');
                $('body').removeClass('locked');
            } else {
                isShowLogin.addClass('active');
                $('body').addClass('locked');
            }
            $('.lrxh-menu-expand').removeClass('active');
        });

        $('.lrxhmecli > a').click(function (e) {
            var parent = $(this).parent();
            if (parent.has('ul').length) {
                e.preventDefault();
                $(parent.siblings('.active')).find('span').html('+');
                parent.siblings().removeClass('active');
                parent.find('span').html('×');
                parent.addClass('active');
            } else {
                e.preventDefault = false;
            }
        });

        $('.lrxhclli-contact,.contact-form-overlay .overlay-close').click(function (e) {
            e.preventDefault();
            var contact_form = $('.contact-form-overlay');
            if (contact_form.hasClass('active')) {
                contact_form.removeClass('active');
                $('body').removeClass('locked');
            } else {
                contact_form.addClass('active');
                $('body').addClass('locked');
            }
        });
    };

    _this.scrollNav = function () {
        $(window).scroll(function () {
            var currentOffset = $(window).scrollTop();
            if (!$(".lrx-header").hasClass('isSticky')) {
                if (currentOffset > 80) {
                    $(".lrx-header").addClass("sticky");
                } else {
                    $(".lrx-header").removeClass("sticky");
                }
            }
        });
    };

    _this.initWOW = function () {
        var wow = new WOW({
            boxClass: 'wow', // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 0, // distance to the element when triggering the animation (default is 0)
            mobile: true // trigger animations on mobile devices (default is true)
        });
        wow.init();
    };

    _this.utils = {
        isMobile: function isMobile(agent) {
            return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(agent || window.navigator.userAgent)
            );
        }
    };

    _this.initMap = function () {
        var mapId = 'js-cte-map';
        if ($('#' + mapId).length === 0) {
            return false;
        }

        var isDraggable = _this.utils.isMobile();

        var myLatlng = new google.maps.LatLng(21.017958, 105.849658);
        var mapOptions = {
            zoom: 17,
            center: myLatlng,
            disableDefaultUI: false,
            scrollwheel: false,
            //draggable: isDraggable,
            scaleControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById(mapId), mapOptions);

        var contentString = '<div class="cte-info-window"><img src="./assets/images/logo-primary.png"/><div class="adr">Số 21 lô 1B  Trung Yên 11, Phường Trung Hoà, Quận Cầu Giấy, Hà Nội</div></div>';

        var infowindow = new google.maps.InfoWindow({ content: contentString });

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(21.016958, 105.849658),
            map: map,
            icon: './assets/images/location.png'
        });

        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

        infowindow.open(map, marker);

        marker.setMap(map);
    };

    _this.initCustomer = function () {
        var $container = $('#js-cte-home-customer');

        var $slider = $container.find('.swiper-container');

        var swiper = new Swiper($slider, {
            nextButton: $container.find('.swiper-button-next'),
            prevButton: $container.find('.swiper-button-prev'),
            spaceBetween: 32,
            slidesPerView: 2,
            loop: true,
            breakpoints: {
                1230: {
                    slidesPerView: 2
                },
                992: {
                    slidesPerView: 2
                },
                768: {
                    slidesPerView: 1.5
                },
                640: {
                    slidesPerView: 1.5,
                    spaceBetween: 16
                },
                480: {
                    slidesPerView: 1.2,
                    spaceBetween: 10
                }
            }
        });
    };

    _this.fixIE = function () {
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement('style');
            msViewportStyle.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}'));
            document.querySelector('head').appendChild(msViewportStyle);
        }
    };

    return _this;
}();

$('document').ready(function () {
    window.CTEScript.init();
});
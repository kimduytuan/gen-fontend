const routes = require('next-routes')();

//
// Because of awesome Next.js, You don't need to add routes for all pages.
// Every file on Pages folder basically has route as they named.
// (index.js => /, about.js => /about, ...etc.)
//
// If you want to change url (for SEO or put different path), please add your route below.
// for more info, please look at https://github.com/Sly777/ran/blob/master/docs/Routing.md
//
//
// Please add your route between of comments
//
// ------------ ROUTES ---------------
routes.add('index', '/');
routes.add('invitation/invitationError', '/invitation');
routes.add('signUp/signUp', '/signUp');
routes.add('signUp/email', '/email');
routes.add('login/index', '/login');

// ------------ ROUTES ---------------
//
//

module.exports = routes;
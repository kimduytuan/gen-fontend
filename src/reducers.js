import {combineReducers} from 'redux';

import Properties from "./containers/_Properties/reducers";
import PropertiesMap from "./containers/_Map/reducers";
import Associates from "./containers/_Associates/reducers";
import Blog from "./containers/_Blog/reducers";

export default combineReducers({
	Properties: Properties,
	Associates: Associates,
	Blog : Blog,
	PropertiesMap: PropertiesMap
})

import _ from "lodash";
import Configs from "../config/index";

export function getImage(_path, _width = null, _height = null) {
	if (_path) {
		if (/^(f|ht)tps?:\/\//i.test(_path)) return _path;
		let __width = '';
		let __height = '';

		if (!_.isNull(_width)) {
			__width = `/w${_width}`;
		}
		if (!_.isNull(_height)) {
			__height = `/h${_height}`;
		}

		return `${Configs.image}${__width}${__height}/!${_path}`;
	}
}



import { all } from "redux-saga/effects";

import propertiesSagas from "./containers/_Properties/sagas";
import propertiesMapSagas from "./containers/_Map/sagas";
import associatesSagas from "./containers/_Associates/sagas";
import blogSagas from "./containers/_Blog/sagas";

export default function* rootSaga(services = {}) {
	yield all([propertiesSagas(), associatesSagas(), blogSagas(), propertiesMapSagas()]);
}

import {filterGet, request} from "./request";
import Config from "../config/index";
import _ from "lodash";

export default {

	register: (userName, name, email, password) => {
		return request(`${Config.api}/api/Users`, null, {
			method: "POST",
			body: JSON.stringify({
				'username': userName,
				'name': name,
				'email': email,
				'password': password
			})
		})
	},

	getHome: () => {
		return request(`${Config.api}/api/home`, {
				method: "GET"
			}
		);
	},

	getListOurStory: () => {
		return request(`${Config.api}/api/Configs`, {
			method: "GET"
		})
	},

	getListAssociates: (_page) => {
		let _data = filterGet({page: _page});
		return request(`${Config.api}/api/Associates${_data}`, {
			method: "GET",
		})
	},

	getDetailAssociates: (_slug) => {
		let _data = filterGet({where:{slug:_slug}});
		return request(`${Config.api}/api/Associates${_data}`, {
			method: "GET",
		})
	},
	getProductAssociates: (_id) => {
		return request(`${Config.api}/api/Associates/${_id}/products`, {
			method: "GET",
		})
	},

	getListVideo: () => {
		let data = filterGet({where: {video: {neq: null}}});
		return request(`${Config.api}/api/Products/${data}`, null, {
				method: "GET"
			}
		);
	},

	getListBlog: (_page) => {
		let _data = filterGet({page: _page});
		return request(`${Config.api}/api/Articles${_data}`, {
			method: "GET"
		})
	},

	getDetailBlogs: (_slug) => {
		let _data = filterGet({where:{slug:_slug}});
		return request(`${Config.api}/api/Articles${_data}`, {
			method: "GET",
		});
	},

	getListProperties: (_slug, _page) => {
		let data = filterGet({where: {slugEn: _slug}, page: _page});
		return request(`${Config.api}/api/Products/categories${data}`, null, {
				method: "GET"
			}
		);
	},

	getDetailProperties: (_id) => {
		let data = filterGet({include: ["associates", "creator", "area_rls"]});
		return request(`${Config.api}/api/Products/${_id}${data}`, {
				method: "GET"
			}
		);
	},

//Category + Neighborhood (Area) + lifeStyle
	getCategory: () => {
		return request(`${Config.api}/api/Categories`, {
				method: "GET"
			}
		);
	},

	getArea: () => {
		return request(`${Config.api}/api/Areas`, {
				method: "GET"
			}
		);
	},

	getLifeStyles: () => {
		return request(`${Config.api}/api/LifeStyles`, {
				method: "GET"
			}
		);
	},

	getCategoryFilter: () => {
		return request(`${Config.api}/api/categories/filter`, {
				method: "GET"
			}
		);
	},

//Search product by option id
	searchProperties: (_neighbhorhood = null, _lifeStyle = null, _price = null, _houseSize = null) => {
		let price = [0, 999999999999999999];
		if (_price == 1) {
			price = [100, 1000];
		} else if (_price == 2) {
			price = [1000, 10000];
		} else if (_price == 3) {
			price = [10000, 50000];
		} else if (_price == 4) {
			price = [50000, 100000];
		} else if (_price == 5) {
			price = [100000, 200000];
		} else if (_price == 6) {
			price = [200000, 300000];
		} else if (_price == 7) {
			price = [300000, 400000];
		} else if (_price == 8) {
			price = [400000, 500000];
		} else if (_price == 9) {
			price = [500000, 1000000];
		} else if (_price == 10) {
			price = [1000000, 9000000000000000000];
		}
		let houseSize = [0, 999999999999999999];
		if (_houseSize == 1) {
			price = [50, 100];
		} else if (_houseSize == 2) {
			houseSize = [100, 200];
		} else if (_houseSize == 3) {
			houseSize = [200, 300];
		} else if (_houseSize == 4) {
			houseSize = [300, 400];
		} else if (_houseSize == 5) {
			houseSize = [400, 500];
		} else if (_houseSize == 6) {
			houseSize = [500, 600];
		} else if (_houseSize == 7) {
			houseSize = [600, 700];
		} else if (_houseSize == 8) {
			houseSize = [700, 800];
		} else if (_houseSize == 9) {
			houseSize = [800, 1000];
		} else if (_houseSize == 10) {
			price = [1000, 9000000000000000000];
		}

		let param = Object.assign({}, {
			price: parseInt(_price) ? {between: price} : undefined,
			house_size: parseInt(_houseSize) ? {between: houseSize} : undefined,
			neighbhorhood: parseInt(_neighbhorhood) ? _neighbhorhood : undefined,
			life_style: parseInt(_lifeStyle) ? _lifeStyle : undefined,
		});
		let _param = _.pickBy(param, _.identity);

		if (parseInt(_neighbhorhood) || parseInt(_lifeStyle) || parseInt(_price) || parseInt(_houseSize)) {
			let data = filterGet({where: _param});
			return request(`${Config.api}/api/Products/${data}`, {
					method: "GET"
				}
			);
		} else {
			let data = filterGet({where: {slugEn: "sale"}, page: 1});
			return request(`${Config.api}/api/Products/categories${data}`, null, {
					method: "GET"
				}
			);
		}


	},

	//Search map
	getListPropertiesMap: (_idArea, _page) => {
		let data = filterGet({"where":{"neighbhorhood":_idArea}, "page": _page});
		return request(`${Config.api}/api/Products/${data}`, null, {
				method: "GET"
			}
		);
	},

	getUserByPage: (_token, _page) => {
		let _data = filterGet({page: _page});
		return request(`${Config.api}/api/Users${_data}`, null, {
			method: "GET"
		})
	},
	deleteUserById: (_id) => {
		return request(`${Config.api}/api/Users/${_id}`, null, {
			method: "DELETE"
		})
	},
	createUser: (_data) => {
		let intinital = {
			"name": "string",
			"email": "234234242324@gmail.com",
			"userType": 0,
			"password": "123456",
			"rememberToken": "string",
			"thumbnail": "string",
			"description": "string",
			"status": 1,
			"realm": "string"
		};

		return request(`${Config.api}/api/Users`, null, {
			method: "post",
			body: JSON.stringify({...intinital, ..._data})
		})
	},
	editUser: (id, _data) => {
		const only = ['description', 'name', 'facebookid', 'status']

		_data = _.pick(_data, only);

		return request(`${Config.api}/api/Users/${id}`, null, {
			method: "PATCH",
			body: JSON.stringify(_data)
		})
	},
	getUserById: (id, token) => {
		return request(`${Config.api}/api/Users/${id}`, token, {
			method: "GET"
		})
	}
}

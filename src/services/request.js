import 'isomorphic-fetch'
function filterGet(_filter) {
	return `?filter=${encodeURIComponent(JSON.stringify(_filter))}`
}

function request(url, token, options) {
	const headers = {
		'Accept': 'application/json',
		'Content-Type': 'application/json'
	};

	if (token) {
		headers['Authorization'] = `Bearer ${token}`
	} else if (typeof localStorage !== "undefined") {
		if (localStorage.getItem('token')) {
			headers['Authorization'] = `Bearer ${localStorage.getItem('token')}`
		}
	}

	return fetch(url, {
		headers,
		...options
	}).then(response => response.json())
		.then(data => data)
}

export {filterGet, request};

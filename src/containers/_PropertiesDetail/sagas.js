import {call, fork, put, takeLatest} from "redux-saga/effects";
import Services from "../../services/req_";
import {LOAD_PROPERTIES, LOAD_PROPERTIES_FAIL, LOADED_PROPERTIES} from "./constants";

function* fetchProperties(action) {
	try {
		const {slug, page} = action;
		const properties = yield call(Services.getListProperties, slug, page);
		yield put({type: LOADED_PROPERTIES, list: properties.response, page: page});
	} catch (err) {
		yield  put({type: LOAD_PROPERTIES_FAIL});
	}
}

function* watchFetchProperties() {
	yield takeLatest(LOAD_PROPERTIES, fetchProperties);
}

export default function* propertiesSagas() {
	yield fork(watchFetchProperties);
}


import {LOAD_PROPERTIES, LOADED_PROPERTIES} from './constants'

// The initial state of the App
const initialState = {
	loading: false,
	error: false,
	currentPage: 1,
	list: []
};

export default function (state = initialState, action) {
	const {type, list, page, total} = action
	switch (type) {
		case LOAD_PROPERTIES:
			return {...state}
		case LOADED_PROPERTIES:
			return {
				...state,
				list: [...state.list, ...list],
				loading: true,
				currentPage: page
			};
		default:
			return state
	}

}

import React from "react";

import Item from "../../components/Properties/ItemProperty";

export default class List extends React.Component {
	render() {
		const list = this.props.data ? this.props.data : [];
		const slug = this.props.slug ? this.props.slug : [];
		return (
			<div className="lrx-list-properties-wrapper">
				<div className="container">
					<div className="lrxlpw-content">
						<ul className="lrxlpwc-list">
							{list.length ? list.map((data, Index) => <Item data={data} slug ={slug} key={Index}/>) : <center>data null</center>}
						</ul>
					</div>
				</div>
			</div>
		);
	}
}

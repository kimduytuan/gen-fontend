import {call, fork, put, takeLatest, select} from "redux-saga/effects";
import Services from '../../services/req_';
import {LOAD_BLOG, LOAD_BLOG_SUCCESS, LOAD_BLOG_FAIL} from "./constants";

function* fetchBlog(action) {
	try {
	const {page} = action;
		const properties = yield call(Services.getListBlog, page);
		yield put({type: LOAD_BLOG_SUCCESS, list: properties.response, total: properties.meta.total, page: page});
	} catch (err) {
		yield  put({type: LOAD_BLOG_FAIL});
	}
}

function* watchFetchBlog() {
	yield takeLatest(LOAD_BLOG, fetchBlog);
}

export default function* blogSagas() {
	yield fork(watchFetchBlog);
}


import {LOAD_BLOG, LOAD_BLOG_SUCCESS} from './constants'

// The initial state of the App
const initialState = {
	loading: false,
	error: false,
	currentPage: 1,
	total: 0,
	list: []
};

export default function (state = initialState, action) {
	const {type, list, page, total} = action
	switch (type) {
		case LOAD_BLOG:
			return {...state}
		case LOAD_BLOG_SUCCESS:
			return {
				...state,
				list: [...state.list, ...list],
				total: total,
				loading: true,
				currentPage: page
			};
		default:
			return state
	}

}

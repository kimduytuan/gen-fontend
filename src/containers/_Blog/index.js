import React from "react";
import Item from '../../components/Blog/Item';

export default class List extends React.Component {
	render() {
		const list = this.props.data ? this.props.data : [];
		return (
			<div className="blog-wrap">
				<div className="bw-list">
					<div className="container">
						<ul className="bwl-wrap">
							{list.length ? list.map((data, Index) => <Item data={data} key={Index}/>) : <center>data null</center>}
						</ul>
						<div className="bwlw-next">
							<a className="bwlwn-pre" href>&lt; Previous</a>
							<a className="bwlwn-next" href>next &gt;</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

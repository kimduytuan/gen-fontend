import {LOAD_BLOG, LOAD_BLOG_SUCCESS} from './constants'


export function loadBlog(page) {
	return {
		type: LOAD_BLOG,
		page: page || 1,
		action: 1
	}
}

export function loadedBlog(page) {
	return {
		type: LOAD_BLOG_SUCCESS,
		list: [],
		total: 0,
		page: 0,
	}
}


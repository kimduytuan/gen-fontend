import {call, fork, put, takeLatest, select} from "redux-saga/effects";
import Services from '../../services/req_';
import {LOAD_ASSOCIATES, LOAD_ASSOCIATES_SUCCESS, LOAD_ASSOCIATES_FAIL} from "./constants";

function* fetchAssociates(action) {
	try {
	const {page} = action;
		const properties = yield call(Services.getListAssociates, page);
		yield put({type: LOAD_ASSOCIATES_SUCCESS, list: properties.response, total: properties.meta.total, page: page});
	} catch (err) {
		yield  put({type: LOAD_ASSOCIATES_FAIL});
	}
}

function* watchFetchAssociates() {
	yield takeLatest(LOAD_ASSOCIATES, fetchAssociates);
}

export default function* associatesSagas() {
	yield fork(watchFetchAssociates);
}


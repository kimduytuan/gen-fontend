import {LOAD_ASSOCIATES, LOAD_ASSOCIATES_SUCCESS} from './constants'


export function loadAssociates(page) {
	return {
		type: LOAD_ASSOCIATES,
		page: page || 1,
		action: 1
	}
}

export function loadedAssociates(page) {
	return {
		type: LOAD_ASSOCIATES_SUCCESS,
		list: [],
		total: 0,
		page: 0,
	}
}


import React from 'react';
import Item from "../../components/Associates/item";

export default class List extends React.Component {

	render(){
		const list = this.props.data ? this.props.data : [];
		return(
			<div className="lrx-associates-wrapper">
				<div className="lassw-content">
					<div className="container">
						<ul className="lasswc-list">
							{list.length && list.map((data, Index) => <Item data={data} key={Index}/>)}
						</ul>
					</div>
				</div>
			</div>
		);
	}
}

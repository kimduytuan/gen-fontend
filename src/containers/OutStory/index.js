import React from "react";
export  default class OutStory extends React.Component {

	render() {
		return (
			<div className="out-story-wrap">
				<div className="container container-content">
					<div className="content-text">
						<img className="ct-img img-responsive" src="https://res.cloudinary.com/luxuryp/image/upload/v1512714256/ugn0o6uufkn9bi0i3f0h.png" alt="" />
						<p>
							Partners Rick Hilton and Jeff Hyland established the firm in 1993 with the intention of creating a small
							real estate boutique that would grow to meet the demands of the market, yet never outgrow a founding
							philosophy for meeting the uniquely individual concerns of each client. Together, they bring a dynamic
							and diverse range of resources to the firm. So do some 90 Hilton &amp; Hyland agents,
							each chosen for their considerable accomplishments, as well as discerning ethics, in the marketing,
							purchase, and sale of luxury real estate.
						</p>
						<p>
							Rick Hilton, whose grandfather founded the Hilton Hotel empire, has vast experience in residential sales
							and commercial financing and sales, as well as real estate development. As a principal, he developed
							Brentwood Country Estates, a 14-parcel, gated-guarded enclave in prestigious Mandeville Canyon. Jeff
							Hyland, a veteran broker and celebrated architectural historian,
							is a past president of the Beverly Hills Board of Realtors as well as the 30,000 member Los Angeles
							County Boards of Real Estate.
						</p>
						<p>
							Partners Rick Hilton and Jeff Hyland established the firm in 1993 with the intention of creating a small
							real estate boutique that would grow to meet the demands of the market, yet never outgrow a founding
							philosophy for meeting the uniquely individual concerns of each client. Together, they bring a dynamic
							and diverse range of resources to the firm. So do some 90 Hilton &amp; Hyland agents,
							each chosen for their considerable accomplishments, as well as discerning ethics, in the marketing,
							purchase, and sale of luxury real estate.
						</p>
						<p>
							Rick Hilton, whose grandfather founded the Hilton Hotel empire, has vast experience in residential sales
							and commercial financing and sales, as well as real estate development. As a principal, he developed
							Brentwood Country Estates, a 14-parcel, gated-guarded enclave in prestigious Mandeville Canyon. Jeff
							Hyland, a veteran broker and celebrated architectural historian,
							is a past president of the Beverly Hills Board of Realtors as well as the 30,000 member Los Angeles
							County Boards of Real Estate.
						</p>
					</div>
				</div>
			</div>
		);
	}
}

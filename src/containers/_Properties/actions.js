import {LOAD_PROPERTIES, LOADED_PROPERTIES} from './constants'


export function loadProperties(slug, page) {
	return {
		slug: slug,
		type: LOAD_PROPERTIES,
		page: page || 1,
		action: 1
	}
}

export function loadedProperties(page) {
	return {
		type: LOADED_PROPERTIES,
		list: [],
		page: 0,
	}
}


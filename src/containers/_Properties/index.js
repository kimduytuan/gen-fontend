import React from "react";

import Item from "../../components/Properties/ItemProperty";

export default class List extends React.Component {
	constructor() {
		super();
		this.state = {list: []}
	}
	componentDidMount() {
		this.setState({list: this.props.data ? this.props.data : []})
	}

	render() {
		const slug = this.props.slug ? this.props.slug : [];
		return (
			<div className="lrx-list-properties-wrapper">
				<div className="container">
					<div className="lrxlpw-content">
						<ul className="lrxlpwc-list">
							{this.state.list.length ? this.state.list.map((data, Index) => <Item data={data ? data : []} slug={slug}
																																									 key={Index}/>) :
								<center>No data, please choose any category !</center>}
						</ul>
					</div>
				</div>
			</div>
		);
	}
}

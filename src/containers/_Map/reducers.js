import {LOAD_PROPERTIES_MAP_SUCCESS, LOAD_PROPERTIES_MAP} from './constants'

// The initial state of the App
const initialState = {
	loading: false,
	error: false,
	currentPage: 1,
	list: [],
	total: 1
};

export default function (state = initialState, action) {
	const {type, list, page, total} = action
	switch (type) {
		case LOAD_PROPERTIES_MAP:
			return {...state};
		case LOAD_PROPERTIES_MAP_SUCCESS:
			return {
				...state,
				list: list,
				loading: true,
				currentPage: page,
				total: total
			};
		default:
			return state
	}
}

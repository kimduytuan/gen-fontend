import React from "react";
import Item from '../../components/Maps/Item';
import config from '../../config/index';
import mapStyle from '../../static/googleMapStyleSiver';
import Option from "../../components/Properties/optionSearch";
import GoogleMapIndex from "../../components/GoogleMap/index";
import InputRange from 'react-input-range';



export default class MapClass extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isHover: null,
			areaId:  null,
			classImg: "",
			flexMap : 1,
			value5: {min: 0, max: 20},
			classRange: 'none',
		}
	}

	render() {

		let list = this.props.data ? this.props.data : [];
		let area = this.props.area && this.props.area.response? this.props.area.response : [];
		return (
			<div className="lrx-map-properties-wrapper">
				<div className="lrxmp-content">
					<div className="lrxmpc-filter">
						<div className="lrxmpcf-filter-wrapper">
							<div className="lfw-content">
								<div className="lfwc-search">
										<img src="/static/images/search.png"/>
									<select className="selectric" title="" style={{height: 35, outline: 'none'}} onChange={(e) => {
										let value = e.target.value;
										this.setState({pageId: value});
										this.props.self.props.loadPropertiesMap(value);
									}}>
										<option value={0}>Any Neighborhood ?</option>
										{area.length ?
											area.map((data) => {
												return (<Option area={data} type="area" key={data.id}/>);
											}) : null
										}
									</select>
								</div>
								<div className="lfwc-filter">
									<ul className="lfwcf-list">
										<li className="lfwcfli" onClick={()=>{
											this.setState(showRange => ({classRange: showRange.classRange === 'block' ? 'none' : 'block'}));
										}}>
											<a href="javascript:void(0)" name="volume">price range</a>
											<div style={{display: this.state.classRange}}>
												<InputRange
																		draggableTrack
																		maxValue={20}
																		minValue={0}
																		formatLabel={value => `${value} M`}
																		onChange={value => this.setState({ value5: value })}
																		onChangeComplete={value => console.log(value)}
																		value={this.state.value5} />
											</div>
											
										</li>
										<li className="lfwcfli">
											<select className="selectric-search" title="" onChange={(e) => {
												let value = e.target.value;
												this.setState({bed: value});
												this.props.self.props.loadPropertiesMap(this.state.areaId);
											}}>
												<option value={1}>1+ BEDS</option>
												<option value={2}>2+ BEDS</option>
												<option value={3}>3+ BEDS</option>
												<option value={4}>4+ BEDS</option>
												<option value={5}>5+ BEDS</option>

											</select>
										</li>
										<li className="lfwcfli">
											<select className="selectric-search" title="" onChange={(e) => {
												let value = e.target.value;
												this.setState({bath: value});
												this.props.self.props.loadPropertiesMap(this.state.areaId);
											}}>
												<option value={1}>1+ BATH</option>
												<option value={2}>2+ BATH</option>
												<option value={3}>3+ BATH</option>
												<option value={4}>4+ BATH</option>
												<option value={5}>5+ BATH</option>
											</select>
										</li>
										<li className="lfwcfli">
											<a href="javascript:void(0)">Living Area</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div className="lrxmpcf-picker-wrapper">
							<div className="lpw-content">
								<a href="javascript:void(0)" className="lpw-grid" onClick={()=> {
									this.setState(prevState => ({flexMap: prevState.flexMap === 1 ? 0 : 1}));
								}

								} />
								<a href="javascript:void(0)" className="lpw-location" />
							</div>
						</div>
					</div>
					<div className="lrxmpc-content">
						<div className="lrxmpcc-left">
							<div className="lrxmpccl-top">
								<div className="lrmpcclt-total">{this.props.total ? this.props.total : 0} Properties</div>
								<div className="lrmpcclt-action">
									<div className="lrmpcclta-content">
										{/*<select className="lrmpcclt-action">
											<option>Price</option>
											<option>Create</option>
										</select>*/}
										<a href="javascript:void(0)" className="ldpwar-save">Save Search</a>
									</div>
								</div>
							</div>
							<div className="lrxmpccl-bottom">
								<div className="lrxlpw-content">
									<ul className="lrxlpwc-list">
										{list.length ?
											list.map((data, Index) => <Item data={data ? data : []} key={Index} flexMap={this.state.flexMap === 1 ? 'calc(50% - 27px) !important' : 'calc(25% - 27px) !important'} self={this}/>)
											:
											null}
									</ul>
								</div>
							</div>
							<div className="pagination">
								<div className="page-numbers">
									<div className="compact-holder"><span data-reactid="412">Page </span><span>1</span><span> of </span><span>497</span>
									</div>
								</div>
								{/*<div className="fill-space"/>*/}
								<div className="nav-right">
									<img src="//s3-us-west-2.amazonaws.com/static-lp/images/hilton/next-black2.svg" data-reactid="420" />
								</div>
								<div className="nav-left disabled" disabled="" data-reactid="417">
									<img src="//s3-us-west-2.amazonaws.com/static-lp/images/hilton/prev-black2.svg" data-reactid="418" />
								</div>
							</div>
						</div>
						<div className="lrxmpcc-right" style={{flex: this.state.flexMap}}>
							<GoogleMapIndex markers={list} isHover={this.state.isHover}/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

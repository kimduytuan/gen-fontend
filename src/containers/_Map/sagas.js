import {call, fork, put, takeLatest} from "redux-saga/effects";
import Services from "../../services/req_";
import {LOAD_PROPERTIES_MAP, LOAD_PROPERTIES_MAP_FAIL, LOAD_PROPERTIES_MAP_SUCCESS} from "./constants";

function* fetchPropertiesMap(action) {
	try {
		const {firstAreaId, page} = action;
		const propertiesMap = yield call(Services.getListPropertiesMap, firstAreaId, page);
		yield put({type: LOAD_PROPERTIES_MAP_SUCCESS, list: propertiesMap.response, page: page, total: propertiesMap.meta.total});
	} catch (err) {
		yield  put({type: LOAD_PROPERTIES_MAP_FAIL});
	}
}

function* watchFetchPropertiesMap() {
	yield takeLatest(LOAD_PROPERTIES_MAP, fetchPropertiesMap);
}

export default function* propertiesMapSagas() {
	yield fork(watchFetchPropertiesMap);
}


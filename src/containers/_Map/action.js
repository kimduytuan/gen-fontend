import {LOAD_PROPERTIES_MAP, LOAD_PROPERTIES_MAP_SUCCESS} from './constants'


export function loadPropertiesMap(firstAreaId, page) {
	return {
		firstAreaId: firstAreaId,
		type: LOAD_PROPERTIES_MAP,
		page: page || 1,
		action: 1
	}
}

export function loadedPropertiesMap() {
	return {
		type: LOAD_PROPERTIES_MAP_SUCCESS,
		list: [],
		page: 0,
		total: 0,
	}
}


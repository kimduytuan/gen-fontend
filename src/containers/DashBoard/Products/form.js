import React from 'react'
import {withFormik} from 'formik';
import Yup from 'yup';
import Panel from "../../components/panel/index"
import TextInput from "../../components/Forms/Input"
import SelectInput from "../../components/Forms/Select"
import Service from "./service"
import {notify} from "react-notify-toast"
import Router from "next/router"
import _ from "lodash-uuid"

const UserEdit = props => {
	const {
		values,
		touched,
		errors,
		status,
		dirty,
		handleChange,
		handleBlur,
		handleSubmit,
		handleReset,
		isSubmitting,
		setFieldValue,
		setFieldTouched,
		setErrors
	} = props;

	return (
		<div className="row">
			<div className="col-md-6 col-md-push-3">
				<Panel title={props.title_panel} success={!!status && !status.success}
							 error={!!errors.submit} isSubmiting={isSubmitting}>
					<form onSubmit={handleSubmit}>
						<fieldset>
							<legend className="text-semibold">Enter your information</legend>
							<TextInput
								id="username"
								className="form-control"
								type="text"
								label="Enter your username:"
								placeholder="Microvn"
								disable={!!values.id}
								error={touched.username && errors.username}
								value={values.username}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
							<TextInput
								id="name"
								className="form-control"
								type="text"
								label="Enter your name:"
								placeholder="John"
								error={touched.name && errors.name}
								value={values.name}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
							<TextInput
								id="password"
								className="form-control"
								type="password"
								label="Enter your password:"
								placeholder="****"
								value={values.password}
								error={touched.password && errors.password}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
							<TextInput
								id="description"
								className="form-control"
								type="text"
								label="Enter your description:"
								placeholder="Description"
								error={touched.description && errors.description}
								value={values.description}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
							<TextInput
								id="facebookId"
								className="form-control"
								type="text"
								label="Enter your facebookid:"
								placeholder="234234242324"
								error={touched.facebookid && errors.facebookid}
								value={values.facebookid}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
							<TextInput
								id="email"
								className="form-control"
								type="text"
								disable={!!values.id}
								label="Enter your email:"
								placeholder="John"
								error={touched.email && errors.email}
								value={values.email}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
							<SelectInput
								id="status"
								value={values.status}
								multi={false}
								onChange={setFieldValue}
								label="Select for status:"
								onBlur={setFieldTouched}
								error={touched.status && errors.status}
								options={[
									{value: '1', label: 'Active'},
									{value: '0', label: 'UnActive'}
								]}
								getValue={true}
								selectedValue={values.status}
								touched={touched.status}
							/>
							{
								values.id !== "" ? (<input type="hidden" value={values.id}/>) : null
							}
						</fieldset>
						<div className="text-right">
							<button disabled={isSubmitting} type="submit" className="btn btn-primary">Submit form<i
								className="icon-arrow-right14 position-right"/></button>
						</div>
					</form>
				</Panel>
			</div>
		</div>
	);
};


const MyEnhancedForm = withFormik({
	validationSchema: Yup.object().shape({
		name: Yup.string()
			.min(3, "Name is longer than that")
			.required('Name is required.'),
		email: Yup.string()
			.email('Invalid email address')
			.required('Email is required!'),
	}),
	mapPropsToValues: (props) => (props.init),
	handleSubmit: async (payload, {props, values, setSubmitting, setStatus, resetForm, setErrors}) => {
		console.log(payload);
		console.log(_.isUuid(payload.id))
		if (_.isUuid(payload.id)) {
			let response = await Service.editUser(payload.id, payload);
			if (response.error.statusCode !== 200) {
				notify.show(response.error.message, 'error');
				setStatus({success: false})
			} else {
				setStatus({success: true})
				resetForm({
					fullname: '', email: ''
				})
				Router.back();
			}
		} else {
			let response = await Service.createUser(payload);
			if (response.error.statusCode !== 200) {
				notify.show(response.error.message, 'error');
				setStatus({success: false})
			} else {
				setStatus({success: true})
				resetForm({
					fullname: '', email: ''
				})
				Router.back();
			}
		}
		setSubmitting(false);

	},
	displayName: 'EditUser',
})(UserEdit);

export default MyEnhancedForm

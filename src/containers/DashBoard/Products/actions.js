import {ADD_TODO, REMOVE_TODO, TODOS} from './constants'


export function Todo(token) {
	return {
		type: TODOS,
		token:token
	}
}

export function addTodo(text) {
	return {
		type: ADD_TODO,
		payload: id
	}
}

export function removeTodo(id) {
	return {
		type: REMOVE_TODO,
		payload: id
	}
}

import {ADD_TODO, REMOVE_TODO, TODOS} from './constants'

// The initial state of the App
const initialState = {
	loading: false,
	error: false,
	currentUser: false,
	list: []
};

export default function (state = initialState, action) {
	const {type, payload, todo} = action
	switch (type) {
		case TODOS:
			return {...state, loading: false}
		case ADD_TODO:
			return {...state, list: payload, loading: true}
		case REMOVE_TODO:
			return {...state, list: state.list.filter(i => i.id !== payload)}
		default:
			return state
	}

}

import Request from '../../services/request'
import Config from '../../config'
import _ from 'lodash'

export default {
	createUser: (_data) => {
		let intinital = {
			"name": "string",
			"email": "234234242324@gmail.com",
			"userType": 0,
			"password": "123456",
			"rememberToken": "string",
			"thumbnail": "string",
			"description": "string",
			"status": 1,
			"realm": "string"
		};

		return Request(`${Config.api}/api/Users`, null, {
			method: "post",
			body: JSON.stringify({...intinital, ..._data})
		})
	},
	editUser: (id, _data) => {
		const only = ['description', 'name', 'facebookid', 'status']

		_data = _.pick(_data, only);

		return Request(`${Config.api}/api/Users/${id}`, null, {
			method: "PATCH",
			body: JSON.stringify(_data)
		})
	},
	getUserById: (id, token) => {
		return Request(`${Config.api}/api/Users/${id}`, token, {
			method: "GET"
		})
	}
}

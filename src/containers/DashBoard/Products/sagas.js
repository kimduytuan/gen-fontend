import {call, fork, put, takeLatest} from "redux-saga/effects";
import request from '../../services/request';
import {ADD_TODO, TODOS, REMOVE_TODO} from "./constants";

function* fetchPosts(action) {
	const posts = yield call(request, 'http://localhost:3000/api/Users', action.token);
	console.log(posts)
	yield put({type: ADD_TODO, payload: posts.response});
}

function* deleteUser(action) {
	const posts = yield call(request, `http://localhost:3000/api/Users/${action.payload}`, null, {method: 'DELETE'});
}


function* watchFetchPosts() {
	yield takeLatest(TODOS, fetchPosts);
}

function* watchDeleteUser() {
	yield takeLatest(REMOVE_TODO, deleteUser);
}

export default function* postsSagas() {
	yield fork(watchFetchPosts);
	yield fork(watchDeleteUser);
}


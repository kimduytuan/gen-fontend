import React from 'react'
import Panel from "../../components/panel/index"
// import {Link} from '../../routes'
import Link from 'next/link'

const Index = ({handleClickDelete, data, title}) => (
	<div className="row">
		<div className="col-md-12">
			<Panel title="Small table" action={[{"add": "/users/create"}]}>
				{title}
				<table className="table table-sm">
					<thead>
					<tr>
						<th>#</th>
						<th>FullName</th>
						<th>Email</th>
						<th>Type</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					{
						data && data.map((item, index) => {
							return (
								<tr key={item.id}>
									<td>{index}</td>
									<td>{item.name}</td>
									<td>{item.email}</td>
									<td><span
										className={item.type === 1 ? "label label-danger" : "label label-success"}>{item.status === 1 ? "admin" : "normal"}</span>
									</td>
									<td><a href="/news/status/"
												 className={item.status === 1 ? "label label-success" : "label label-danger"}>{item.status === 1 ? "Kích Hoạt" : "Khóa"}</a>
									</td>
									<td>
										<div className="btn-group">
											<Link prefetch href={`/users/edit?id=${item.id}`} as={`/users/edit/${item.id}`}>
												<a className="label label-info">Edit</a>
											</Link>
											<a onClick={handleClickDelete(item.id)}
												 className="label label-danger">Delete</a>
										</div>
									</td>
								</tr>
							)
						})
					}
					</tbody>
				</table>
			</Panel>
		</div>
	</div>
)

export default Index

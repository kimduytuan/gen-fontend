import React from 'react'

const Index = ({stars}) => (
	<div className="row">
		<div className="col-lg-7">
			<div className="panel panel-flat">
				<div className="panel-heading">
					<h6 className="panel-title">Traffic sources</h6>
					<div className="heading-elements">
						<form className="heading-form" action="#">
							<div className="form-group">
								<label className="checkbox-inline checkbox-switchery checkbox-right switchery-xs">
									<input type="checkbox" className="switch" />
									Live update:
								</label>
							</div>
						</form>
					</div>
				</div>

				<div className="container-fluid">
					<div className="row">
						<div className="col-lg-4">
							<ul className="list-inline text-center">
								<li>
									<a href="javascript:void(0)"
										 className="btn border-teal text-teal btn-flat btn-rounded btn-icon btn-xs valign-text-bottom">
										<i className="icon-plus3"/>
									</a>
								</li>
								<li className="text-left">
									<div className="text-semibold">New visitors</div>
									<div className="text-muted">2,349 avg</div>
								</li>
							</ul>

							<div className="col-lg-10 col-lg-offset-1">
								<div className="content-group" id="new-visitors"></div>
							</div>
						</div>

						<div className="col-lg-4">
							<ul className="list-inline text-center">
								<li>
									<a href="javascript:void(0)"
										 className="btn border-warning-400 text-warning-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom">
										<i className="icon-watch2"/></a>
								</li>
								<li className="text-left">
									<div className="text-semibold">New sessions</div>
									<div className="text-muted">08:20 avg</div>
								</li>
							</ul>

							<div className="col-lg-10 col-lg-offset-1">
								<div className="content-group" id="new-sessions"></div>
							</div>
						</div>

						<div className="col-lg-4">
							<ul className="list-inline text-center">
								<li>
									<a href="javascript:void(0)"
										 className="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom">
										<i className="icon-people"/></a>
								</li>
								<li className="text-left">
									<div className="text-semibold">Total online</div>
									<div className="text-muted"><span className="status-mark border-success position-left"/> 5,378
										avg
									</div>
								</li>
							</ul>

							<div className="col-lg-10 col-lg-offset-1">
								<div className="content-group"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div className="col-lg-5">

			<div className="panel panel-flat">
				<div className="panel-heading">
					<h6 className="panel-title">Sales statistics</h6>
					<div className="heading-elements">
						<form className="heading-form" action="#">
							<div className="form-group">
								<select className="change-date select-sm" id="select_date">
									<optgroup label="<i class='icon-watch pull-right'></i> Time period">
										<option value="val1">June, 29 - July, 5</option>
										<option value="val2">June, 22 - June 28</option>
										<option value="val3" selected="selected">June, 15 - June, 21</option>
										<option value="val4">June, 8 - June, 14</option>
									</optgroup>
								</select>
							</div>
						</form>
					</div>
				</div>

				<div className="container-fluid">
					<div className="row text-center">
						<div className="col-md-4">
							<div className="content-group">
								<h5 className="text-semibold no-margin"><i className="icon-calendar5 position-left text-slate"/> 5,689
								</h5>
								<span className="text-muted text-size-small">orders weekly</span>
							</div>
						</div>

						<div className="col-md-4">
							<div className="content-group">
								<h5 className="text-semibold no-margin">
									<i className="icon-calendar52 position-left text-slate"/> 32,568</h5>
								<span className="text-muted text-size-small">orders monthly</span>
							</div>
						</div>

						<div className="col-md-4">
							<div className="content-group">
								<h5 className="text-semibold no-margin"><i className="icon-cash3 position-left text-slate"/> $23,464
								</h5>
								<span className="text-muted text-size-small">average revenue</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
)

export default Index

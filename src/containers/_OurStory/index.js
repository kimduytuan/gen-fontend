import React from "react";
export  default class OurStory extends React.Component {

	render() {
		const list = this.props.data ? this.props.data : [];
		return (
			<div className="out-story-wrap">
				<div className="container container-content">
					<div className="content-text">
						<img className="ct-img img-responsive" src="static/images/logo2.png" alt="logo" />

						{list.length ?
							list.map((data, key) => {
								if (data.name && data.name === "ours_story") return (
									<div key={data.id} dangerouslySetInnerHTML={{__html: data.value}}></div>);
							})
							:
							<p>
								Partners Rick Hilton and Jeff Hyland established the firm in 1993 with the intention of creating a small
								real estate boutique that would grow to meet the demands of the market, yet never outgrow a founding
								philosophy for meeting the uniquely individual concerns of each client. Together, they bring a dynamic
								and diverse range of resources to the firm. So do some 90 Hilton &amp; Hyland agents,
								each chosen for their considerable accomplishments, as well as discerning ethics, in the marketing,
								purchase, and sale of luxury real estate.
							</p>
						}
					</div>
				</div>
			</div>
		);
	}
}

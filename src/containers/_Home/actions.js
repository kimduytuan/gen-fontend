import {LOAD_HOME, LOAD_HOME_SUCCESS} from './constants'


export function loadHome() {
	return {
		type: LOAD_HOME,
		action: 1
	}
}

export function loadedHome() {
	return {
		type: LOAD_HOME_SUCCESS,
		list: []
	}
}


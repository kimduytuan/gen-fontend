import React from "react";
import OverView from "../../components/Home/overview";
import AboutUs from "../../components/Home/aboutus";
import Block from "../../components/Home/block";
import Slide from "../../components/Home/slide";
import Video from "../../components/Components/videos";
import Blog from "../../components/Home/blog";

export default class List extends React.Component {
	render() {
		const data = this.props.data ? this.props.data : [];
		const slide = data.slide ? data.slide : [];
		const config = data.configs ? data.configs : [];
		const products = data.products ? data.products.slice(0, 4) : [];
		const area = data.area ? data.area : [];
		const blogs = data.blogs ? data.blogs : [];
		const videos = data.videos ? data.videos.slice(0, 1)[0] : [];

		return (
			<div className="blog-wrap">
				<OverView data={slide}/>
				<AboutUs data={config}/>
				<Block data={products}/>
				<Slide data={area}/>
				<Video data={videos.video} thumbnail={videos.thumbnail}/>
				<Blog data ={blogs}/>
			</div>
		);
	}
}

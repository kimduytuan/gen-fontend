import {LOAD_HOME, LOAD_HOME_SUCCESS} from './constants'

// The initial state of the App
const initialState = {
	loading: false,
	error: false,
	list: []
};

export default function (state = initialState, action) {
	const {type, list, page, total} = action
	switch (type) {
		case LOAD_HOME:
			return {...state}
		case LOAD_HOME_SUCCESS:
			return {
				...state,
				list: [...state.list, ...list],
				loading: true
			};
		default:
			return state
	}
}

import {call, fork, put, takeLatest} from "redux-saga/effects";
import Services from '../../services/req_';
import {LOAD_HOME, LOAD_HOME_FAIL, LOAD_HOME_SUCCESS} from "./constants";

function* fetchHome() {
	try {
		const properties = yield call(Services.getHome);
		yield put({type: LOAD_HOME_SUCCESS, list: properties.response});
	} catch (err) {
		yield  put({type: LOAD_HOME_FAIL});
	}
}

function* watchFetchHome() {
	yield takeLatest(LOAD_HOME, fetchHome);
}

export default function* HomeSagas() {
	yield fork(watchFetchHome);
}


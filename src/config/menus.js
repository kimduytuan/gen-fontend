module.exports = [
	{
		"name": "Dashboard",
		"url": "/",
		"icon": "icon-home4"
	},
	{
		"name": "Users",
		"url": "/users",
		"icon": "icon-stack2"
		// "sub": [
		// 	{
		// 		"name": "Users Create",
		// 		"url": "/users/create",
		// 	},
		// 	{
		// 		"name": "Users",
		// 		"url": "/aboutus",
		// 	}
		// ]
	},
	{
		"name": "Category",
		"url": "/category",
		"icon": "icon-stack2"
	}
]

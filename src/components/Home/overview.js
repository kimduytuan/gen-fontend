import React from "react";
import ReactWOW from "react-wow";
import Link from "next/link";
import buildModal from "../../models/buildModel";
import Swiper from "react-id-swiper";
export default class Overview extends React.Component {

	render() {
		const data = this.props.data ? this.props.data : [];



		const params = {
			spaceBetween: 0,
			centeredSlides: true,
			loop: true,
			autoplay: {
				delay: 3000,
				disableOnInteraction: false
			}
		};
		return (
			<section className="lrx-slide-wrapper">
				<div className="lsw-content">
					<div className="lswc-slide">
						{data.length ?
							<Swiper {...params}>
								{data.map((data, k) => {
									let thumb = data.thumbnail ? data.thumbnail : "https://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1600/n6exyndykur1ucjbfsta";
									return (
										<a key={data.id ? data.id : k} className="lswc-item" style={{backgroundImage: `url("${thumb}")`}}>
											<img src="/static/skin/frm-slide-home.png"/>
										</a>
									);
								})}
							</Swiper>
							:
							<a href="javascript:void(0)" className="lswc-item"
								 style={{backgroundImage: 'url("https://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1600/n6exyndykur1ucjbfsta")'}}>
								<img src="/static/skin/frm-slide-home.png"/>
							</a>

						}
					</div>
					<div className="lswc-text">
						<ReactWOW animation='fadeIn'>
							<h2 className="wow fadeIn">The Leading
								Los Angeles<br />Luxury Real Estate Brokerage</h2>
						<div className="wow fadeIn" data-wow-delay=".2s">
							<Link prefetch href="/properties/sale/" className="">
								<a className="link-btn link-btn-filled">
								View ExclusiveHomes
							</a>
							</Link>
						</div>
						</ReactWOW>
					</div>
					<div className="lswc-content">
						{data.length ?
							data.map((data, k) => {
								let mProperties = new buildModal().properties(data);
								let price = mProperties.price;
								let name = mProperties.name;
								let city = mProperties.city;
								let active = k == 0 ? 'active' : '';
								return(
									<a className={`lc-item ${active}`}>
              <span className="lci-number">
                0{k+1}
              </span>
										<span className="lci-cost">
                <span className="lcic-cost">
                  ${price}
                </span>
                <span className="lcic-name">
									{city}
                </span>
              </span>
									</a>
								);

							})
							:
							null
						}
					</div>
				</div>
			</section>
		);
	}
}

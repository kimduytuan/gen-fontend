import React from "react";
import ReactWOW from "react-wow";
import Link from "next/link";

export default class AboutUs extends React.Component {

	render() {
		const {data} = this.props;
		return (
			<section className="lrx-about-wrapper">
				{data.home_ours_story ? (
					<div className="container">
						<div className="law-content">
							<ReactWOW animation='fadeIn'>
								<h3 className="wow fadeIn">Luxury Casa</h3>
									<div className="wow fadeIn" data-wow-delay=".2s" dangerouslySetInnerHTML={{__html: data.home_ours_story}}/>
									<Link prefetch href="/ourstory" className="">
										<a className="btn-link">Read More</a>
									</Link>
							</ReactWOW>
						</div>
					</div>
				) : null}
			</section>
		);
	}
}

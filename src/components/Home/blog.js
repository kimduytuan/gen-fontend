import React from "react";
import Swiper from 'react-id-swiper';
import Link from 'next/link';
import buildModal from "../../models/buildModel";

export default class Blog extends React.Component {

	render() {
		let data = this.props.data ? this.props.data : [];
		const params = {
			slidesPerView: 1.6,
			spaceBetween: 180,
			loop: true,
			centeredSlides: true,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			}
		};
		return (
			<section className="lrx-blog-wrapper">
				<div className="lbw-content">
					<ul>
						{data.length ?
							<Swiper {...params}>
								{data.map((data, k) => {
									let mBlog = new buildModal().blog(data);
									let title = mBlog.title;
									let slug = mBlog.slug;
									let thumbnail = mBlog.thumbnail;
									return (
										<li key={data.id} className="lbwl-li" style={{backgroundImage: `url("${thumbnail}")`}}>
											<img src="/static/skin/frm-news.png"/>
											<div className="lbwlli-content">
												<h2 className="lbwllic-title">{title}
												</h2>
												<Link prefetch href={`/blogs/detail/?slug=${data.slug}`} as={`/blogs/detail/${slug}`}>
													<a className="lbwllic-link">Read More</a>
												</Link>
											</div>
										</li>
									)
								})}
							</Swiper>
							: null
						}
					</ul>
				</div>
			</section>
		);
	}
}

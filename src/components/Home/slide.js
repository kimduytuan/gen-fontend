import React from "react";
import Swiper from 'react-id-swiper';
import Link from 'next/link';
import buildModal from "../../models/buildModel";
import {getImage} from "../../helper/index";


export default class Slide extends React.Component {
	render() {
		let data = this.props.data ? this.props.data : [];

		const params = {
			slidesPerView: 1,
			spaceBetween: 0,
			loop: true,
			autoplay: {
				delay: 3500,
				disableOnInteraction: false
			}
		};
		return (
			<section className="lrx-slide-product-wrapper">
				<div className="container">
					<div className="lspw-content">
						<ul>

							{data.length ?
								<Swiper {...params}>
									{data.map((data, k) => {
										let mProperties = new buildModal().properties(data);
										let id = mProperties.id;
										let price = mProperties.price;
										let slug = mProperties.slug;
										let name = mProperties.name;
										let addressFull = mProperties.addressFull;
										let area = mProperties.area ? mProperties.area : '4.225.320';
										let thumbnail = getImage(mProperties.thumbnail);
										return (
											<li className="lspw-li">
												<img
													style={{backgroundImage: `url("${thumbnail}")`}}/>
												<div className="lspwl-content">
													<span className="block-title">${price}</span>
													<h3><span>{name}</span></h3>
													<p>
														{addressFull}
														<br/>
														sq {area}
													</p>
													<Link prefetch href={`/propertiesdetail?slug=${slug}?id=${id}`} as = {`/propertiesdetail/${slug}/${id}`}>
														<a>View</a>
													</Link>
												</div>
											</li>
										)
									})}
								</Swiper>
								: null
							}


						</ul>
					</div>
				</div>
			</section>
		);
	}
}

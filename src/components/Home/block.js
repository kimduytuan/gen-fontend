import React from "react";
import ReactWOW from "react-wow";
import Link from "next/link";
import buildModal from "../../models/buildModel";
import {getImage} from "../../helper";


export default class Block extends React.Component {

	render() {
		const {data} = this.props;
		return (
			<section className="lrx-block-list-wrapper">
				<div className="container">
					<div className="lblw-content">
						{
							data && data.map((item, key) => {
								const blockSmall = (key % 3) === 0 ? 'block-small' : '';
								const blockBig = key === 1 || key === 2 ? 'block-big' : '';
								const blockPostition = (key % 2) === 0 ? 'block-left' : 'block-right';
								let mProperties = new buildModal().properties(item);
								let id = mProperties.id;
								let price = mProperties.price;
								let thumbnail = getImage(mProperties.thumbnail);
								return (
									<div key={id? id : key} className={`lblw-block ${blockSmall} ${blockBig} ${blockPostition}`}
											 style={blockBig && (blockPostition === 'block-right') ? {
												 height: '80vh',
												 overflow: 'hidden'
											 } : {}}>
										<ReactWOW animation='fadeIn'>
											<div className="lblwb-block-wrapper">
												<div className="lbw-image" style={blockBig && (blockPostition === 'block-left') ? {
													height: '50vh',
													overflow: 'hidden',
													backgroundImage: `url("${thumbnail}")`
												} : {backgroundImage: `url("${thumbnail}")`}}>
													<img src="http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto/ExclusiveHOmes_pzctoe"/>
												</div>
												<div className="lbw-content">
													<span
														className="block-title">{price ? 'Price Upon Request' : `$${price}`}</span>
													<h3><span>View</span> Our Featured <br/> Listings</h3>
													<Link prefetch href={`/propertiesdetail?id=${id}`}
																as={`/propertiesdetail/${id}`}>
														<a>View</a>
													</Link>

												</div>
											</div>
										</ReactWOW>
									</div>
								)
							})
						}
					</div>
				</div>
			</section>
		);
	}
}

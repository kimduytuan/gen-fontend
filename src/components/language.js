const language = [
	//English
	{
		'header': {
			'menu': 'Menu',
			'ourStory': 'Our Story',
			'associates': 'associates',
			'properties': 'Properties',
			'searchAllHomes': 'Search all Homes',
			'contactUs': 'Contact US',
		},
		'home': {
			'pageTitle': 'Home',
			'headerOverViewTop': 'The Leading Los Angeles',
			'headerOverViewBottom': 'Luxury Real Estate Brokerage'
		},
		'associatesDetail': {
			'phone': 'Phone',
			'add': 'Add',
			'no': 'No',
			'contact': 'Contact',
			'about': 'About',
			'properties': 'Properties',
		}
	},
	
	//Vietnamese
	{
		'header': {
			'menu': 'Menu',
			'ourStory': 'Giới thiệu',
			'associates': 'Đối tác',
			'properties': 'Bất Động Sản',
			'searchAllHomes': 'Tìm kiếm dự án',
			'contactUs': 'Liên hệ',
		},
		'home': {
			'pageTitle': 'Trang chủ',
			'headerOverViewTop': 'Luxury casa',
			'headerOverViewBottom': 'Đi đầu trong lĩnh vực bất động sản'
		},
		'associatesDetail': {
			'phone': 'SĐT',
			'add': 'Đc',
			'no': 'Mã',
			'contact': 'Liên hệ',
			'about': 'Giới thiệu về',
			'properties': 'với các dự án',
		}
	}
	
];

export default language;

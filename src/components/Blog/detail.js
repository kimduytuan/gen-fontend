import React from "react";
import buildModel from "../../models/buildModel";
import Link from "next/link";
export  default class Detail extends React.Component {

	render() {
		const data = this.props.data;
		const mBlog = new buildModel().blog(data);
		let description = mBlog.description;
		console.log(description);
		console.log(123);
		let content = mBlog.content;
		return (
			<div className="detail-wrap">
				<div className="container">
					<div className="dwi-content">
						<p>
							<div dangerouslySetInnerHTML={{__html: description}}/>
							<img className="img-responsive" src="http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1600/v1529087714/ufjj098atmqbcquwjkvn.jpg"/>
						</p>
						<p>
							<div dangerouslySetInnerHTML={{__html: content}}/>
							<br /><img src="http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1600/v1529087053/pxvuquutpgzigovoqghf.jpg" className="img-responsive" data-mce-src="http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1600/v1524852382/mmla83uwy4zlwulckfqe.jpg" data-pagespeed-url-hash={2263113641} onload="pagespeed.CriticalImages.checkImageForCriticality(this);" />
							Additionally, the newest report from Christie’s International Real Estate further proves that Hilton &amp; Hyland has not only set the new standard for luxury real estate, but continues to maintain a stronghold on top sales worldwide among Christie’s premier affiliates by comprising 50% of Christie’s Top Ten Reported Sales for the First Quarter of 2018.
						</p>
					</div>
				</div>
			</div>
		);
	}
}

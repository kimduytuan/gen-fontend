import React from "react";
import buildModel from "../../models/buildModel";
import Link from "next/link";
import {getImage} from "../../helper/index";

export default class Item extends React.Component {

	render() {
		const data = this.props.data;
		const mBlog = new buildModel().blog(data);
		let title = mBlog.title;
		let slug = mBlog.slug;
		let thumbnail = getImage(mBlog.thumbnail);
		let description = mBlog.description;
		let createAt = mBlog.created_at;
		return (
			<li className="bwlw-item">
				<Link prefetch href={`/blogs/detail/?slug=${slug}`} as={`/blogs/detail/${slug}`}>
					<a>
						<img className="bwlwi-img"
								 src={thumbnail}/>
						<span className="bwlwi-date" style={{textTransform: 'uppercase'}}>{createAt}</span>
						<h4 className="bwlwi-host">
							<span>{title}</span>
						</h4>
						<p className="bwlwi-content">{description}</p>
					</a>
				</Link>
				<Link prefetch href={`/blogs/detail/?slug=${slug}`} as={`/blogs/detail/${slug}`}>
					<a className="btn-link" data-reactid={220}>READ MORE</a>
				</Link>

			</li>
		);
	}
}

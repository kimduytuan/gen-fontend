import React from "react";
export default class Detail extends React.Component {

	render() {
		return (
			<div>
				<div className="grid-list-layout-wrap">
					<div className="gllw-center">
						<div className="container">
							<div className="gllwc-item">
								<img className="gi-coverimg" src="http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1920/praxy0za5mhowpzij4ev" alt />
								<div className="gi-title">
									<h3>
										<span>Beverly West</span>
									</h3>
									<a>Read More </a>
								</div>
							</div>
							<div className="gllwc-item">
								<img className="gi-coverimg" src="http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1920/praxy0za5mhowpzij4ev" alt />
								<div className="gi-title">
									<h3>
										<span>Beverly West</span>
									</h3>
									<a>Read More </a>
								</div>
							</div>
							<div className="gllwc-item">
								<img className="gi-coverimg" src="http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1920/praxy0za5mhowpzij4ev" alt />
								<div className="gi-title">
									<h3>
										<span>Beverly West</span>
									</h3>
									<a>Read More </a>
								</div>
							</div>
							<div className="gllwc-item">
								<img className="gi-coverimg" src="http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1920/praxy0za5mhowpzij4ev" alt />
								<div className="gi-title">
									<h3>
										<span>Beverly West</span>
									</h3>
									<a>Read More </a>
								</div>
							</div>
							<div className="gllwc-item">
								<img className="gi-coverimg" src="http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1920/praxy0za5mhowpzij4ev" alt />
								<div className="gi-title">
									<h3>
										<span>Beverly West</span>
									</h3>
									<a>Read More </a>
								</div>
							</div>
							<div className="gllwc-item">
								<img className="gi-coverimg" src="http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1920/praxy0za5mhowpzij4ev" alt />
								<div className="gi-title">
									<h3>
										<span>Beverly West</span>
									</h3>
									<a>Read More </a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="contact-form-overlay">
					<div className="overlay-close">
						X
					</div>
					<div className="contact-form-overlay-wrap">
						<div className="container">
							<h2 className="cfow-title">
								Contact
							</h2>
							<div className="cfow-contact-block">
								<h5>
									Contact Details
								</h5>
								<span>
                  Hilton &amp; Hyland
                </span>
								<p className="cfowcb-add">
									257 North Cañon Drive <br /> Beverly Hills, CA 90210 <br /> CalBRE# 01160681
								</p>
								<hr />
								<p className="cfowcb-number">
									+1 310.278.3311 <br />
									<a href>info@hiltonhyland.com</a>
								</p>
							</div>
							<div className="cfow-form-block">
								<h5>
									Online Inquiry
								</h5>
								<form id="contact-form">
									<ul>
										<li>
											<input type="text" name="name" placeholder="Name" />
										</li>
										<li>
											<input type="email" name="email" placeholder="Email" />
										</li>
										<li>
											<input type="phone" name="phone" placeholder="Phone" />
										</li>
										<li>
											<div className="selectric-input-component" data-reactid={105}>
												<div className="selectric-wrapper selectric-open" data-reactid={106}>
													<div className="selectric-hide-select" data-reactid={107}>
														<select className="selectric" data-reactid={108}>
															<option data-reactid={109}>What are you interested in?</option>
															<option data-reactid={110}>Selling</option>
															<option data-reactid={111}>Buying</option>
															<option data-reactid={112}>Media</option>
															<option data-reactid={113}>Other</option>
														</select>
													</div>
												</div>
											</div>
										</li>
										<li>
											<textarea name="message" placeholder="Message" required data-reactid={126} defaultValue={""} />
										</li>
										<li><input type="submit" className="link-btn submit-btn" defaultValue="SEND" /></li>
									</ul>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

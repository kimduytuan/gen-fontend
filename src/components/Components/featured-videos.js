import React from "react";
import VideoPlay from "./video-play";
import $ from 'jquery';
import Swiper from "react-id-swiper";

export default class List extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			linkVideo: "http://www.youtube.com/watch?v=vnYJxD2jd7k"
		}
	}


	handleClickPlayVideo = (e) => {
		e.preventDefault();
		let video_play = $('.video-play-popup');
		if (video_play.hasClass('active')) {
			video_play.removeClass('active');
			$('body').removeClass('locked');
		} else {
			video_play.addClass('active');
			$('body').addClass('locked');
		}
	};

	render() {
		let data = this.props.data;

		const params = {
			slidesPerView: 'auto',
			spaceBetween: 0,
			loop: true,
			autoplay: {
				delay: 2500,
				disableOnInteraction: false
			},
			navigation: {
				nextEl: '.swiper-button-next.fa.fa-angle-right',
				prevEl: '.swiper-button-prev.fa.fa-angle-left'
			},
		};

		return (
			<section className="lrx-featured-video-wrapper ">
				<div className="container">
					<div className="lfvw-content">
						<h3 className="lfvwc-title">Featured Videos</h3>
						<ul>
							<Swiper {...params}>
							{data ?
								data.map((data, k) => {
									let thumbnail = data && data.thumbnail ? data.thumbnail : "http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1500/lnhs1rzyort8wbvr06ca";
									let video = data && data.video ? data.video : "123";
									let name = data && data.name ? data.name : "http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1500/lnhs1rzyort8wbvr06ca";
									if (k !== 0 && k < 5) {
										return (
											<li className="lrvwclli" onClick={e => {
												this.setState({linkVideo: video})
												this.handleClickPlayVideo(e);
											}}>
												<a>
													<img style={{backgroundImage: `url("${thumbnail}")`}}/>
													<h2>{name}</h2>
													<span>Watch</span>
												</a>
											</li>
										)
									}
								})
								:
								null
							}
							</Swiper>

						</ul>
					</div>
				</div>
				<VideoPlay data = {this.state.linkVideo}/>
			</section>
		);
	}
}

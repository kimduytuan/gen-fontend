import React from "react";
import $ from "jquery";

export default class VideoPlay extends React.Component {

	handleClick = (e) => {
		e.preventDefault();
		let contact_form = $('.video-play-popup');
		if (contact_form.hasClass('active')) {
			contact_form.removeClass('active');
			$('body').removeClass('locked');
		}
	};

	render() {
		let linkVideo = this.props.data.length ? this.props.data.replace("watch?v=", "embed/") : null;
		return (
			<div className="video-play-popup">
				<a className="overlay-close" onClick={(e) => this.handleClick(e)}>×</a>
						{linkVideo ?
							<iframe frameBorder="0"
											allowFullScreen="1"
											allow="autoplay; encrypted-media"
											title="YouTube video player"
											width="100%" height="100%"
											src={linkVideo + "?autoplay=0&playsinline=true&showinfo=0&rel=0&iv_load_policy=3&controls=0&start=0&origin=https%3A%2F%2Fhiltonhyland.com&enablejsapi=1&widgetid=11"}
											id="widget12">
							</iframe>
							:
							<p>Video error</p>
						}
			</div>
		);
	}
}

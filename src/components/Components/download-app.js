import React from "react";
export default class DownloadApp extends React.Component {

	render() {
		return (
			<section className="cte-section section-download-app">
				<div className="container">
					<div className="section-body">
						<div className="cte-download-app">
							<div className="row">
								<div className="thumb col-sm-6 col-md-5 col-lg-4">
									<img className="img-responsive wow bounceInRight" src="./assets/images/app.png" alt />
								</div>
								<div className="download-app-content col-xs-12 col-sm-6">
									<div className="content wow bounceInLeft">
										<h2 className="title">Download App</h2>
										<div className="description">
											Reimagined with you in mind, your top tasks are now front and center, and it's easier to find what you need within CityEpress.com
										</div>
										<div className="download-group">
											<a href="javascript:void(0)" title><i className="cte-icon icon-app-store" /></a>
											<a href="javascript:void(0)" title><i className="cte-icon icon-google-play" /></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

import React from "react";
import $ from "jquery";
import services from "../../services/req_";
import {notify} from "react-notify-toast";
import cookie from "react-cookies";


const regexPass = (/[[a-zA-Z0-9][^#&<>\"~<;$^%{}?]{6,50}$/g);


export default class Login extends React.Component {

	constructor(props) {
		super(props);
		this.state = {showLogin: "signUp"}
	}

	validateEmail(txtEmail) {
		if (txtEmail === '') {
			return true;
		} else {
			let atpos = txtEmail.indexOf("@");
			let dotpos = txtEmail.lastIndexOf(".");
			if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= txtEmail.length) {
				return false;
			} else {
				return true;
			}
		}
	}


	handleClickShowLogin(e) {
		e.preventDefault();
		let isShowLogin = $('.luxury-login-form');
		if (isShowLogin.hasClass('active')) {
			isShowLogin.removeClass('active');
			$('body').removeClass('locked');
		} else {
			isShowLogin.addClass('active');
			$('body').addClass('locked');
		}
		$('.lrxh-menu-expand').removeClass('active');
	}

	async _register(e) {
		e.preventDefault();
		let name = this.rg_firstName && this.rg_lastName ? String(this.rg_firstName.value) + String(this.rg_lastName.value) : '';
		let email = this.rg_email ? this.rg_email.value.trim() : '';
		let v_Email = this.validateEmail(email);
		let password = this.rg_password ? this.rg_password.value.trim() : '';
		let v_Pass = password.length > 5;
		if (password && email && v_Email && v_Pass) {
			let rg = await services.register(email, name, email, password);
			if (rg && rg.response && rg.meta && rg.meta.status ===200) {
				await cookie.remove('id');
				await cookie.save('id', 'weq');
				await localStorage.setItem('userInfo', JSON.stringify(rg.response));
				window.location.reload();
			} else {
				let err = rg && rg.error ? rg.error.message : 'Tài khoản đã tồn tại';
				console.log(rg);
				notify.show(err, 'error');
			}
		} else if (!email) {
			notify.show(e.message || 'Email null', 'error');
			this.rg_email.focus();
		} else if (!v_Email) {
			notify.show(e.message || 'Email false (Please insert ex: abc@gmai.com)', 'error');
			this.rg_email.focus();
		} else if (!password) {
			notify.show(e.message || 'Password null', 'error');
			this.rg_password.focus();
		}else if (!v_Pass) {
			notify.show(e.message || 'Password > 6', 'error');
			this.rg_password.focus();
		}
	}



	render() {
		return (
			<div className="luxury-login-form">
				<a href="javascript:void(0)" className="llf-close" onClick={(e) => this.handleClickShowLogin(e)}>×</a>
				
				{this.state.showLogin === "login" ?
					<div className="llf-wrap">
						<h3 className="llfw-title">
							Login to My Account
						</h3>
						<p className="llfw-slogan">
							Access your saved properties and settings.
						</p>
						<form className="llfw-form">
							<div className="lf-email">
								<input ref={(e) => this.lg_email = e} maxLength="60" name="login-email" placeholder="Email"
											 autoComplete="random-string"/>
							</div>
							<div className="lf-password">
								<input ref={(e) => this.lg_password = e} maxLength="60" type="password" name="login-password"
											 placeholder="Password" autoComplete="random-string"/>
							</div>
							<div className="lf-button">
								<button className="lfb-res" type="submit">LOGIN</button>
								<button className="lfb-loginfb">
									<span className="lfbl-text">
                  Login with facebook
                </span>
								</button>
							</div>
						</form>
						<p className="llfw-text">
							<a href="javascript:void(0)" className="llfwr-black" onClick={() => this.setState({showLogin: "forgot"})}>Forgot password ?</a>
						</p>
						<div className="llfwr-login-text">
							<span>New on Luxury Casa?</span>
							<a href="javascript:void(0)" onClick={() => this.setState({showLogin: "signUp"})}> Create a account</a>
							
						</div>
					</div>
					:
					null
				}
				
				{this.state.showLogin === "signUp" ?
					<div className="llf-wrap">
						<h3 className="llfw-title">
							welcome back
						</h3>
						<p className="llfw-slogan">
							Sign up for Luxury Casa account
						</p>
						<form className="llfw-form">
							<div className="lf-name">
								<input ref={(e) => this.rg_firstName = e} maxLength="60" name="register-first-name"
											 placeholder="First Name" style={{float: 'left'}} autoComplete="random-string"/>
								<input ref={(e) => this.rg_lastName = e} maxLength="60" name="register-first-name"
											 placeholder="Last Name" style={{float: 'right'}} autoComplete="random-string"/>
							</div>
							<div className="lf-email">
								<input ref={(e) => this.rg_email = e} maxLength="60" name="register-email" placeholder="Email"
											 autoComplete="random-string"/>
							</div>
							<div className="lf-password">
								<input ref= {(e) => this.rg_password = e} maxLength="60" type="password" name="register-password"
											 placeholder="Password" autoComplete="random-string"/>
							</div>
							<div className="lf-button">
								<button className="lfb-res" type="submit" onClick={(e) => this._register(e)}>Register</button>
								<button className="lfb-loginfb">
									<svg width="20px" height="20px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px"
											 y="0px" viewBox="0 0 310 310">
										<g fill="white" id="XMLID_834_">
											<path id="XMLID_835_"
														d="M81.703,165.106h33.981V305c0,2.762,2.238,5,5,5h57.616c2.762,0,5-2.238,5-5V165.765h39.064 c2.54,0,4.677-1.906,4.967-4.429l5.933-51.502c0.163-1.417-0.286-2.836-1.234-3.899c-0.949-1.064-2.307-1.673-3.732-1.673h-44.996 V71.978c0-9.732,5.24-14.667,15.576-14.667c1.473,0,29.42,0,29.42,0c2.762,0,5-2.239,5-5V5.037c0-2.762-2.238-5-5-5h-40.545 C187.467,0.023,186.832,0,185.896,0c-7.035,0-31.488,1.381-50.804,19.151c-21.402,19.692-18.427,43.27-17.716,47.358v37.752H81.703 c-2.762,0-5,2.238-5,5v50.844C76.703,162.867,78.941,165.106,81.703,165.106z"/>
										</g>
										<style dangerouslySetInnerHTML={{__html: ""}}/>
									</svg>
									<span className="lfbl-text">
                  register with facebook
                </span>
								</button>
							</div>
						</form>
						<p className="llfw-text">
							By signing up you acknowladge that you have read and agree to <br /> the
							<a href="javascript:void(0)" className="llfwr-black"> Terms of service and Privacy Policy.</a>
						</p>
						<div className="llfwr-login-text">
							<span>Have a account ?</span>
							<a href="javascript:void(0)" onClick={() => this.setState({showLogin: "login"})}> Log in</a>
						</div>
					</div>
					:
					null
				}
				
				{this.state.showLogin === "forgot" ?
					<div className="llf-wrap">
						<h3 className="llfw-title">
							Forgot your password ?
						</h3>
						<p className="llfw-slogan">
							Enter your email address below and we'll send you password reset instructions.
						</p>
						<form className="llfw-form">
							<div className="lf-email">
								<input ref={(e) => this.rg_email = e} maxLength="60" name="register-email"
											 autoComplete="random-string"/>
							</div>
							<div className="lf-button">
								<button className="lfb-res" type="submit" onClick={(e) => this._register(e)}>Register</button>
							</div>
						</form>
						
						<div className="llfwr-login-text">
							<span>Have a account ?</span>
							<a href="javascript:void(0)" onClick={() => this.setState({showLogin: "login"})}> Log in</a>
						</div>
					</div>
					:
					null
				}
			</div>
		);
	}
}

import React from "react";
import $ from "jquery";

export default class Contactus extends React.Component {
	constructor(props) {
		super(props);
		this.state = {showContactUs: false}
	}

	handleClickMenu = (e) => {
		e.preventDefault();
		this.setState({showContactUs: !this.state.showContactUs});
	};

	handleClickCloseContactUs = (e) => {
		e.preventDefault();
		let contact_form = $('.contact-form-overlay');
		if (contact_form.hasClass('active')) {
			contact_form.removeClass('active');
			$('body').removeClass('locked');
		} else {
			contact_form.addClass('active');
			$('body').addClass('locked');
		}
	};

	render() {
		return (
			<div className="contact-form-overlay">
				<a className="overlay-close" onClick={(e) => this.handleClickCloseContactUs(e)}>×</a>
				<div className="contact-form-overlay-wrap">
					<div className="container">
						<h2 className="cfow-title">
							Contact
						</h2>
						<div className="cfow-contact-block">
							<h5>
								Contact Details
							</h5>
							<span>
                Hilton &amp; Hyland
              </span>
							<p className="cfowcb-add">
								257 North Cañon Drive <br /> Beverly Hills, CA 90210 <br /> CalBRE# 01160681
							</p>
							<hr />
							<p className="cfowcb-number">
								+1 310.278.3311 <br />
								<a href="javascript:void(0)">info@hiltonhyland.com</a>
							</p>
						</div>
						<div className="cfow-form-block">
							<h5>
								Online Inquiry
							</h5>
							<form id="contact-form" style={{overflow: 'hidden'}}>
								<ul>
									<li>
										<input type="text" name="name" placeholder="Name" autoComplete="random-string"/>
									</li>
									<li>
										<input type="email" name="email" placeholder="Email" autoComplete="random-string" />
									</li>
									<li>
										<input type="phone" name="phone" placeholder="Phone" autoComplete="random-string" />
									</li>
									<li>
										<div className="selectric-input-component" data-reactid={105}>
											<div className="selectric-wrapper selectric-open" data-reactid={106}>
												<div className="selectric-hide-select" data-reactid={107}>
													<select className="selectric" data-reactid={108}>
														<option data-reactid={109}>What are you interested in?</option>
														<option data-reactid={110}>Selling</option>
														<option data-reactid={111}>Buying</option>
														<option data-reactid={112}>Media</option>
														<option data-reactid={113}>Other</option>
													</select>
												</div>
											</div>
										</div>
									</li>
									<li>
										<textarea name="message" placeholder="Message" required data-reactid={126} defaultValue={""} />
									</li>
									<li><input type="submit" className="link-btn submit-btn" defaultValue="SEND" /></li>
								</ul>
							</form>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

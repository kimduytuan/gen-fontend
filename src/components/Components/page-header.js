import React from "react";
export default class PageHeader extends React.Component {

	render() {
		return (
			<div className={`lrxc-header-page-wrapper ${this.props.pageHeader ? this.props.pageHeader.type : ''}`}>
				<div className="lrhpw-content" style={{backgroundImage: `url("${this.props.pageHeader ? this.props.pageHeader.img : ''}")`}}>
					<div className="lrhpwc-text">
						<div className="lrhpw-title">
							LATEST
						</div>
						<h3 className="lrhpw-name">
							{this.props.pageHeader ? this.props.pageHeader.title : ''}
						</h3>
						<p className="lrhpwslogan">
							{this.props.pageHeader ? this.props.pageHeader.slogan : ''}
						</p>
					</div>
				</div>
			</div>
		);
	}
}

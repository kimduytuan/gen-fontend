import React from "react";
import VideoPlay from "./video-play";
import $ from 'jquery';
export default class Videos extends React.Component {

	handleClickPlayVideo = (e) => {
		e.preventDefault();
		let video_play = $('.video-play-popup');
		if (video_play.hasClass('active')) {
			video_play.removeClass('active');
			$('body').removeClass('locked');
		} else {
			video_play.addClass('active');
			$('body').addClass('locked');
		}
	};

	render() {
		let linkVideo = this.props.data ? this.props.data : '';
		let thumbnail = this.props.thumbnail ? this.props.thumbnail : '';
		return (
			<section className="lrx-video-wrapper ">
				<div className="container">
					<div className="lvw-content">
						<img src={thumbnail} />
						<a href="javascript:void(0)" className="lvwc-play" onClick={e => this.handleClickPlayVideo(e)}>Play Video</a>
					</div>
				</div>
				<VideoPlay data = {linkVideo}/>
			</section>
		);
	}
}

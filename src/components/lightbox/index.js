import React from 'react'
import "./style.scss"

class Index extends React.Component {
	constructor() {
		super();
		this.state = {
			showBody: true
		}
	}

	render() {
		// const panelClass = classNames('panel panel-flat', {
		// 	'panel-collapsed': this.state.showBody,
		// 	'panel-overlay': this.props.isSubmiting,
		// 	'panel-error': this.props.success
		// });

		return (
			<div className="lb-wrapper">
				<span className="lbw-overlay"/>
				<div className="lbw-content">
					<div className="lbwc-header">
					</div>
					<div className="lbwc-content">
						<iframe width="100%" height="600px" frameBorder="0" allowFullScreen=""
										src={this.props.url}/>
					</div>
				</div>
			</div>
		)
	}
}

Index.propTypes = {}

Index.defaultProps = {}

export default Index

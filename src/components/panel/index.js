import React from 'react'
import PropTypes from "prop-types"
import classNames from 'classnames';
import Link from "next/link"
import "./style.scss"

const IconSpiner = ({isActive}) =>
	isActive ? (
		<div className="panel-spinner"><i className="icon-spinner2 spinner"/></div>
	) : null;


class Index extends React.Component {
	constructor() {
		super();
		this.state = {
			showBody: true
		}
	}

	handleClickMenu = (e) => {
		e.preventDefault();
		console.log('Action on Panel')
		this.setState({showBody: !this.state.showBody});
	}

	render() {
		const panelClass = classNames('panel panel-flat', {
			'panel-collapsed': this.state.showBody,
			'panel-overlay': this.props.isSubmiting,
			'panel-error': this.props.success
		});

		return (
			<div className={panelClass}>
				<IconSpiner isActive={this.props.isSubmiting}/>
				<div className="panel-heading">
					<h5 className="panel-title">{this.props.title}<a className="heading-elements-toggle"><i
						className="icon-more"/></a></h5>
					<div className="heading-elements">
						<ul className="icons-list">
							{this.props.action && this.props.action.map((item, key) => {
								if (item.add) {
									return (
										<li key={key}>
											<Link prefetch href={item.add}>
												<a data-action="add"/>
											</Link>
										</li>
									)
								}
							})}
							<li onClick={this.handleClickMenu}>
								<a data-action="collapse"/>
							</li>
							<li><a data-action="reload"/></li>
							<li><a data-action="close"/></li>
						</ul>
					</div>
				</div>
				<div className={this.state.showBody ? "panel-body active" : "panel-body"}>
					{this.props.children}
				</div>
			</div>
		)
	}
}

Index.propTypes = {
	title: PropTypes.string.isRequired
}

Index.defaultProps = {
	title: "Panel Title"
}

export default Index

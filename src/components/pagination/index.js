import React from 'react';
import "./style.scss"

const Pagination = ({error, total, currentPage, setPage, lastPage, ...props}) => currentPage <= lastPage ? (
	<div className="text-center">
		<ul className="pagination pagination-flat pagination-xs">
			<li className={currentPage - 1 < 1 ? "disabled" : ""}>
				<a onClick={setPage(currentPage <= 1 ? null : currentPage - 1)}>«</a>
			</li>
			<li className="active">
				<span>{currentPage}</span>
			</li>
			<li className={currentPage + 1 > lastPage ? "disabled" : ""}>
				<a onClick={setPage(currentPage + 1 > lastPage ? null : currentPage + 1)}>»</a>
			</li>
		</ul>
	</div>) : null
export default Pagination

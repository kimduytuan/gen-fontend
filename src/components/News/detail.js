import React from "react";
export default class Detail extends React.Component {

	render() {
		return (
			<article className="cte-news-detail">
				<div className="thumb">
					<img src="./assets/images/news/news-1.png" alt />
				</div>
				<div className="info">
					<h1 className="title">Ra mắt gói dịch vụ Thử Nghiệm “Siêu Tiết Kiệm” tại Hồ Chí Minh, Giao 8 giờ – đồng giá 15.000 VNĐ</h1>
					<div className="time">
						May 17, 2015
					</div>
				</div>
				<div className="content">
					<p>Từ ngày 01/08/2017 đến 01/09/2017, GHN xin ra mắt gói dịch vụ Thử Nghiệm “Siêu Tiết Kiệm” dành cho khách hàng sử dụng dịch vụ Giao hàng siêu tốc trong nội thành Hồ Chí Minh.</p>
					<p>Cụ thể: Phạm vi giao hàng, lấy hàng: Nội Thành Hồ Chí Minh gồm các quận Q1, Q3, Q4, Q5, Q6, Q7, Q8, Q10, Q11, Q. Tân Bình, Q. Tân Phú, Q. Phú Nhuận (danh sách liên tục được cập nhật thêm). Biểu giá dịch vụ:</p>
					<p>Lưu ý:</p>
					<p><img src="./assets/images/news/news-2.png" alt /></p>
					<p>1. Giao hàng siêu tốc không hỗ trợ những yêu cầu nằm ngoài quy định của công ty như: giao hàng ra ngoài phạm vi giao, giao 1 phần và trả lại một phần của đơn hàng,… xem tại https://goo.gl/YczGZr</p>
					<p>3. Khối lượng quy đổi tính theo theo công thức:</p>
					<p>Khối lượng quy đổi (kg) = (Dài (cm) x rộng (cm) x cao (cm)) / 5000</p>
					<p>Bảng giá được áp dụng với mức Khối lượng tương ứng được làm tròn lên gần nhất.</p>
					<p>4. Gói dịch vụ Siêu Tiết Kiệm không áp dụng cho các khách hàng ký hợp đồng riêng với GHN.</p>
					<p>Thông tin liên hệ hỗ trợ:</p>
					<p>Tổng đài miễn cước – 1800 1201</p>
					<p>Thư điện tử: chamsockhachhang@cte.vn</p>
				</div>
			</article>
		);
	}
}

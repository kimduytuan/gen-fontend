import React from "react";
export default class Form extends React.Component {

	render() {
		return (
			<div className="contact-block wow fadeInUp" data-wow-delay="0.25">
				<h3 className="title">
					Liên hệ
				</h3>
				<div className="content" data-wow-delay="0.25s">
					<form className="cte-contact-form" action="index.html" method="post">
						<div className="form-group">
							<input type="text" name="fullname" placeholder="Họ và tên" defaultValue className="form-control" />
						</div>
						<div className="form-group">
							<input type="text" name="phone" placeholder="Điện thoại" defaultValue className="form-control" />
						</div>
						<div className="form-group">
							<input type="text" name="email" placeholder="Email" defaultValue className="form-control" />
						</div>
						<div className="form-group">
							<textarea name="content" placeholder="Nội dung" className="form-control" defaultValue={""} />
						</div>
						<div className="row">
							<div className="col-xs-12 col-sm-6">
								<div className="form-group">
									<button type="reset" className="btn btn-lg btn-block btn-grey">Làm lại</button>
								</div>
							</div>
							<div className="col-xs-12 col-sm-6">
								<div className="form-group">
									<button type="submit" className="btn btn-lg btn-block btn-cte">Gửi ngay</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		);
	}
}

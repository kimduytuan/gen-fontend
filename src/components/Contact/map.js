import React from "react";
export default class Map extends React.Component {

	render() {
		return (
			<div className="cte-section section-map">
				<div id="js-cte-map" className="cte-map" />
			</div>
		);
	}
}

import React from "react";
export default class Info extends React.Component {

	render() {
		return (
			<div>
				<div className="contact-block wow fadeInUp" data-wow-delay="0.5s">
					<h3 className="title">
						Hotline
					</h3>
					<div className="content">
						<div className="cte-contact-hotline">
							<i className="cte-icon icon-contact" />
							024 3200 8846
						</div>
						<dl className="dl-horizontal cte-dl">
							<dt>Thứ Hai – Thứ Bảy</dt>
							<dd>07:00 – 21:00</dd>
							<dt>Chủ Nhật</dt>
							<dd>
								08:00 – 17:00</dd>
						</dl>
					</div>
				</div>
				<div className="contact-block wow fadeInUp" data-wow-delay="0.75s">
					<h3 className="title">
						Email
					</h3>
					<div className="content">
						<p>
							<b>Tư vấn về sản phẩm &amp; dịch vụ GHN:</b>
						</p>
						<p>Bộ phận Chăm sóc khách hàng:
							<br />
							<a href="javascript:void(0)">chamsockhachhang@cte.vn</a>
						</p>
					</div>
				</div>
				<div className="contact-block wow fadeInUp" data-wow-delay="1s">
					<h3 className="title">
						Kết nối
					</h3>
					<div className="content">
						<ul className="list-unstyled cte-socials">
							<li className="item">
								<a href="javascript:void(0)" title>
									<i className="cte-icon icon-pinterest-dark" />
								</a>
							</li>
							<li className="item">
								<a href="javascript:void(0)" title>
									<i className="cte-icon icon-instagram-dark" />
								</a>
							</li>
							<li className="item">
								<a href="javascript:void(0)" title>
									<i className="cte-icon icon-facebook-dark" />
								</a>
							</li>
							<li className="item">
								<a href="javascript:void(0)" title>
									<i className="cte-icon icon-linkedin-dark" />
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		);
	}
}

import React from "react";
export default class Contact extends React.Component {

	render() {
		return (
			<section className="cte-section section-contact">
				<div className="container">
					<div className="cte-contact-container">
						<div className="row">
							<div className="col-xs-12 col-sm-6">
								{'{'}% include "./_form.njk" %{'}'}
							</div>
							<div className="col-xs-12 col-sm-6">
								{'{'}% include "./_info.njk" %{'}'}
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

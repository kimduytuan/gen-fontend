import React from "react";
export default class OderTracking extends React.Component {

	render() {
		return (
			<div className="modal fade cte-modal" tabIndex={-1} role="dialog" id="cte-order-tracking">
				<div className="vertical-alignment-helper">
					<div className="vertical-align-center">
						<div className="modal-dialog" role="document">
							<div className="modal-content">
								<div className="modal-header">
									<button type="button" className="close" data-dismiss="modal" aria-label="Close">
										<i className="cte-icon icon-close" />
									</button>
									<h4 className="modal-title">Tình trạng đơn hàng</h4>
								</div>
								<div className="modal-body">
									<dl className="dl-horizontal cte-order-tracking-info">
										<dt>Trạng thái</dt>
										<dd>
											<span className="label label-success">Đã giao</span>
										</dd>
										<dt>Tên shipper</dt>
										<dd>Nguyễn Văn A</dd>
										<dt>SĐT Shipper</dt>
										<dd>0989590194</dd>
										<dt>Tên người nhận</dt>
										<dd>Trần Văn B</dd>
										<dt>SĐT người nhận</dt>
										<dd>0989590194</dd>
										<dt>Địa chỉ</dt>
										<dd>Tower 2, Times City, 458 Minh Khai, Phường Vĩnh Tuy, Quận Hai Bà Trưng, Thành Phố Hà Nội.</dd>
									</dl>
								</div>
								{'{'}# <div className="modal-footer">
									<div className="row">
									<div className="col-xs-12 col-sm-6">
									<button type="button" className="btn btn-grey btn-block btn-lg" data-dismiss="modal">Huỷ</button>
									</div>
									<div className="col-xs-12 col-sm-6">
									<button type="button" className="btn btn-cte btn-block btn-lg">Kiểm tra</button>
									</div>
									</div>
									</div> #{'}'}
							</div>
							{/* /.modal-content */}
						</div>
						{/* /.modal-dialog */}
					</div>
				</div>
			</div>
		);
	}
}

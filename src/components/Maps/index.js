import React from "react";



export default class Blog extends React.Component {

	render() {
		return (
			<div className="lrx-map-properties-wrapper">
				<div className="lrxmp-content">
					<div className="lrxmpc-filter">
						<div className="lrxmpcf-filter-wrapper">
							<div className="lfw-content">
								<div className="lfwc-search">
									<input className="lfwcs-input" title placeholder="Search Towns or Neighborhoods" />
								</div>
								<div className="lfwc-filter">
									<ul className="lfwcf-list">
										<li className="lfwcfli">
											<a href="javascript:void(0)">price range</a>
										</li>
										<li className="lfwcfli">
											<a href="javascript:void(0)">price range</a>
										</li>
										<li className="lfwcfli">
											<a href="javascript:void(0)">1+ BATH</a>
										</li>
										<li className="lfwcfli">
											<a href="javascript:void(0)">More Filters</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div className="lrxmpcf-picker-wrapper">
							<div className="lpw-content">
								<a href="javascript:void(0)" className="lpw-grid" />
								<a href="javascript:void(0)" className="lpw-location" />
							</div>
						</div>
					</div>
					<div className="lrxmpc-content">
						<div className="lrxmpcc-left">
							<div className="lrxmpccl-top">
								<div className="lrmpcclt-total">10000 Properties</div>
								<div className="lrmpcclt-action">
									<div className="lrmpcclta-content">
										<select name className="lrmpcclt-action" id translate>
											<option>Price</option>
											<option>Create</option>
										</select>
										<a href="javascript:void(0)" className="ldpwar-save">Save Search</a>
									</div>
								</div>
							</div>
							<div className="lrxmpccl-bottom">
								<div className="lrxlpw-content">
									<ul className="lrxlpwc-list">
										<li className="lrxlpwcli">
											<div className="lrxlpwcli-content-properties">
												<div className="lrcpw-image" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1920/j6o8zqufch3doszmjsyv")'}}>
													<a href="/properties-detail.html">
														<img src="/static/skin/frm-properties-list.png" />
													</a>
												</div>
												<div className="lrcpw-content">
													<a href="javascript:void(0)" className="ldpwar-save" />
													<span className="block-title">EXCLUSIVE HOMES</span>
													<h3><a href="/properties-detail.html">View Our Featured <br /> Listings</a></h3>
													<a href className="lrcpwc-read">Detail</a>
												</div>
											</div>
										</li>
										<li className="lrxlpwcli">
											<div className="lrxlpwcli-content-properties">
												<div className="lrcpw-image" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1600/n6exyndykur1ucjbfsta")'}}>
													<a href="/properties-detail.html">
														<img src="/static/skin/frm-properties-list.png" />
													</a>
												</div>
												<div className="lrcpw-content">
													<a href="javascript:void(0)" className="ldpwar-save" />
													<span className="block-title">EXCLUSIVE HOMES</span>
													<h3><a href="/properties-detail.html">View Our Featured </a></h3>
													<a href className="lrcpwc-read">Detail</a>
												</div>
											</div>
										</li>
										<li className="lrxlpwcli">
											<div className="lrxlpwcli-content-properties">
												<div className="lrcpw-image" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1920/tac0h0eywx5uinrrk9qd")'}}>
													<a href="/properties-detail.html">
														<img src="/static/skin/frm-properties-list.png" />
													</a>
												</div>
												<div className="lrcpw-content">
													<a href="javascript:void(0)" className="ldpwar-save" />
													<span className="block-title">EXCLUSIVE HOMES</span>
													<h3><a href="/properties-detail.html">View Our Featured Listings </a></h3>
													<a href className="lrcpwc-read">Detail</a>
												</div>
											</div>
										</li>
										<li className="lrxlpwcli">
											<div className="lrxlpwcli-content-properties">
												<div className="lrcpw-image" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1600/acl4zmunwdb1grf1m3ey")'}}>
													<a href="/properties-detail.html">
														<img src="/static/skin/frm-properties-list.png" />
													</a>
												</div>
												<div className="lrcpw-content">
													<a href="javascript:void(0)" className="ldpwar-save" />
													<span className="block-title">EXCLUSIVE HOMES</span>
													<h3><a href="/properties-detail.html">Our Featured <br /> Listings</a></h3>
													<a href className="lrcpwc-read">Detail</a>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>

						</div>
						<div className="lrxmpcc-right">
							<img src="/static/images/map-img.jpg" />
						</div>
					</div>
				</div>
			</div>
		);
	}
}

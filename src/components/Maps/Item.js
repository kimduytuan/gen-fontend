import React from "react";
import Link from "next/link";
import buildModel from "../../models/buildModel";
import {getImage} from "../../helper/index";

export  default class Item extends React.Component {



	setOver(lat, long, thumbnail, id, name, price) {
		this.props.self.setState({isHover:{lat: parseFloat(lat), long: parseFloat(long),thumbnail:thumbnail, id: id, name: name, price: price}});
	}

	render() {
		const data = this.props.data;
		const slugCategory = this.props.slug ? this.props.slug : 'sale';
		const mProperties = new buildModel().properties(data);

		let id = mProperties.id;
		let price = mProperties.price;
		let name = mProperties.name;
		let thumbnail = getImage(mProperties.thumbnail);
		let address = mProperties.addressFull;
		let lat = mProperties.late;
		let long = mProperties.long;
		return (
			<li className="lrxlpwcli" onMouseOver={() => this.setOver(lat, long, thumbnail, id, name, price)} style={{width: this.props.flexMap}}>
					<div className="lrxlpwcli-content-properties">
						<div className="lrcpw-image"
								 style={{backgroundImage: `url("${thumbnail}")`}}>
							<Link href={`/propertiesdetail?&id=${id}`} as = {`/propertiesdetail/${id}`}>
								<a><img src="/static/skin/frm-properties-list.png"/></a>
							</Link>
						</div>
						<div className="lrcpw-content">
							<span className="block-title">${price}</span>
							<Link href={`/propertiesdetail?&id=${id}`} as = {`/propertiesdetail/${id}`}>
								<h3><a>{name}</a></h3>
							</Link>
							<p style={{display: 'inline-block', width: '100%', margin: '-15px 0 30px', color: 'rgba(0,0,0,.88)'}}>{address}</p>
							<Link href={`/propertiesdetail?&id=${id}`} as = {`/propertiesdetail/${id}`}>
								<a className="lrcpwc-read">Detail</a>
							</Link>
						</div>
					</div>

			</li>
		);
	}
}

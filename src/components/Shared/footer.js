import React from "react";
import ReactWOW from "react-wow";
import Swiper from "react-id-swiper";
/*import "react-id-swiper/src/styles/scss/swiper.scss"*/

export default class Footer extends React.Component {



	render() {
		const params = {
			slidesPerView: 'auto',
			spaceBetween: 0,
			loop: true,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev'
			},
			autoplay: {
				delay: 2500,
				disableOnInteraction: false
			}
		};
		return (
			<footer className="lxr-footer">
				<div className="lxrfc-instagram">
					<div className="lxrfci-content clearfix">
						<ul >
							<Swiper {...params}>
								<li className="lxrfcic-li">
									<a href="https://www.instagram.com/p/BmtpnC8Fy8j/"
										 style={{backgroundImage: 'url("https://scontent.cdninstagram.com/vp/1bed9356cbfa4b5c3a3916f2510f0fde/5C0B1A79/t51.2885-15/sh0.08/e35/s640x640/38886814_999835920178380_8107984766843748352_n.jpg")'}}>
										<img src="/static/skin/frm-vel.png"/>
									</a>
								</li>
								<li className="lxrfcic-li">
									<a href="https://www.instagram.com/p/Bmb4-fNlnOZ/"
										 style={{backgroundImage: 'url("https://scontent.cdninstagram.com/vp/8e6b29ad512ea42bf11320300061c444/5BFF7803/t51.2885-15/sh0.08/e35/s640x640/38849616_237158363799643_5357168336717217792_n.jpg")'}}>
										<img src="/static/skin/frm-vel.png"/>
									</a>
								</li>
								<li className="lxrfcic-li">
									<a href="https://www.instagram.com/p/BmPI21gFptD/"
										 style={{backgroundImage: 'url("https://scontent.cdninstagram.com/vp/d2d1a76c74680bec9d1e3ea7030c1a8b/5C082879/t51.2885-15/sh0.08/e35/s640x640/38097068_301717113916259_858268700648669184_n.jpg")'}}>
										<img src="/static/skin/frm-vel.png"/>
									</a>
								</li>
								<li className="lxrfcic-li">
									<a href="https://www.instagram.com/p/Bl8aXsylbXs/"
										 style={{backgroundImage: 'url("https://scontent.cdninstagram.com/vp/c8b227b83d95e899d093ddb7c47c3591/5C063BE8/t51.2885-15/sh0.08/e35/s640x640/37627274_309245489812901_8574318215465795584_n.jpg")'}}>
										<img src="/static/skin/frm-vel.png"/>
									</a>
								</li>
								<li className="lxrfcic-li">
									<a href="https://www.instagram.com/p/BmtpnC8Fy8j/"
										 style={{backgroundImage: 'url("https://scontent.cdninstagram.com/vp/1bed9356cbfa4b5c3a3916f2510f0fde/5C0B1A79/t51.2885-15/sh0.08/e35/s640x640/38886814_999835920178380_8107984766843748352_n.jpg")'}}>
										<img src="/static/skin/frm-vel.png"/>
									</a>
								</li>
								<li className="lxrfcic-li">
									<a href="https://www.instagram.com/p/Bmb4-fNlnOZ/"
										 style={{backgroundImage: 'url("https://scontent.cdninstagram.com/vp/8e6b29ad512ea42bf11320300061c444/5BFF7803/t51.2885-15/sh0.08/e35/s640x640/38849616_237158363799643_5357168336717217792_n.jpg")'}}>
										<img src="/static/skin/frm-vel.png"/>
									</a>
								</li>
								<li className="lxrfcic-li">
									<a href="https://www.instagram.com/p/BmPI21gFptD/"
										 style={{backgroundImage: 'url("https://scontent.cdninstagram.com/vp/d2d1a76c74680bec9d1e3ea7030c1a8b/5C082879/t51.2885-15/sh0.08/e35/s640x640/38097068_301717113916259_858268700648669184_n.jpg")'}}>
										<img src="/static/skin/frm-vel.png"/>
									</a>
								</li>
							</Swiper>
						</ul>
					</div>
				</div>
				<div className="lxrf-content">
					<div className="lxrfc-content">
						<div className="container">
							<ReactWOW animation='fadeIn'>
								<a className="wow fadeIn lxrfc-logo">
									<img src="/static/images/logo.png"/>
								</a>
							</ReactWOW>
							<ul>
								<li className="lxrfclli">
									<div className="lxrfcllli-title">
										<a className="item-nav">Contact Us
										</a>
									</div>
									<ul className="left-section">
										<ReactWOW animation='fadeIn'>
											<li className="wow fadeIn" data-wow-delay=".4s">
												<p data-reactid={637}>Hilton &amp; Hyland<br />257 North Cañon Drive<br />Beverly Hills, CA
													90210<br />DRE 01160681</p>
											</li>
											<li className="wow fadeIn" data-wow-delay=".6s">
												<p>
													Phone:
													+1 310.278.3311
													<br data-reactid={642}/>
													Email:
													<a href="mailto:info@hiltonhyland.com">
														&nbsp;
														info@hiltonhyland.com
													</a>
												</p>
											</li>
										</ReactWOW>
									</ul>
								</li>
								<li className="lxrfclli social-wrapper">
									<div className="lxrfcllli-title">
										<a className="item-nav">Social Media</a>
									</div>
									<div className="social">
										<a href="https://www.facebook.com/theluxurycasa/" target="_blank">
											<img src="https://s3-us-west-2.amazonaws.com/static-lp/images/hilton/facebook.svg"/>
										</a>
										<a href="https://twitter.com/hiltonhyland" target="_blank">
											<img src="https://s3-us-west-2.amazonaws.com/static-lp/images/hilton/twitter.svg"/>
										</a>
										<a href="https://www.instagram.com/hiltonhyland" target="_blank">
											<img src="https://s3-us-west-2.amazonaws.com/static-lp/images/hilton/instagram.svg"/>
										</a>
										<a href="https://www.pinterest.com/hiltonhyland/" target="_blank">
											<img src="https://s3-us-west-2.amazonaws.com/static-lp/images/hilton/pinterest.svg"/>
										</a>
										<a href="https://www.youtube.com/user/hiltonhyland" target="_blank">
											<img src="https://s3-us-west-2.amazonaws.com/static-lp/images/hilton/youtube.svg"/>
										</a>
									</div>
								</li>
								<ReactWOW animation='fadeIn'>
									<li className="right-section-wrapper wow fadeIn" data-wow-delay=".6s">
										<div className="right-section-inner">
											<div className="lxrfcllli-title">
												<a className="item-nav">Newsletter</a>
											</div>
											<div className="newsletter-block">
												<input placeholder="Enter your email" className="newsletter-input"/>
												<div className="submit-wrapper" data-reactid={669}>
													<svg version={1.0} xmlns="http://www.w3.org/2000/svg" width="22px" height="14px"
															 viewBox="0 0 22 14" preserveAspectRatio="xMidYMid meet">
														<metadata>Created by potrace 1.15, written by Peter Selinger
															2001-2017
														</metadata>
														<g id="Page-1" stroke="none" strokeWidth={1} fill="#3d3d3d" fillRule="evenodd">
															<g id="1.0-homepage" transform="translate(-1390.000000, -6642.000000)" fill="#3d3d3d">
																<g id={6} transform="translate(-1160.000000, 6076.000000)">
																	<g id="Group-18" transform="translate(0.000000, 221.000000)">
																		<g id="Group-23" transform="translate(1188.000000, 345.000000)">
																			<g id="nav"
																				 transform="translate(692.000000, 7.000000) scale(-1, 1) translate(-692.000000, -7.000000) ">
																				<g id="back">
																					<rect id="Rectangle" x={2} y={6} width={20} height={2}/>
																					<path
																						d="M10.863961,3.51396256 L10.863961,2.51396256 L1.86396103,2.51396256 L1.86396103,4.51396256 L8.86396103,4.51396256 L8.86396103,11.5139626 L10.863961,11.5139626 L10.863961,3.51396256 Z"
																						id="Combined-Shape"
																						transform="translate(6.363961, 7.013963) rotate(-135.000000) translate(-6.363961, -7.013963) "/>
																				</g>
																			</g>
																		</g>
																	</g>
																</g>
															</g>
														</g>
													</svg>
												</div>
											</div>
										</div>
									</li>
								</ReactWOW>
							</ul>
						</div>
					</div>
					<div className="lxrfc-copyright">
						<ReactWOW animation='fadeIn'>
							<p className="wow fadeIn" data-wow-offset={10} data-wow-delay=".8s">Copyright © 2018 Luxury Casa. All Rights Reserved<br />Website developed by AgileTech.</p>
						</ReactWOW>
					</div>
				</div>
			</footer>
		);
	}
}

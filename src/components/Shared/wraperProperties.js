import React from "react";
import Header from "./header";
import Footer from "./footer";
import Contactus from "../Components/contactus";
import Login from "../Components/login";

export default class Wraper extends React.Component {
	render() {
		return (
			<div className={`luxury-wrapper ${this.props.pageClass ? this.props.pageClass : ''}`}>
				<main className="body-main">
					<Header headerClasses={this.props.headerClasses ? this.props.headerClasses : ''}/>
					{this.props.children}
				</main>
				<Footer/>
				<Contactus/>
				<Login/>
			</div>
		)
	}
}

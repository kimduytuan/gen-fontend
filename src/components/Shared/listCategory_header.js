import React from "react";
import buildModal from "../../models/buildModel";
import Link from "next/link";

export default class ListCategory extends React.Component {
	render() {
		console.log(1234);
		let data = this.props.data ? this.props.data : [];
		let mCategory = new buildModal().category(data);
		let slug = mCategory.slug;
		let name = mCategory.name;
		return (
			<li>
				<Link prefetch href={`/properties/${slug}`}>
					<a href="/properties-list.html">{name}</a>
				</Link>
			</li>
		);
	}
}

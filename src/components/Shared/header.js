import React from "react";
import $ from "jquery";
import Link from "next/link";
import cookie from "react-cookies";
import services from "../../services/req_";
import ListCategory from "./listCategory_header";
import t from '../../components/language';

export default class Header extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showMenu: false,
			isLogin: cookie.load('id') ? true : false,
			listCategory: [],
			lang: cookie.load('lang') ? cookie.load('lang') : 'vi',
			classLanguage: '',
		};
	}

	async handleClickMenu(e) {
		e.preventDefault();
		let Category = await services.getCategory();
		let category = Category && Category.meta && Category.meta.error !== 200 && Category.response ? Category.response : [];
		this.setState({showMenu: !this.state.showMenu, listCategory: category});
	};

	handleClickContactUs = (e) => {
		e.preventDefault();
		let contact_form = $('.contact-form-overlay');
		if (contact_form.hasClass('active')) {
			contact_form.removeClass('active');
			$('body').removeClass('locked');
		} else {
			contact_form.addClass('active');
			$('body').addClass('locked');
		}
	};

	handleClickShowLogin(e) {
		e.preventDefault();
		let isShowLogin = $('.luxury-login-form');
		if (isShowLogin.hasClass('active')) {
			isShowLogin.removeClass('active');
			$('body').removeClass('locked');
		} else {
			isShowLogin.addClass('active');
			$('body').addClass('locked');
		}
		$('.lrxh-menu-expand').removeClass('active');
	}

	ShowCategoryProperties(e) {
		e.preventDefault();
		$('.lrxhmecli > a').click(function (e) {
			var parent = $(this).parent();
			if (parent.has('ul').length) {
				e.preventDefault();
				$(parent.siblings('.active')).find('span').html('+');
				parent.siblings().removeClass('active');
				parent.find('span').html('×');
				parent.addClass('active');
			} else {
				e.preventDefault = false;
			}
		});
	}

	render() {
		let lang = this.state.lang;
		let header = t && t[this.state.lang === 'vi' ? 1 : 0] && t[this.state.lang === 'vi' ? 1 : 0].header ? t[this.state.lang === 'vi' ? 1 : 0].header : null;
		return (
			<div className={`lrx-header ${this.props.headerClasses ? this.props.headerClasses : ''}`}>
				<div className={this.state.showMenu ? "lrxh-menu-expand active" : "lrxh-menu-expand"}>
					<a href="javascript:void(0)" className="lrxhme-close" onClick={(e) => this.handleClickMenu(e)}>×</a>
					<div className="lrxhme-content">
						<div className="container">
							<ul className="lrxhmec-list">
								<li className="lrxhmecli" onClick={(e) => this.ShowCategoryProperties(e)}>
									<a href="/">
										Properties
										<span>+</span>
									</a>
									<ul>
										{this.state.listCategory.length ?
											this.state.listCategory.map((data, k) => {
												return (<ListCategory data={data} key={data.id ? data.id : k}/>)
											})
											:
											<li>
												<Link prefetch href="/properties/sale">
													<a>Sale</a>
												</Link>
											</li>
										}
									</ul>
								</li>
								<li className="lrxhmecli">
									<Link prefetch href="/properties/sale">
										<a>
											Development
										</a>
									</Link>
								</li>
								{/*<li className="lrxhmecli">
								 <a href="/neighborhood.html">
								 Neighborhoods
								 </a>
								 </li>*/}
								<li className="lrxhmecli" onClick={(e) => this.ShowCategoryProperties(e)}>
									<a>
										About
										<span>+</span>
									</a>
									<ul>
										<li><Link prefetch href="/ourstory"><a>The Hilton &amp; Hyland Story</a></Link></li>
									</ul>
								</li>
								<li className="lrxhmecli">
									<Link prefetch href="/associates">
										<a>
											Associates
										</a>
									</Link>
								</li>
								{/*<li className="lrxhmecli">
								 <a href="http://google.com">
								 Services
								 <span>+</span>
								 </a>
								 <ul>
								 <li><a href="/associates-detail2.html">Secured Escrow</a></li>
								 </ul>
								 </li>*/}

								<li className="lrxhmecli">
									<Link prefetch href="/videos">
										<a>
											Videos
										</a>
									</Link>
								</li>
								<li className="lrxhmecli">
									<a href="/blogs">
										Blog
									</a>
								</li>
								<li className="lrxhmecli" onClick={(e) => this.handleClickContactUs(e)}>
									<a href="javascript:void(0)">
										Contact
									</a>
								</li>
								{this.state.isLogin ?
									<li className="lrxhmecli">
										<a href="javascript:void(0)" className="lrxhmecli-login">
											Create properties
										</a>
									</li>
									:
									null
								}

								{this.state.isLogin ?
									<li className="lrxhmecli" onClick={(e) => {
										cookie.remove('id');
										this.setState({showMenu: false});
										window.location.reload();

									}
									}>
										<a href="javascript:void(0)" className="lrxhmecli-login">
											Log out
										</a>
									</li>
									:
									<li className="lrxhmecli" onClick={(e) => this.handleClickShowLogin(e)}>
										<a href="javascript:void(0)" className="lrxhmecli-login">
											Login
										</a>
									</li>

								}

							</ul>
						</div>
					</div>
				</div>
				<div className="container">
					<div className="lrxh-wrapper">
						<div className="lrxh-content">
							<div className="lrxhc-left">
								<ul className="lrxhcl-list">
									<li className="lrxhclli lrxhclli-menu" onClick={(e) => this.handleClickMenu(e)}>
										<a href="javascript:void(0)">Menu</a>
									</li>
									<Link prefetch href="/ourstory">
										<li className="lrxhclli">
											<a>{header ? header.ourStory : 'OurStory'}</a>
										</li>
									</Link>
									<Link prefetch href="/associates">
										<li className="lrxhclli">
											<a>{header ? header.associates : 'associates'}</a>
										</li>
									</Link>
									<Link prefetch href="/videos">
										<li className="lrxhclli">
											<a>Video</a>
										</li>
									</Link>
									<li className="lrxhclli">
										<Link prefetch href="/blogs">
											<a>Blog</a>
										</Link>
									</li>
								</ul>
							</div>
							<Link prefetch href="/">
								<a className="lrxhc-logo" title="Home">
									<img src="https://res.cloudinary.com/luxuryp/image/upload/h_45/emg2uesigdcgskyiq3ge.png"/>
								</a>
							</Link>
							<div className="lrxhc-right">
								<ul className="lrxhcl-list">
									<li className="lrxhclli">
										<Link prefetch href="/properties?slug=sale" as="/properties/sale">
											<a>{header ? header.properties : 'properties'}</a>
										</Link>
									</li>
									<li className="lrxhclli">
										<Link prefetch href="/maps">
											<a>{header ? header.searchAllHomes : 'Search all Homes'}</a>
										</Link>
									</li>
									<li className="lrxhclli lrxhclli-contact" onClick={(e) => this.handleClickContactUs(e)} style={{marginRight: 20}}>
										<a>{header ? header.contactUs : 'Contact Us'}</a>
									</li>

									<li className="lrxhclli">
										{/*<a className="lrxhclli-img" href><span>Eng</span></a>*/}
										<i onClick={() => {
													this.setState({classLanguage: 'show'});
										}}/>
										<ul className={`lrxhclli-ul ${this.state.classLanguage}`}>
											<li className="lrxhclli-li" onClick={()=>{
												cookie.save('lang', 'en');
												this.setState({lang: 'en', classLanguage: ''});
												window.location.reload();
											}}>
												<a>
													<span>En</span>
												</a>
											</li>
											<li className="lrxhclli-li" onClick={()=>{
												cookie.save('lang', 'vi');
												this.setState({lang: 'vi', classLanguage: ''});
												window.location.reload();
											}}>
												<a>
													<span>Vi</span>
												</a>
											</li>
										</ul>
									</li>
									{/*<li className="lrxhclli lrxhclli-contact" title="Change language"
											onClick={() => {
												cookie.save('lang', this.state.lang === 'vi' ? 'en' : 'vi');
												this.setState(prevState => ({lang: prevState.lang === 'vi' ? 'en' : 'vi'}));
											}}>
										<a className="change-language"
											 style={{backgroundImage: `url('/static/images/${this.state.lang}.png')`}}/>
									</li>*/}
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}


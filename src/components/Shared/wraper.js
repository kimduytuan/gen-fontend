import React from "react";
import Header from "./header";
import Footer from "./footer";
import PageHeader from "../Components/page-header";
import Contactus from "../Components/contactus";
import Login from "../Components/login";
import $ from "jquery";

export default class Wraper extends React.Component {

	componentDidMount() {
		window.addEventListener('scroll', this.handleScroll);	
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll);
	}

	handleScroll = () => {
		$(window).scroll(function () {
			var currentOffset = $(window).scrollTop();
			if (!$(".lrx-header").hasClass('isSticky')) {
				if (currentOffset > 80) {
					$(".lrx-header").addClass("sticky");
				} else {
					$(".lrx-header").removeClass("sticky");
				}
			}
		});
	};


	render() {
		return (
			<div className={`luxury-wrapper ${this.props.pageClass ? this.props.pageClass : ''}`}>
				<main className="body-main">
					<Header headerClasses={this.props.headerClasses ? this.props.headerClasses : ''}/>
					{this.props.pageHeader ?	<PageHeader pageHeader={this.props.pageHeader ? this.props.pageHeader : ''}/> : null}
					{this.props.children}
				</main>
				<Footer/>
				<Contactus/>
				<Login/>
			</div>
		)
	}
}

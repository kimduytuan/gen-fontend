import React from 'react';
import Error from "./Error"
import Label from "./Label"

const TextArea = ({type, id, label, error, value = '', disable, onChange, className}) => (
	<div className="form-group">
		<Label htmlFor={id} error={error}>
			{label}
		</Label>
		<textarea
			id={id}
			className={className}
			disabled={disable}
			onChange={onChange}
			defaultValue={value}
		/>
		<Error error={error}/>
	</div>
);


export default TextArea

import React, {Component} from 'react'
import Error from './Error'
import Label from './Label'
import Select from 'react-select'
import 'react-select/scss/default.scss';

export default class MySelect extends React.Component {
	handleChange = value => {
		// this is going to call setFieldValue and manually update values.topcis
		this.props.getValue ? this.props.onChange(this.props.id, value.value)
			: this.props.onChange(this.props.id, value);

	};

	handleBlur = () => {
		// this is going to call setFieldTouched and manually update touched.topcis
		this.props.onBlur(this.props.id, true);
	};

	render() {
		const {id, options, error, multi, placeholder, value, label, selectedValue, defaultValue} = this.props;
		return (
			<div className="form-group">
				<Label htmlFor={id} error={error}>
					{label}
				</Label>
				<Select
					id={id}
					instanceId={id}
					options={options}
					multi={multi}
					onChange={this.handleChange}
					onBlur={this.handleBlur}
					value={value}
					selectedValue={selectedValue}
					placeholder={placeholder}
				/>
				<Error error={error}/>
			</div>
		);
	}
}


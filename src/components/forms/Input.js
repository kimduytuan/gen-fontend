import React from 'react';
import Error from "./Error"
import Label from "./Label"

const TextInput = ({type, id, label, error, value = '', disable, onChange, className, ...props}) => (
	<div className="form-group">
		<Label htmlFor={id}>
			{label}
		</Label>
		<input
			id={id}
			className={className}
			type={type}
			value={value}
			disabled={disable}
			onChange={onChange}
			{...props}
		/>
		<Error id={id} error={error}/>
	</div>
);


export default TextInput

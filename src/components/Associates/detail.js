import React from 'react';
import ReactWOW from "react-wow";
import Link from "next/link";
import $ from "jquery";
import t from '../../components/language';
import cookie from "react-cookies";
import buildModel from "../../models/buildModel";

export default class Detail extends React.Component {
	
	constructor(props) {
		super(props);
		this.language = cookie.load('lang') ? cookie.load('lang') : 'vi';
	}

	handleClickShowContactUs = (e) => {
		e.preventDefault();
		let contact_form = $('.contact-form-overlay');
		if (contact_form.hasClass('active')) {
			contact_form.removeClass('active');
			$('body').removeClass('locked');
		} else {
			contact_form.addClass('active');
			$('body').addClass('locked');
		}
	};

	render(){
		const data = this.props.data ? this.props.data[0] : null;
		const mAssociates = new buildModel().associates(data);
		let id = mAssociates.id;
		let name = mAssociates.name;
		let phone = mAssociates.phone;
		let email = mAssociates.email;
		let address = mAssociates.address;
		let thumbnail = mAssociates.thumbnail;
		let content = mAssociates.content;
		let lang = t && t[this.language === 'vi' ? 1 : 0] && t[this.language === 'vi' ? 1 : 0].associatesDetail ? t[this.language === 'vi' ? 1 : 0].associatesDetail : null;
		return(
			<div className="lrx-associates-detail-wrapper {{ associates.column }}">
				<div className="ladw-content">
					<div className="ladwc-wrapper clearfix">
						<div className="ladwcw-information">
							<div className="container">
								<div className="ladwcei-thumb" style={{backgroundImage: `url("${thumbnail}")`}}>
									<img src="/static/skin/frm-associates-avatar.png" />
								</div>
								<div className="ladwcei-profile"><h4>
									<span>{name} </span></h4>
									<ul className="ladwcei-info-list">
										<li>
											<span>{lang.phone ? lang.phone : 'Phone'}: {phone}</span>
											<span>{lang.add ? lang.add : 'Add'}: {address}</span>
											<span>Email: <a href={`mailto:${email}`}>{email}</a></span>
										</li>
									</ul>
								</div>
								<a href="javascript:void(0)" className="ladwcil-contact" onClick={e => this.handleClickShowContactUs(e)}>{lang.contact ? lang.contact : 'Liên hệ' }</a>
							</div>
						</div>
						<div className="ladwcw-content">
							<div className="container">
								<div className="ladwcwc-wrap">
									<h3 className="ladwcwc-title">
										{lang.about? lang.about : 'About'} {name}
									</h3>
									<div className="ladwcwc-content">
										<div key={id} dangerouslySetInnerHTML={{__html: content}}/>
									</div>
								</div>
							</div>
						</div>
						<div className="container">
							<h5 className="title-associates-detail">{name} {lang.properties ? lang.properties : 'Properties'}</h5>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

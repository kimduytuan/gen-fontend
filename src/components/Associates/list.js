import React from 'react';

export default class List extends React.Component {

	render(){
		return(
			<div className="lrx-associates-wrapper">
				<div className="lassw-content">
					<div className="container">
						<ul className="lasswc-list">
							<li className="lasswcli">
								<div className="lasswcli-content">
									<a href="/associates-detail.html" className="lasswcli-thumb" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1060/omhfunkxekyhqhlisaav")'}}>
										<img src="/static/skin/frm-associates.png" />
									</a>
									<h4><a href> Adeena Fitterman </a></h4>
									<div className="profile-list-social">
										<a href className="social-links" />
										<a className="btn-link white-btn-link profile-btn">Profile</a>
									</div>
								</div>
							</li>
							<li className="lasswcli">
								<div className="lasswcli-content">
									<a href="/associates-detail.html" className="lasswcli-thumb" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1060/omhfunkxekyhqhlisaav")'}}>
										<img src="/static/skin/frm-associates.png" />
									</a>
									<h4><a href> Adeena Fitterman </a></h4>
									<div className="profile-list-social">
										<a href className="social-links" />
										<a className="btn-link white-btn-link profile-btn">Profile</a>
									</div>
								</div>
							</li>
							<li className="lasswcli">
								<div className="lasswcli-content">
									<a href="/associates-detail.html" className="lasswcli-thumb" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1060/omhfunkxekyhqhlisaav")'}}>
										<img src="/static/skin/frm-associates.png" />
									</a>
									<h4><a href> Adeena Fitterman </a></h4>
									<div className="profile-list-social">
										<a href className="social-links" />
										<a className="btn-link white-btn-link profile-btn">Profile</a>
									</div>
								</div>
							</li>
							<li className="lasswcli">
								<div className="lasswcli-content">
									<a href="/associates-detail.html" className="lasswcli-thumb" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1060/omhfunkxekyhqhlisaav")'}}>
										<img src="/static/skin/frm-associates.png" />
									</a>
									<h4><a href> Adeena Fitterman </a></h4>
									<div className="profile-list-social">
										<a href className="social-links" />
										<a className="btn-link white-btn-link profile-btn">Profile</a>
									</div>
								</div>
							</li>
							<li className="lasswcli">
								<div className="lasswcli-content">
									<a href="/associates-detail.html" className="lasswcli-thumb" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1060/omhfunkxekyhqhlisaav")'}}>
										<img src="/static/skin/frm-associates.png" />
									</a>
									<h4><a href> Adeena Fitterman </a></h4>
									<div className="profile-list-social">
										<a href className="social-links" />
										<a className="btn-link white-btn-link profile-btn">Profile</a>
									</div>
								</div>
							</li>
							<li className="lasswcli">
								<div className="lasswcli-content">
									<a href="/associates-detail.html" className="lasswcli-thumb" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1060/omhfunkxekyhqhlisaav")'}}>
										<img src="/static/skin/frm-associates.png" />
									</a>
									<h4><a href> Adeena Fitterman </a></h4>
									<div className="profile-list-social">
										<a href className="social-links" />
										<a className="btn-link white-btn-link profile-btn">Profile</a>
									</div>
								</div>
							</li>
							<li className="lasswcli">
								<div className="lasswcli-content">
									<a href="/associates-detail.html" className="lasswcli-thumb" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1060/omhfunkxekyhqhlisaav")'}}>
										<img src="/static/skin/frm-associates.png" />
									</a>
									<h4><a href> Adeena Fitterman </a></h4>
									<div className="profile-list-social">
										<a href className="social-links" />
										<a className="btn-link white-btn-link profile-btn">Profile</a>
									</div>
								</div>
							</li>
							<li className="lasswcli">
								<div className="lasswcli-content">
									<a href="/associates-detail.html" className="lasswcli-thumb" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1060/omhfunkxekyhqhlisaav")'}}>
										<img src="/static/skin/frm-associates.png" />
									</a>
									<h4><a href> Adeena Fitterman </a></h4>
									<div className="profile-list-social">
										<a href className="social-links" />
										<a className="btn-link white-btn-link profile-btn">Profile</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		);
	}
}

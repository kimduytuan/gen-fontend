import React from "react";
import Link from "next/link";
import buildModel from "../../models/buildModel";

export default class Item extends React.Component {

	render() {
		const data = this.props.data ? this.props.data : null;
		const mAssociates = new buildModel().associates(data);
		let id = mAssociates.id;
		let name = mAssociates.name;
		let slug = data && data.slug ? data.slug : null;
		let thumbnail = mAssociates.thumbnail;
		return (
			<li className="lasswcli">
				<div className="lasswcli-content">
					<Link prefetch href={`/associates/detail?slug=${slug}_${id}`} as={`/associates/detail/${slug}_${id}`}>
						<a href={`/associates/${slug}`} className="lasswcli-thumb" style={{backgroundImage: `url("${thumbnail}")`}}>
						<img src="/static/skin/frm-associates.png"/>
					</a>
					</Link>
					<h4><a>{name}</a></h4>
					<div className="profile-list-social">
						<a className="social-links"/>
						<Link prefetch href={`/associates/detail?slug=${slug}_${id}`} as={`/associates/detail/${slug}_${id}`}>
							<a href={`/associates/${slug}`} className="btn-link white-btn-link profile-btn">Profile</a>
						</Link>
					</div>
				</div>
			</li>
		);
	}
}

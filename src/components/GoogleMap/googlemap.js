import React from "react";
import mapStyle from "../../static/googleMapStyleSiver";
import {GoogleMap, withGoogleMap, withScriptjs} from "react-google-maps";
import {MarkerWithLabel} from "react-google-maps/lib/components/addons/MarkerWithLabel";

class Index extends React.Component {
 	constructor(){
 		super();
 		this.state={
			isOpen: false,
			k: -1,
			isHover: null,
			lat: 10.8230989,
			long: 106.6296638
		}
	}

 	_onToggleOpen  = (k) => this.setState({
		isOpen: !this.state.isOpen,
		k: k
	});

 	componentWillReceiveProps(newProps) {
 		this.setState({isHover: newProps.isHover});
	}


	render() {
		const {markers} = this.props;
		return (
			<GoogleMap
				defaultZoom={2}
				center={{lat: this.state.isHover ? this.state.isHover.lat : 10.8230989, lng: this.state.isHover ? this.state.isHover.long : 106.6296638}}
				defaultOptions={{ styles: mapStyle}}
			>

				{markers && markers.length && markers.map((marker, k) => {
						let id = marker.id ? marker.id : 0;
						let lat = marker.late ? parseFloat(marker.late) : 0;
						let long = marker.long ? parseFloat(marker.long) : 0;
						let thumbnail = marker.thumbnail ? marker.thumbnail : '';
						let name = marker.name ? marker.name : '';
						let price = marker.price ? marker.price : '';
						if (lat && long) {
							return(
								<MarkerWithLabel
									position={{ lat: lat, lng: long }}
									labelAnchor={new google.maps.Point(25, -2)}
									labelStyle={{
										backgroundColor: "#0e0b0bd6",
										color: "#fff",
										borderRadius: "2px",
										fontSize: "12px",
										padding: "4px 8px"
									}}
									onClick={() => this.setState({isHover:{lat: parseFloat(lat), long: parseFloat(long),thumbnail:thumbnail}, lat: parseFloat(lat), long: parseFloat(long), id: id, name: name, price: price})}
									onMouseOver={() => this._onToggleOpen(k)}
									onMouseOut={() => this._onToggleOpen(k)}
								>
									<div>
										{
											this.state.isOpen && this.state.k === k ?
												<div>
													<span>${marker.price} | {marker.beds} | {marker.name}</span>
												</div>

												:
												<div>${marker.price}</div>
										}
									</div>
								</MarkerWithLabel>
							);
						}
					}
				)}

				{this.state.isHover ?
				<MarkerWithLabel
					position={{ lat: this.state.isHover.lat, lng: this.state.isHover.long }}
					labelAnchor={new google.maps.Point(80, 80)}
					labelStyle={{fontSize: "12px", padding: "4px"}}
					onClick={() => window.location.href = `propertiesDetail/${this.state.isHover.id}`}
				>
					<div style={{height: 50, minWidth: 50}}>

						<img style={{height: 50}} src={this.state.isHover.thumbnail}/>
					</div>
				</MarkerWithLabel>
					:
					null
				}
			</GoogleMap>
		);
	}
}


export default withScriptjs(withGoogleMap(Index))

import React from "react";
import GoogleMap from './googlemap';

class Index extends React.Component {
	render() {
		const {markers,self,isHover} = this.props;
		return (
			<GoogleMap
				googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeTz-AA6UeBJuv8odeJEmdXMMWnP8qPFk&v=3.exp&libraries=geometry,drawing,places"
				// onChange={onChange}
				// defaultCenter={defaultCenter}
				// defaultZoom={parseInt(defaultZoom)}
				markers={markers}
				// polygon={polygon}
				isHover={isHover}
				isMarkerShown loadingElement={<div style={{height: `100%`}}/>}
				containerElement={<div style={{height: `100%`}}/>}
				mapElement={<div style={{height: `100%`}}/>}/>

		);
	}
}


export default Index

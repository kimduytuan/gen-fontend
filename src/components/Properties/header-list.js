import React from "react";
import ReactWOW from "react-wow";
import Option from "./optionSearch";
import Services from "../../services/req_";

export  default class HeaderList extends React.Component {
	constructor() {
		super();
		this.state = {neighbhorhood: '', lifeStyle: '', price: 0, houseSize: 0};
	}

	async SearchProperty() {
		let res = await Services.searchProperties(this.state.neighbhorhood, this.state.lifeStyle, this.state.price, this.state.houseSize);
		if (res.response) this.props.self.listProperties.setState({list: res.response});
	}

	render() {
		let area = this.props.area ? this.props.area : [];
		let lifeStyle = this.props.lifeStyle ? this.props.lifeStyle : [];
		return (
			<div className="lrx-header-properties-wrapper">
				<div className="lhpw-content">
					<div className="lhpw-background"
							 style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1920/tac0h0eywx5uinrrk9qd")'}}>
						<div className="lhpwb-content">
							<div className="lhpwb-title-wrapper">
								<div className="container">
									<ReactWOW animation='fadeIn'>
									<h2 className="">
										<span>Browse</span> our listings or <br /> schedule a private showing.
									</h2>
									</ReactWOW>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="lhpw-filter-wrapper">
					<div className="container">
						<div className="lhpwbfw-content">
							<select className="selectric" title="" onChange={(e) => {
								let value = e.target.value;
								this.setState({neighbhorhood: value});
								setTimeout(() => this.SearchProperty(e), 10);
							}}>
								<option value={0}>Any Neighborhood</option>
								{area.length ?
									area.map((data) => {
										return (<Option area={data} type="area" key={data.id}/>);
									}) : null
								}
							</select>
							<select className="selectric" title="" onChange={(e) => {
								let value = e.target.value;
								this.setState({lifeStyle: value});
								setTimeout(() => this.SearchProperty(e), 10);
							}}>
								<option value={0}>Any LifeStyle</option>
								{lifeStyle.length ?
									lifeStyle.map((data) => {
										return (<Option lifeStyle={data} type="lifeStyle" key={data.id}/>);
									}) : null
								}
							</select>
							<select className="selectric" title="" onChange={(e) => {
								let value = e.target.value;
								this.setState({price: value});
								setTimeout(() => this.SearchProperty(e), 100);
							}}>
								<option value={0}>Any Price</option>
								<option value={1}>$100 - $1K</option>
								<option value={2}>$1K - $10K</option>
								<option value={3}>$10K - $50K</option>
								<option value={4}>$50K - $100K</option>
								<option value={5}>$100K - $200K</option>
								<option value={6}>$200K - $300K</option>
								<option value={7}>$300K - $400K</option>
								<option value={8}>$400K - $500K</option>
								<option value={9}>$500K - $1M</option>
								<option value={10}>$1M +</option>
							</select>
							<select className="selectric" title="" onChange={(e) => {
								let value = e.target.value;
								this.setState({houseSize: value});
								setTimeout(() => this.SearchProperty(e), 100);
							}}>
								<option value={0}>Any House Size</option>
								<option value={1}>50m2 - 100m2</option>
								<option value={2}>100m2 - 200m2</option>
								<option value={3}>200m2 - 300m2</option>
								<option value={4}>400m2 - 400m2</option>
								<option value={5}>400m2 - 500m2</option>
								<option value={6}>500m2 - 600m2</option>
								<option value={7}>600m2 - 700m2</option>
								<option value={8}>700m2 - 800m2</option>
								<option value={9}>800m2 - 1000m2</option>
								<option value={10}>1000m2 +</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

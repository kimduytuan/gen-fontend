import React from "react";
export  default class Sidebar extends React.Component {

	render() {
		return (
			<aside className="cte-news-sidebar">
				<div className="cte-sidebar-block">
					<div className="sidebar-header">
						<h2 className="title">Tin tức nổi bật</h2>
					</div>
					<div className="sidebar-body">
						<div className="sidebar-news-list">
							<div className="row row-eq-height">
								{[...Array(3).map((data, k) => {
									return (
										<div className="col-md-12 col-sm-6 col-xs-6">
											<a href="./news-detail.html" className="item">
												<div className="thumb">
													<img src="./assets/images/news/news-sidebar-{{i+1}}.png" alt/>
												</div>
												<div className="info">
													<h3 className="title">Hướng dẫn nạp ví City Express trên ứng dụng bằng Zalo Pay</h3>
													<div className="time">
														May 17, 2015
													</div>
												</div>
											</a>
										</div>
									)
								})]}
							</div>
						</div>
					</div>
				</div>
			</aside>
		);
	}
}

import React from "react";
import ReactWOW from "react-wow";
import Link from "next/link";
import buildModel from "../../models/buildModel";
import {getImage} from "../../helper/index";

export  default class Item extends React.Component {
	render() {
		const data = this.props.data ? this.props.data : null;
		const mProperties = new buildModel().properties(data);
		let id = mProperties.id;
		let price = mProperties.price;
		let name = mProperties.name;
		let thumbnail = getImage(mProperties.thumbnail, 500, 300);
		let address = mProperties.addressFull;
		return (
			<li className="lrxlpwcli">
				<ReactWOW animation='fadeIn'>
					<Link href={`/propertiesdetail?&id=${id}`} as = {`/propertiesdetail/${id}`}>
					<div className="lrxlpwcli-content-properties" data-wow-duration="2s" data-wow-delay="5s">
					<div className="lrcpw-image"
							 style={{backgroundImage: `url("${thumbnail}")`}}>
							<a><img src="/static/skin/frm-properties-list.png"/></a>
					</div>
					<div className="lrcpw-content">
						<span className="block-title">{price} VNĐ</span>
						<h3><a>{name}</a></h3><p
						style={{display: 'inline-block', width: '100%', margin: '-15px 0 30px', color: 'rgba(0,0,0,.88)'}}>{address}</p>
							<a className="lrcpwc-read">Detail</a>
					</div>
				</div>
					</Link>
				</ReactWOW>
			</li>
		);
	}
}

import React, {Component} from "react";
import buildModel from "../../models/buildModel";
import Swiper from "react-id-swiper";


export  default class HeaderDetail extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			gallerySwiper: null,
			thumbnailSwiper: null
		};
		this.galleryRef = this.galleryRef.bind(this);
		this.thumbRef = this.thumbRef.bind(this);
	}

	componentWillUpdate(nextProps, nextState) {
		if (nextState.gallerySwiper && nextState.thumbnailSwiper) {
			const {gallerySwiper, thumbnailSwiper} = nextState

			gallerySwiper.controller.control = thumbnailSwiper;
			thumbnailSwiper.controller.control = gallerySwiper;
		}
	}

	galleryRef(ref) {
		if (ref) this.setState({gallerySwiper: ref.swiper})
	}

	thumbRef(ref) {
		if (ref) this.setState({thumbnailSwiper: ref.swiper})
	}

	render() {
		let data = this.props.data ? this.props.data : [];
		const mProperties = new buildModel().properties(data);
		let cover = mProperties.cover ?  mProperties.cover : "https://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1920/j6o8zqufch3doszmjsyv";
		let album = mProperties.album ? JSON.parse(mProperties.album) : [];


		const gallerySwiperParams = {
			spaceBetween: 0,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			}
		};
		return (
			<div className="lrx-header-detail-properties-wrapper">
				<div className="lhdpw-content">
					<ul className="lhdpwc-list">

						{album.length > 1 ?
								<Swiper {...gallerySwiperParams} ref={this.galleryRef}>
									{
										album.map((data, k) => {
											return (
												<li key={k * 193888447474} className="lhdpwcli" style={{backgroundImage: `url("${data}")`}}>
													<img src="/static/skin/frm-slide-home.png"/>
												</li>
											);
										})
									}
								</Swiper>

							:
							<li className="lhdpwcli" style={{backgroundImage: `url("${cover}")`}}>
								<img src="/static/skin/frm-slide-home.png"/>
							</li>
						}
					</ul>
				</div>
			</div>
		);
	}
}

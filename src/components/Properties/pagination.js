import React from "react";
export  default class Pagination extends React.Component {

	render() {
		return (
			<div className="text-center cte-pagination">
				<nav aria-label="Page navigation">
					<ul className="pagination">
						<li className="disabled">
							<a href="javascript:void(0)" aria-label="Previous">
								<i className="cte-icon icon-left-arrow-black"/>
							</a>
						</li>
						<li className="active">
							<a href="javascript:void(0)">1</a>
						</li>
						<li>
							<a href="javascript:void(0)">2</a>
						</li>
						<li>
							<a href="javascript:void(0)">3</a>
						</li>
						<li>
							<a href="javascript:void(0)" aria-label="Next">
								<i className="cte-icon icon-right-arrow-black"/>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		);
	}
}

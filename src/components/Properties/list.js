import React from "react";
export  default class List extends React.Component {

	render() {
		return (
			<div className="lrx-list-properties-wrapper">
				<div className="container">
					<div className="lrxlpw-content">
						<ul className="lrxlpwc-list">
							<li className="lrxlpwcli">
								<div className="lrxlpwcli-content-properties">
									<div className="lrcpw-image" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1920/j6o8zqufch3doszmjsyv")'}}>
										<a href="/properties-detail.html">
											<img src="/static/skin/frm-properties-list.png" />
										</a>
									</div>
									<div className="lrcpw-content">
										<span className="block-title">EXCLUSIVE HOMES</span>
										<h3><a href="/properties-detail.html">View Our Featured <br /> Listings</a></h3>
										<a href className="lrcpwc-read">Detail</a>
									</div>
								</div>
							</li>
							<li className="lrxlpwcli">
								<div className="lrxlpwcli-content-properties">
									<div className="lrcpw-image" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1600/n6exyndykur1ucjbfsta")'}}>
										<a href="/properties-detail.html">
											<img src="/static/skin/frm-properties-list.png" />
										</a>
									</div>
									<div className="lrcpw-content">
										<span className="block-title">EXCLUSIVE HOMES</span>
										<h3><a href="/properties-detail.html">View Our Featured </a></h3>
										<a href className="lrcpwc-read">Detail</a>
									</div>
								</div>
							</li>
							<li className="lrxlpwcli">
								<div className="lrxlpwcli-content-properties">
									<div className="lrcpw-image" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1920/tac0h0eywx5uinrrk9qd")'}}>
										<a href="/properties-detail.html">
											<img src="/static/skin/frm-properties-list.png" />
										</a>
									</div>
									<div className="lrcpw-content">
										<span className="block-title">EXCLUSIVE HOMES</span>
										<h3><a href="/properties-detail.html">View Our Featured Listings </a></h3>
										<a href className="lrcpwc-read">Detail</a>
									</div>
								</div>
							</li>
							<li className="lrxlpwcli">
								<div className="lrxlpwcli-content-properties">
									<div className="lrcpw-image" style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1600/acl4zmunwdb1grf1m3ey")'}}>
										<a href="/properties-detail.html">
											<img src="/static/skin/frm-properties-list.png" />
										</a>
									</div>
									<div className="lrcpw-content">
										<span className="block-title">EXCLUSIVE HOMES</span>
										<h3><a href="/properties-detail.html">Our Featured <br /> Listings</a></h3>
										<a href className="lrcpwc-read">Detail</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		);
	}
}

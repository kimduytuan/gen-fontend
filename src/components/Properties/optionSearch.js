import React from "react";
import buildModal from "../../models/buildModel";

export default class OptionClass extends React.Component {


	render() {
		let type = this.props.type ? this.props.type : null;
		let area = this.props.area ? this.props.area : [];
		let mArea = new buildModal().area(area);
		let area_id = mArea.id;
		let area_name = mArea.name;
		let lifeStyle = this.props.lifeStyle ? this.props.lifeStyle : [];
		let mLifeStyle = new buildModal().lifeStyle(lifeStyle);
		let lifeStyle_id = mLifeStyle.id;
		let lifeStyle_name = mLifeStyle.name;
		return (<option value={type == "area" ? area_id : lifeStyle_id}>{type == "area" ? area_name : lifeStyle_name}</option>);
	}


}

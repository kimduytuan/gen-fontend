import React from "react";
import buildModel from "../../models/buildModel";
import ReactWOW from "react-wow";
import Video from "../Components/videos";
import Link from "next/link";
import $ from "jquery";
import config from '../../config/index';
import mapStyle from '../../static/googleMapStyleSiver';
import {getImage} from '../../helper/index';

const { compose } = require("recompose");
const {
	withScriptjs,
	withGoogleMap,
	GoogleMap,
	Marker,
} = require("react-google-maps");

const MapWithAMarker = compose(
	withScriptjs,
	withGoogleMap
)(props =>
	<GoogleMap
		defaultZoom={13}
		defaultOptions={{ styles: mapStyle}}
		defaultCenter={{ lat: parseFloat(props.data.late ? props.data.late : 0), lng: parseFloat(props.data.long ? props.data.long : 0) }}
	>
		<Marker
			position={{ lat: parseFloat(props.data.late ? props.data.late : 0), lng: parseFloat(props.data.long ? props.data.long : 0) }}
		/>
	</GoogleMap>
);



export  default class Detail extends React.Component {
	handleClickShowContactUs = (e) => {
		e.preventDefault();
		let contact_form = $('.contact-form-overlay');
		if (contact_form.hasClass('active')) {
			contact_form.removeClass('active');
			$('body').removeClass('locked');
		} else {
			contact_form.addClass('active');
			$('body').addClass('locked');
		}
	};

	render() {
		const data = this.props.data ? this.props.data : [];
		const mProperties = new buildModel().properties(data);
		console.log(data);

		let id = mProperties.id;
		let price = mProperties.price;
		let name = mProperties.name;
		let content = mProperties.content;
		let description = mProperties.description;
		let thumbnail = getImage(mProperties.thumbnail);
		let address = mProperties.addressFull;
		let house_size = mProperties.house_size;
		let quote = mProperties.quote;
		let quote_by = mProperties.quote_by;
		let video = mProperties.video;
		let album = mProperties.album;

		let creator = data.creator ? data.creator : [];
		let associates = data.associates ? data.associates : [];
		let area = data.area_rls ? data.area_rls : [];
		let area_id = area && area.id ? area.id : null;
		let area_name = area && area.name ? area.name : null;
		let area_content = area && area.content ? area.content : null;
		let area_thumbnail = area && area.thumbnail ? getImage(area.thumbnail, 1200) : null;
		let listAlbum = album ? JSON.parse(album) : null;
		return (
			<div className="lrx-detail-properties-wrapper">
				<div className="ldpw-content">
					<div className="ldpwc-info">
						<div className="container">
							<ReactWOW animation='fadeIn'>
							<span className="block-title wow fadeIn">${price}</span>
							<h3 className="wow fadeIn" data-wow-delay=".2s">
								<span>{address}</span>
							</h3>
							<p className="wow fadeIn" data-wow-delay=".4s">
								/ SQ {house_size}
							</p>
							</ReactWOW>
						</div>
					</div>

					<div className="ldpwc-associates">
						<div className="container">
							<div className="ldpwa-content clearfix">
								<ReactWOW animation='fadeIn'>
								<div className="ldpwa-left">
									<img src={thumbnail} />
								</div>
								</ReactWOW>
								<div className="ldpwa-right">
									<a href="javascript:void(0)" className="ldpwar-save">Save Property</a>
									<div className="ldpwa-associates">
										<ul className="ldpwaa-list">
											{associates.length ?
												associates.map((data, k) => {
												return(
													<li className="ldpwaali" key={data.id}>
														<div className="property-details-user">
															<Link prefetch href={`/associates&id=${data.id}`} as = {`/associates/${data.slug}`}>
																<a>
																	<img src={data && data.thumbnail ? data.thumbnail: null} />
																</a>
															</Link>
															<span>{data && data.name ? data.name : null}</span>
														</div>
													</li>
												)
											})
												:
												null
											}

										</ul>
									</div>
									<div className="ldpwa-content">
										<h5 className="ldpwac-title">{name}</h5>
										<div className="ldpwac-content">
											<p>{description}</p>
											<div dangerouslySetInnerHTML={{__html: content}}/>
										</div>
									</div>
									<div className="ldpwa-button">
										<a className="btn-link" onClick={e => this.handleClickShowContactUs(e)}> Request Info </a>
										<a className="btn-link white-btn-link pdf-btn" target="_blank">Download PDF</a>
										<a className="btn-link white-btn-link share-btn">Share This</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					{quote ? <div className="ldpwc-quote-wrapper">
						<div className="container">
							<h3>
								<span>“…{quote}"</span>
							</h3>
							<p>{quote_by}</p>
						</div>
					</div> : null}

					{video ? <Video data={video} thumbnail={thumbnail}/> : null}

					<div className="ldpwc-gallery-wrapper">
						<div className="container">
							<ul className="lgw-list">
								<li className="lgwli lgwli-content" style={{paddingTop: 20}}>
									{ listAlbum ?
										listAlbum.map((data, k) => {
											let className = (k === 0 || k % 2 == 0) ? "lgwlil-left" : "lgwlil-right";
											if (k < 2)
												return (
													<div className={className} key={k}>
														<img src={data}/>
													</div>
												);
										})
										:
										null
									}
								</li>
							</ul>
						</div>
					</div>
					{ area_id ?
						<div className="ldpwc-neibor-wrapper">
							<div className="lgww-content">
								<img src={area_thumbnail}/>
							</div>
							<ReactWOW animation='fadeIn'>
							<div className="lgww-info">
								<div className="container">
									<div className="lspwl-content">
										<h3><span>The Neighbhorhood: <br/> {area_name}</span></h3>
										<div dangerouslySetInnerHTML={{__html: area_content}} />
										<a href="/">Learn More</a></div>
								</div>
							</div>
							</ReactWOW>
						</div>
						:
						null

					}

					<div className="ldpwc-map-wrapper">
						<div className="container">
							<MapWithAMarker
								googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyATv8ec5MYMEmQQvSRFI0aFDxGsJtPkH9I&v=3.exp&libraries=geometry,drawing,places"
								loadingElement={<div style={{ height: `100%` }} />}
								containerElement={<div style={{ height: `400px` }} />}
								mapElement={<div style={{ height: `100%` }} />}
								data={this.props.data ? this.props.data : []}
							/>
						</div>
					</div>

					<div className="ldpwc-disclaimer-wrapper">
						<div className="container">
							<div className="ldissw-left">
								<span>Disclaimer</span>
							</div>
							<div className="ldissw-right">
								<p>Luxyra &amp; Casa makes no representation, warranty or guaranty as to
									accuracy of any data contained herein, and advises interested parties to confirm all information
									before relying on it for a purchase decision. All information provided has been obtained from
									sources believed reliable, but may be subject to errors, omissions, change of price, prior sale,
									or withdrawal without notice. Measurements and square footages are approximate and may be
									verified by consulting an appraiser or architect. The content set forth on this website is
									provided exclusively for consumers’ personal, non-commercial use, solely to identify potential
									properties for potential purchase; unlawful use, distribution or duplication is strictly
									prohibited and may violate relevant federal and state law. © 2018 Hilton &amp; Hyland.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

import React from "react";
export  default class HeaderListNoFilter extends React.Component {

	render() {
		return (
			<div className="lrx-header-properties-wrapper no-filter">
				<div className="lhpw-content">
					<div className="lhpw-background"
							 style={{backgroundImage: 'url("http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto/sefgbxem23ky0hppl3oe")'}}>
						<div className="lhpwb-content">
							<div className="lhpwb-title-wrapper">
								<div className="container">
									<h2 className="wow fadeIn">
										<span>International Properties</span>
									</h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

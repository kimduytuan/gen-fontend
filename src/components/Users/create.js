import React from 'react'
import {withFormik} from 'formik';
import Yup from 'yup';
import Panel from "../../containers/panel"
import TextInput from "../Forms/Input"

const UserCreate = props => {
	const {
		values,
		touched,
		errors,
		status,
		dirty,
		handleChange,
		handleBlur,
		handleSubmit,
		handleReset,
		isSubmitting,
	} = props;

	return (
		<div className="row">
			<div className="col-md-6 col-md-push-3">
				<Panel title="Create User" success={!!status && !status.success}
							 error={!!errors.submit} isSubmiting={isSubmitting}>
					<form onSubmit={handleSubmit}>
						<fieldset>
							<legend className="text-semibold">Enter your information</legend>
							<TextInput
								id="fullname"
								className="form-control"
								type="text"
								label="Enter your name:"
								placeholder="John"
								error={touched.fullname && errors.fullname}
								value={values.fullname}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
							<TextInput
								id="email"
								className="form-control"
								type="text"
								label="Enter your name:"
								placeholder="John"
								error={touched.email && errors.email}
								value={values.email}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
						</fieldset>
						<div className="text-right">
							<button disabled={isSubmitting} type="submit" className="btn btn-primary">Submit form<i
								className="icon-arrow-right14 position-right"/></button>
						</div>
					</form>
				</Panel>
			</div>
		</div>
	);
};

const MyEnhancedForm = withFormik({
	validationSchema: Yup.object().shape({
		fullname: Yup.string()
			.min(2, "C'mon, your name is longer than that")
			.required('First name is required.'),
		email: Yup.string()
			.email('Invalid email address')
			.required('Email is required!'),
	}),

	mapPropsToValues: () => ({
		fullname: '', email: ''
	}),
	handleSubmit: (payload, {setSubmitting, setStatus, resetForm}) => {
		setTimeout(() => {
			alert(JSON.stringify(payload));
			resetForm({
				fullname: '', email: ''
			})
			setStatus({success: false})
			setSubmitting(false);
		}, 3000)
	},
	displayName: 'CreateUser',
})(UserCreate);

export default MyEnhancedForm

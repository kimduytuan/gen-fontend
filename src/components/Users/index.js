import React from 'react'
import Panel from "../../containers/panel"

const Index = ({}) => (
	<div className="row">
		<div className="col-md-12">
			<Panel title="Small table">

				Example of <code>small</code> table sizing using <code>.table-sm</code> class added to the <code>.table</code>.
				All table rows have <code>40px</code> height.

				<div className="table-responsive">
					<table className="table table-sm">
						<thead>
						<tr>
							<th>#</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Username</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>1</td>
							<td>Eugene</td>
							<td>Kopyov</td>
							<td>@Kopyov</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Victoria</td>
							<td>Baker</td>
							<td>@Vicky</td>
						</tr>
						<tr>
							<td>3</td>
							<td>James</td>
							<td>Alexander</td>
							<td>@Alex</td>
						</tr>
						<tr>
							<td>4</td>
							<td>Franklin</td>
							<td>Morrison</td>
							<td>@Frank</td>
						</tr>
						</tbody>
					</table>
				</div>
			</Panel>
		</div>
	</div>
)

export default Index

import React from 'react'
import NavBar from '../../containers/NavBar/index'
import SlideBar from '../../containers/SlideBar/index'

class Index extends React.Component {
	constructor() {
		super();
		this.state = {
			showMenu: false
		}
	}

	handleClickMenu = (e) => {
		e.preventDefault();
		this.setState({showMenu: !this.state.showMenu});
	}

	render() {
		return (
			<div className={this.state.showMenu ? "" : "sidebar-xs"}>
				<NavBar handleClick={this.handleClickMenu}/>
				<div className="page-container" style={{minHeight: ' calc(100vh - 48px)'}}>
					<div className="page-content">
						<SlideBar/>
						<div className="content-wrapper">
							<div className="content">
								{this.props.children}
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default Index

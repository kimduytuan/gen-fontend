import React from "react";
export  default class Post extends React.Component {

	render() {
		return (
			<div className="social-auth">
				<div className="social-auth-divider">
					<span>Or</span>
				</div>
				<div className="row">
					<div className="col-xs-6">
						<a href="javascript:void(0)" className="btn btn-facebook btn-block btn-lg">
							<i className="cte-icon icon-facebook"/>
							Facebook
						</a>
					</div>
					<div className="col-xs-6">
						<a href="javascript:void(0)" className="btn btn-google-plus btn-block btn-lg">
							<i className="cte-icon icon-google-plus"/>
							Google
						</a>
					</div>
				</div>
			</div>
		);
	}
}

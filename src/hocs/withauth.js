import React, {Component} from 'react'
import Service from '../services'
import Router from 'next/router'

export default function withAuth(AuthComponent) {
	const Auth = new Service()
	return class Authenticated extends Component {
		constructor(props) {
			super(props)
			this.state = {
				isLoading: true
			};
		}

		componentDidMount() {
			console.log(Auth.loggedIn())
			if (!Auth.loggedIn()) {
				// this.props.url.replaceTo('/')
				Router.push('/')
			}
			this.setState({isLoading: false})
		}

		render() {
			return (
				<div>
					{this.state.isLoading ? (
						<div>LOADING....</div>
					) : (
						<AuthComponent {...this.props} auth={Auth}/>
					)}
				</div>
			)
		}
	}
}

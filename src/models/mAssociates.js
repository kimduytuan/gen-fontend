import cookie from "react-cookies";
import * as moment from 'moment';

export default class mAssociates {
	constructor() {
		this.lang = cookie.load('lang') ? cookie.load('lang') : 'vi';
		this._id = null;
		this._name = null;
		this._nameEn = null;
		this._slug = null;
		this._slugEn = null;
		this._thumbnail = null;
		this._address = null;
		this._phone = null;
		this._email = null;
		this._content = null;
		this._contentEn = null;
		this._creator = null;
		this._created_at = null;
		this._updated_at = null;
	}

	get id() {
		return this._id
	}
	set id(value) {
		this._id = value
	}

	get name() {
		return this.lang === 'vi' ? this._name : this._nameEn
	}
	set name(value) {
		this._name = value
	}
	set nameEn(value) {
		this._nameEn = value
	}

	get slug() {
		return this.lang === 'vi' ? this._slugEn : this._slugEn
	}
	set slug(value) {
		this._slug = value
	}
	set slugEn(value) {
		this._slugEn = value
	}

	get thumbnail() {
		return this._thumbnail
	}
	set thumbnail(value) {
		this._thumbnail = value
	}

	get address() {
		return this._address
	}
	set address(value) {
		this._address = value
	}

	get phone() {
		return this._phone
	}
	set phone(value) {
		this._phone = value
	}

	get email() {
		return this._email
	}
	set email(value) {
		this._email = value
	}
	
	get content() {
		return this.lang === 'vi' ? this._content : this._contentEn
	}
	set content(value) {
		this._content = value
	}
	set contentEn(value) {
		this._contentEn = value
	}

	get creator() {
		return this._creator
	}
	set creator(value) {
		this._creator = value
	}

	get created_at() {
		return this.lang === 'vi' ? moment.utc(this._created_at).format("DD-MM-YYYY") : moment.utc(this._created_at).format('LL');
	}
	set created_at(value) {
		this._created_at = value
	}

	get updated_at() {
		return this.lang === 'vi' ? moment.utc(this._updated_at).format("DD-MM-YYYY") : moment.utc(this._updated_at).format('LL');
	}
	set updated_at(value) {
		this._updated_at = value
	}

}

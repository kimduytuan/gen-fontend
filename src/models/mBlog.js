import cookie from "react-cookies";
import * as moment from 'moment';

export default class mBlog {
	constructor() {
		this.lang = cookie.load('lang') ? cookie.load('lang') : 'vi';
		this._id = null;
		this._title = null;
		this._titleEn = null;
		this._slug = null;
		this._slugEn = null;
		this._status = null;
		this._thumbnail = null;
		this._thumbnailEn = null;
		this._description = null;
		this._descriptionEn = null;
		this._content = null;
		this._contentEn = null;
		this._piorty = null;
		this._type = null;
		this._creator = null;
		this._parentCategory = null;
		this._seoTitle = null;
		this._seoTitleEn = null;
		this._seoMeta = null;
		this._seoMetaEn = null;
		this._seoDescription = null;
		this._seoDescriptionEn = null;
		this._view = null;
		this._tags = null;
		this._related = null;
		this._album = null;
		this._author = null;
		this._created_at = null;
		this._updated_at = null;
	}

	get id() {
		return this._id
	}
	set id(value) {
		this._id = value
	}

	get title() {
		return this.lang === 'vi' ? this._title : this._titleEn
	}
	set title(value) {
		this._title = value
	}
	set titleEn(value) {
		this._titleEn = value
	}

	get slug() {
		return this.lang === 'vi' ? this._slug : this._slugEn
	}
	set slug(value) {
		this._slug = value
	}
	set slugEn(value) {
		this._slugEn = value
	}

	get status() {
		return this._status
	}
	set status(value) {
		this._status = value
	}

	get thumbnail() {
		return this.lang === 'vi' ? this._thumbnail : this._thumbnailEn
	}
	set thumbnail(value) {
		this._thumbnail = value
	}
	set thumbnailEn(value) {
		this._thumbnailEn = value
	}

	get description() {
		return this.lang === 'vi' ? this._description : this._descriptionEn
	}
	set description(value) {
		this._description = value
	}
	set descriptionEn(value) {
		this._descriptionEn = value
	}

	get content() {
		return this.lang === 'vi' ? this._content : this._contentEn
	}
	set content(value) {
		this._content = value
	}
	set contentEn(value) {
		this._contentEn = value
	}

	get piorty() {
		return this._piorty
	}
	set piorty(value) {
		this._piorty = value
	}

	get type() {
		return this._type
	}
	set type(value) {
		this._type = value
	}

	get creator() {
		return this._creator
	}
	set creator(value) {
		this._creator = value
	}

	get parentCategory() {
		return this._parentCategory
	}
	set parentCategory(value) {
		this._parentCategory = value
	}

	get seoTitle() {
		return this.lang === 'vi' ? this._seoTitle : this._seoTitleEn
	}
	set seoTitle(value) {
		this._seoTitle = value
	}
	set seoTitleEn(value) {
		this._seoTitleEn = value
	}

	get seoMeta() {
		return this.lang === 'vi' ? this._seoMeta : this._seoMetaEn
	}
	set seoMeta(value) {
		this._seoMeta = value
	}
	set seoMetaEn(value) {
		this._seoMetaEn = value
	}

	get seoDescription() {
		return this.lang === 'vi' ? this._seoDescription : this._seoDescriptionEn
	}
	set seoDescription(value) {
		this._seoDescription = value
	}
	set seoDescriptionEn(value) {
		this._seoDescriptionEn = value
	}

	get view() {
		return this._view
	}
	set view(value) {
		this._view = value
	}

	get tags() {
		return this._tags
	}
	set tags(value) {
		this._tags = value
	}

	get related() {
		return this._related
	}
	set related(value) {
		this._related = value
	}

	get album() {
		return this._album
	}
	set album(value) {
		this._album = value
	}

	get author() {
		return this._author
	}
	set author(value) {
		this._author = value
	}

	get created_at() {
		return this.lang === 'vi' ? moment.utc(this._created_at).format("DD-MM-YYYY") : moment.utc(this._created_at).format('LL');
	}
	set created_at(value) {
		this._created_at = value
	}

	get updated_at() {
		return this.lang === 'vi' ? moment.utc(this._updated_at).format("DD-MM-YYYY") : moment.utc(this._updated_at).format('LL');
	}
	set updated_at(value) {
		this._updated_at = value
	}
}

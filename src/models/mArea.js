import cookie from "react-cookies";
import * as moment from 'moment';

export default class mArea {
	constructor() {
		this.lang = cookie.load('lang') ? cookie.load('lang') : 'vi';
		this._id = null;
		this._name = null;
		this._nameEn = null;
		this._slug = null;
		this._slugEn = null;
		this._description = null;
		this._descriptionEn = null;
		this._content = null;
		this._contentEn = null;
		this._addressFull = null;
		this._addressFullEn = null;
		this._city = null;
		this._postalcode = null;
		this._country = null;
		this._state = null;
		this._streetname = null;
		this._late = null;
		this._long = null;
		this._area = null;
		this._price = null;
		this._parentId = null;
		this._album = null;
		this._type = null;
		this._status = null;
		this._created_at = null;
		this._updated_at = null;
		this._deletedAt = null;
		this._thumbnail = null;
	}

	get id() {
		return this._id
	}
	set id(value) {
		this._id = value
	}

	get name() {
		return this.lang === 'vi' ? this._name : this._nameEn
	}
	set name(value) {
		this._name = value
	}
	set nameEn(value) {
		this._nameEn = value
	}


	get slug() {
		return this.lang === 'vi' ? this._slug : this._slugEn
	}
	set slug(value) {
		this._slug = value
	}
	set slugEn(value) {
		this._slugEn = value
	}

	get description() {
		return this.lang === 'vi' ? this._description : this._descriptionEn
	}
	set description(value) {
		this._description = value
	}
	set descriptionEn(value) {
		this._descriptionEn = value
	}

	get content() {
		return this.lang === 'vi' ? this._content : this._contentEn
	}
	set content(value) {
		this._content = value
	}
	set contentEn(value) {
		this._contentEn = value
	}

	get addressFull() {
		return this.lang === 'vi' ? this._addressFull : this._addressFullEn
	}
	set addressFull(value) {
		this._addressFull = value
	}
	set addressFullEn(value) {
		this._addressFullEn = value
	}

	get city() {
		return this._city
	}
	set city(value) {
		this._city = value
	}

	get postalcode() {
		return this._postalcode
	}
	set postalcode(value) {
		this._postalcode = value
	}

	get country() {
		return this._country
	}
	set country(value) {
		this._country = value
	}

	get state() {
		return this._state
	}
	set state(value) {
		this._state = value
	}

	get streetname() {
		return this._streetname
	}
	set streetname(value) {
		this._streetname = value
	}

	get late() {
		return this._late
	}
	set late(value) {
		this._late = value
	}

	get long() {
		return this._long
	}
	set long(value) {
		this._long = value
	}

	get area() {
		return this._area
	}
	set area(value) {
		this._area = value
	}

	get price() {
		return this._price
	}
	set price(value) {
		this._price = value
	}

	get parentId() {
		return this._parentId
	}
	set parentId(value) {
		this._parentId = value
	}

	get album() {
		return this._album
	}
	set album(value) {
		this._album = value
	}

	get type() {
		return this._type
	}
	set type(value) {
		this._type = value
	}

	get status() {
		return this._status
	}
	set status(value) {
		this._status = value
	}

	get created_at() {
		return this.lang === 'vi' ? moment.utc(this._created_at).format("DD-MM-YYYY") : moment.utc(this._created_at).format('LL');
	}
	set created_at(value) {
		this._created_at = value
	}

	get updated_at() {
		return this.lang === 'vi' ? moment.utc(this._updated_at).format("DD-MM-YYYY") : moment.utc(this._updated_at).format('LL');
	}
	set updated_at(value) {
		this._updated_at = value
	}

	get deletedAt() {
		return this.lang === 'vi' ? moment.utc(this._deletedAt).format("DD-MM-YYYY") : moment.utc(this._deletedAt).format('LL');
	}
	set deletedAt(value) {
		this._deletedAt = value
	}

	get thumbnail() {
		return this._thumbnail
	}
	set thumbnail(value) {
		this._thumbnail = value
	}
}

import mBlog from "./mBlog";
import mProperties from "./mProperties";
import mAssociate from "./mAssociates";
import mAreas from "./mArea";
import mCategories from "./mCategory";
import mLifeStyles from "./mLifeStyle";



//Check ok 21/08/2018
export default class buildModel {

	area(data = null) {
		let mArea = new mAreas();
		if (data.id) mArea.id = data.id;
		if (data.name) mArea.name = data.name;
		if (data.nameEn) mArea.nameEn = data.nameEn;
		if (data.slug) mArea.slug = data.slug;
		if (data.slugEn) mArea.slugEn = data.slugEn;
		if (data.description) mArea.description = data.description;
		if (data.descriptionEn) mArea.descriptionEn = data.descriptionEn;
		if (data.content) mArea.content = data.content;
		if (data.contentEn) mArea.contentEn = data.contentEn;
		if (data.addressFull) mArea.addressFull = data.addressFull;
		if (data.addressFullEn) mArea.addressFullEn = data.addressFullEn;
		if (data.city) mArea.city = data.city;
		if (data.postalcode) mArea.postalcode = data.postalcode;
		if (data.country) mArea.country = data.country;
		if (data.streetname) mArea.streetname = data.streetname;
		if (data.late) mArea.late = data.late;
		if (data.long) mArea.long = data.long;
		if (data.area) mArea.area = data.area;
		if (data.price) mArea.price = data.price;
		if (data.parentId) mArea.parentId = data.parentId;
		if (data.album) mArea.album = data.album;
		if (data.type) mArea.type = data.type;
		if (data.status) mArea.status = data.status;
		if (data.created_at) mArea.created_at = data.created_at;
		if (data.updated_at) mArea.updated_at = data.updated_at;
		if (data.deletedAt) mArea.deletedAt = data.deletedAt;
		if (data.thumbnail) mArea.thumbnail = data.thumbnail;
		return mArea;
	}

	blog(data = null) {
		let mBlogs = new mBlog();
		if (data.id) mBlogs.id = data.id;
		if (data.title) mBlogs.title = data.title;
		if (data.titleEn) mBlogs.titleEn = data.titleEn;
		if (data.slug) mBlogs.slug = data.slug;
		if (data.slugEn) mBlogs.slugEn = data.slugEn;
		if (data.status) mBlogs.status = data.status;
		if (data.thumbnail) mBlogs.thumbnail = data.thumbnail;
		if (data.thumbnailEn) mBlogs.thumbnailEn = data.thumbnailEn;
		if (data.description) mBlogs.description = data.description;
		if (data.descriptionEn) mBlogs.descriptionEn = data.descriptionEn;
		if (data.content) mBlogs.content = data.content;
		if (data.contentEn) mBlogs.contentEn = data.contentEn;
		if (data.piorty) mBlogs.piorty = data.piorty;
		if (data.type) mBlogs.type = data.type;
		if (data.creator) mBlogs.creator = data.creator;
		if (data.parentCategory) mBlogs.parentCategory = data.parentCategory;
		if (data.seoTitle) mBlogs.seoTitle = data.seoTitle;
		if (data.seoTitleEn) mBlogs.seoTitleEn = data.seoTitleEn;
		if (data.seoMeta) mBlogs.seoMeta = data.seoMeta;
		if (data.seoMetaEn) mBlogs.seoMetaEn = data.seoMetaEn;
		if (data.seoDescription) mBlogs.seoDescription = data.seoDescription;
		if (data.seoDescriptionEn) mBlogs.seoDescriptionEn = data.seoDescriptionEn;
		if (data.view) mBlogs.view = data.view;
		if (data.tags) mBlogs.tags = data.tags;
		if (data.related) mBlogs.related = data.related;
		if (data.album) mBlogs.album = data.album;
		if (data.author) mBlogs.author = data.author;
		if (data.created_at) mBlogs.created_at = data.created_at;
		if (data.updated_at) mBlogs.updated_at = data.updated_at;
		return mBlogs;
	}

	associates(data = []) {
		let mAssociates = new mAssociate();
		if (data.id) mAssociates.id = data.id;
		if (data.name) mAssociates.name = data.name;
		if (data.nameEn) mAssociates.nameEn = data.nameEn;
		if (data.slug) mAssociates.slug = data.slug;
		if (data.slugEn) mAssociates.slugEn = data.slugEn;
		if (data.thumbnail) mAssociates.thumbnail = data.thumbnail;
		if (data.address) mAssociates.address = data.address;
		if (data.phone) mAssociates.phone = data.phone;
		if (data.email) mAssociates.email = data.email;
		if (data.content) mAssociates.content = data.content;
		if (data.contentEn) mAssociates.contentEn = data.contentEn;
		if (data.creator) mAssociates.creator = data.creator;
		if (data.created_at) mAssociates.created_at = data.created_at;
		if (data.updated_at) mAssociates.updated_at = data.updated_at;
		return mAssociates;
	}

	category(data = null) {
		let mCategory = new mCategories();
		if (data.id) mCategory.id = data.id;
		if (data.name) mCategory.name = data.name;
		if (data.nameEn) mCategory.nameEn = data.nameEn;
		if (data.slug) mCategory.slug = data.slug;
		if (data.slugEn) mCategory.slugEn = data.slugEn;
		if (data.description) mCategory.description = data.description;
		if (data.descriptionEn) mCategory.descriptionEn = data.descriptionEn;
		if (data.parentId) mCategory.parentId = data.parentId;
		if (data.type) mCategory.type = data.type;
		if (data.status) mCategory.status = data.status;
		if (data.created_at) mCategory.created_at = data.created_at;
		if (data.updated_at) mCategory.updated_at = data.updated_at;
		if (data.deletedAt) mCategory.deletedAt = data.deletedAt;
		if (data.thumbnail) mCategory.thumbnail = data.thumbnail;
		return mCategory;
	}

	lifeStyle(data = null) {
		let mLifeStyle = new mLifeStyles();
		if (data.id) mLifeStyle.id = data.id;
		if (data.name) mLifeStyle.name = data.name;
		if (data.nameEn) mLifeStyle.nameEn = data.nameEn;
		if (data.slug) mLifeStyle.slug = data.slug;
		if (data.slugEn) mLifeStyle.slugEn = data.slugEn;
		if (data.created_at) mLifeStyle.created_at = data.created_at;
		if (data.updated_at) mLifeStyle.updated_at = data.updated_at;
		return mLifeStyle;
	}

	properties(data = null) {
		let mProperty = new mProperties();
		if (data.id) mProperty.id = data.id;
		if (data.name) mProperty.name = data.name;
		if (data.nameEn) mProperty.nameEn = data.nameEn;
		if (data.slug) mProperty.slug = data.slug;
		if (data.slugEn) mProperty.slugEn = data.slugEn;
		if (data.description) mProperty.description = data.description;
		if (data.descriptionEn) mProperty.descriptionEn = data.descriptionEn;
		if (data.content) mProperty.content = data.content;
		if (data.contentEn) mProperty.contentEn = data.contentEn;
		if (data.addressFull) mProperty.addressFull = data.addressFull;
		if (data.addressFullEn) mProperty.addressFullEn = data.addressFullEn;
		if (data.city) mProperty.city = data.city;
		if (data.postalcode) mProperty.postalcode = data.postalcode;
		if (data.country) mProperty.country = data.country;
		if (data.state) mProperty.state = data.state;
		if (data.streetname) mProperty.streetname = data.streetname;
		if (data.price) mProperty.price = data.price;
		if (data.late) mProperty.late = data.late;
		if (data.long) mProperty.long = data.long;
		if (data.area) mProperty.area = data.area;
		if (data.parentId) mProperty.parentId = data.parentId;
		if (data.album) mProperty.album = data.album;
		if (data.albumContent) mProperty.albumContent = data.albumContent;
		if (data.quote) mProperty.quote = data.quote;
		if (data.quote_by) mProperty.quote_by = data.quote_by;
		if (data.beds) mProperty.beds = data.beds;
		if (data.baths) mProperty.baths = data.baths;
		if (data.typePrice) mProperty.typePrice = data.typePrice;
		if (data.special) mProperty.special = data.special;
		if (data.type) mProperty.type = data.type;
		if (data.status) mProperty.status = data.status;
		if (data.creator) mProperty.creator = data.creator;
		if (data.created_at) mProperty.created_at = data.created_at;
		if (data.updated_at) mProperty.updated_at = data.updated_at;
		if (data.video) mProperty.video = data.video;
		if (data.life_style) mProperty.life_style = data.life_style;
		if (data.neighbhorhood) mProperty.neighbhorhood = data.neighbhorhood;
		if (data.source) mProperty.source = data.source;
		if (data.house_size) mProperty.house_size = data.house_size;
		if (data.document) mProperty.document = data.document;
		if (data.cover) mProperty.cover = data.cover;
		if (data.thumbnail) mProperty.thumbnail = data.thumbnail;

		if (data.associates) mProperty.associates = data.associates;

		return mProperty;
	}
}

/*
export default class buildModel {
	constructor() {

	}

	blog(data = null) {
		let Models = [];

		data.forEach((blog) => {
			let mBlogs = new mBlog();
			if (blog.title) mBlogs.title = blog.title;
			if (blog.title) mBlogs.titleEn = blog.titleEn;
			Models.push(mBlogs);
		});


		return Models;

	}
}*/

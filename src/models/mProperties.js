import cookie from "react-cookies";
import * as moment from "moment";

export default class mProperties {
	constructor() {
		this.lang = cookie.load('lang') ? cookie.load('lang') : 'vi';
		this._id = null;
		this._name = null;
		this._nameEn = null;
		this._slug = null;
		this._slugEn = null;
		this._description = null;
		this._descriptionEn = null;
		this._content = null;
		this._contentEn = null;
		this._addressFull = null;
		this._addressFullEn = null;
		this._city = null;
		this._postalcode = null;
		this._country = null;
		this._state = null;
		this._streetname = null;
		this._price = null;
		this._late = null;
		this._long = null;
		this._area = null;
		this._parentId = null;
		this._album = null;
		this._albumContent = null;
		this._quote = null;
		this._quote_by = null;
		this._beds = null;
		this._baths = null;
		this._typePrice = null;
		this._special = null;
		this._type = null;
		this._status = null;
		this._creator = null;
		this._created_at = null;
		this._updated_at = null;
		this._video = null;
		this._life_style = null;
		this._neighbhorhood = null;
		this._source = null;
		this._house_size = null;
		this._document = null;
		this._cover = null;
		this._thumbnail = null;

		this._associates = null;
	}

	get id() {
		return this._id
	}
	set id(value) {
		this._id = value
	}

	get name() {
		return this.lang === 'vi' ? this._name : this._nameEn
	}
	set name(value) {
		this._name = value
	}
	set nameEn(value) {
		this._nameEn = value
	}

	get slug() {
		return this.lang === 'vi' ? this._slug : this._slugEn
	}
	set slug(value) {
		this._slug = value
	}
	set slugEn(value) {
		this._slugEn = value
	}

	get description() {
		return this.lang === 'vi' ? this._description : this._descriptionEn
	}
	set description(value) {
		this._description = value
	}
	set descriptionEn(value) {
		this._descriptionEn = value
	}

	get content() {
		return this.lang === 'vi' ? this._content : this._contentEn
	}
	set content(value) {
		this._content = value
	}
	set contentEn(value) {
		this._contentEn = value
	}

	get addressFull() {
		return this.lang === 'vi' ? this._addressFull : this._addressFullEn
	}
	set addressFull(value) {
		this._addressFull = value
	}
	set addressFullEn(value) {
		this._addressFullEn = value
	}

	get city() {
		return this._city
	}
	set city(value) {
		this._city = value
	}

	get postalcode() {
		return this._postalcode
	}
	set postalcode(value) {
		this._postalcode = value
	}

	get country() {
		return this._country
	}
	set country(value) {
		this._country = value
	}

	get state() {
		return this._state
	}
	set state(value) {
		this._state = value
	}

	get streetname() {
		return this._streetname
	}
	set streetname(value) {
		this._streetname = value
	}

	get price() {
		return this._price
	}
	set price(value) {
		this._price = value
	}

	get late() {
		return this._late
	}
	set late(value) {
		this._late = value
	}

	get long() {
		return this._long
	}
	set long(value) {
		this._long = value
	}

	get area() {
		return this._area
	}
	set area(value) {
		this._area = value
	}

	get parentId() {
		return this._parentId
	}
	set parentId(value) {
		this._parentId = value
	}

	get album() {
		return this._album
	}
	set album(value) {
		this._album = value
	}

	get albumContent() {
		return this._albumContent
	}
	set albumContent(value) {
		this._albumContent = value
	}

	get quote() {
		return this._quote
	}
	set quote(value) {
		this._quote = value
	}

	get quote_by() {
		return this._quote_by
	}
	set quote_by(value) {
		this._quote_by = value
	}

	get beds() {
		return this._beds
	}
	set beds(value) {
		this._beds = value
	}

	get baths() {
		return this._baths
	}
	set baths(value) {
		this._baths = value
	}

	get typePrice() {
		return this._typePrice
	}
	set typePrice(value) {
		this._typePrice = value
	}

	get special() {
		return this._special
	}
	set special(value) {
		this._special = value
	}

	get type() {
		return this._type
	}
	set type(value) {
		this._type = value
	}

	get status() {
		return this._status
	}
	set status(value) {
		this._status = value
	}

	get creator() {
		return this._creator
	}
	set creator(value) {
		this._creator = value
	}

	get created_at() {
		return this.lang === 'vi' ? moment.utc(this._created_at).format("DD-MM-YYYY") : moment.utc(this._created_at).format('LL');
	}
	set created_at(value) {
		this._created_at = value
	}

	get updated_at() {
		return this.lang === 'vi' ? moment.utc(this._updated_at).format("DD-MM-YYYY") : moment.utc(this._updated_at).format('LL');
	}
	set updated_at(value) {
		this._updated_at = value
	}

	get video() {
		return this._video
	}
	set video(value) {
		this._video = value
	}

	get life_style() {
		return this._life_style
	}
	set life_style(value) {
		this._life_style = value
	}

	get neighbhorhood() {
		return this._neighbhorhood
	}
	set neighbhorhood(value) {
		this._neighbhorhood = value
	}

	get source() {
		return this._source
	}
	set source(value) {
		this._source = value
	}

	get house_size() {
		return this._house_size
	}
	set house_size(value) {
		this._house_size = value
	}

	get document() {
		return this._document
	}
	set document(value) {
		this._document = value
	}

	get cover() {
		return this._cover
	}
	set cover(value) {
		this._cover = value
	}

	get thumbnail() {
		return this._thumbnail
	}
	set thumbnail(value) {
		this._thumbnail = value
	}

	get associates() {
		return this._associates
	}
	set associates(value) {
		this._associates = value
	}

}

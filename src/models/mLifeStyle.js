import cookie from "react-cookies";
import * as moment from 'moment';

export default class mLifeStyle {
	constructor() {
		this.lang = cookie.load('lang') ? cookie.load('lang') : 'vi';
		this._id = null;
		this._name = null;
		this._nameEn = null;
		this._slug = null;
		this._slugEn = null;
		this._created_at = null;
		this._updated_at = null;
	}

	get id() {
		return this._id
	}
	set id(value) {
		this._id = value
	}

	get name() {
		return this.lang === 'vi' ? this._name : this._nameEn
	}
	set name(value) {
		this._name = value
	}
	set nameEn(value) {
		this._nameEn = value
	}

	get slug() {
		return this.lang === 'vi' ? this._slug : this._slugEn
	}
	set slug(value) {
		this._slug = value
	}
	set slugEn(value) {
		this._slugEn = value
	}

	get created_at() {
		return this.lang === 'vi' ? moment.utc(this._created_at).format("DD-MM-YYYY") : moment.utc(this._created_at).format('LL');
	}
	set created_at(value) {
		this._created_at = value
	}

	get updated_at() {
		return this.lang === 'vi' ? moment.utc(this._updated_at).format("DD-MM-YYYY") : moment.utc(this._updated_at).format('LL');
	}
	set updated_at(value) {
		this._updated_at = value
	}
}

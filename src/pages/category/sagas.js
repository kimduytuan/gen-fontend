import {call, fork, put, takeLatest} from "redux-saga/effects";
import {ADD_CATEGORIES, LOAD_CATEGORIES, REMOVE_CATEGORY, UPDATE_CATEGORY} from "./constants";
import Services from "./service";

function* fetchAreas(action) {
	const {token, page} = action;
	// const currentState = yield select((state => state.areas));
	const posts = yield call(Services.getCategoryByPage, token, page);
	const {data, current_page, last_page, per_page, total} = posts.response;
	yield put({
		type: ADD_CATEGORIES,
		payload: {list: data, total: total, page: current_page, lastPage: last_page, perPage: per_page}
	});

}

function* deleteArea(action) {
	console.log(action)
	yield call(Services.deleteAreaById, action.id);
}

function* updateCategory(action) {
	yield call(Services.updateCategory, action.payload);
}


function* watchFetchAreas() {
	yield takeLatest(LOAD_CATEGORIES, fetchAreas);
}

function* watchDeleteArea() {
	yield takeLatest(REMOVE_CATEGORY, deleteArea);
}

function* watchUpdateArea() {
	yield takeLatest(UPDATE_CATEGORY, updateCategory);
}

export default function* postsSagas() {
	yield fork(watchFetchAreas);
	yield fork(watchDeleteArea);
	yield fork(watchUpdateArea);
}


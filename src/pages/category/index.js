import 'isomorphic-fetch'
import React from 'react'
import jsHttpCookie from 'cookie';
import withRedux from "next-redux-wrapper";
import withSaga from "next-redux-saga";
import Container from '../../components/default/index'
import Pagination from '../../components/pagination'
import Loading from '../../components/loadding'
import {loadCategories, removeCategory, updateCategory} from "./actions"
import {bindActionCreators} from "redux"
import Store from "../../store"
import Panel from "../../components/panel"
import Link from "next/link"
import Router from "next/router"


class Index extends React.Component {

	static getInitialProps({query, req, store, isServer}) {
		const {page} = query;
		if (req && req.headers && isServer) {
			const cookies = req.headers.cookie;
			if (typeof cookies === 'string') {
				const cookiesJSON = jsHttpCookie.parse(cookies);
				store.dispatch(loadCategories(cookiesJSON.token, page));
			}
		} else {
			store.dispatch(loadCategories(null, page));
		}
	}

	_delete = (index) => (e) => {
		e.preventDefault();
		this.props.removeCategory(index)
	};

	_updateStatus = (data) => (e) => {
		e.preventDefault();
		data.status = Number(!data.status)
		this.props.updateCategory(data);
	};

	_setPage = (page) => (e) => {
		e.preventDefault();
		if (page !== null) {
			const {lastPage} = this.props.Categories
			this.props.loadCategories(null, page)
			if (page < lastPage) Router.push(`/category?page=${page}`, `/category?page=${page}`, {shallow: true})
		}
	}

	render() {
		const {loading, list, currentPage, total, lastPage} = this.props.Categories

		return (
			<Container>
				{loading === true ? (
					<div className="row">
						<div className="col-md-12">
							<Panel title="Chuyên mục" action={[{"add": "/category/create"}]}>
								Danh sách chuyên mục
								<table className="table table-sm">
									<thead>
									<tr>
										<th>#</th>
										<th>Ảnh</th>
										<th>Tiêu đề</th>
										<th>Mô tả</th>
										<th>Loại</th>
										<th>Số bài</th>
										<th>Trạng thái</th>
										<th>Hành động</th>
									</tr>
									</thead>
									<tbody>
									{
										list && list.map((item, index) => {
											return (
												<tr key={item.id}>
													<td>{index}</td>
													<td>{item.thumbnail}</td>
													<td>{item.title}</td>
													<td>{item.description}</td>
													<td>{item.type}</td>
													<td>{item.total_article}</td>
													<td><a onClick={this._updateStatus(item)}
																 className={item.status === 1 ? "label label-success" : "label label-danger"}>{item.status === 1 ? "Kích Hoạt" : "Khóa"}</a>
													</td>
													<td>
														<div className="btn-group">
															<Link prefetch href={`/category/edit?id=${item.id}`} as={`/category/edit/${item.id}`}>
																<a className="label label-info">Edit</a>
															</Link>
															<a onClick={this._delete(item.id)}
																 className="label label-danger">Delete</a>
														</div>
													</td>
												</tr>
											)
										})
									}
									</tbody>
								</table>
								<Pagination total={total} currentPage={currentPage} lastPage={lastPage}
														setPage={(e) => this._setPage(e)}/>
							</Panel>
						</div>
					</div>
				) : (
					<Loading/>)}
			</Container>
		)
	}
}

const mapStateToProps = state => ({
	Categories: state.Categories
});

const mapDispatchToProps = dispatch =>
	bindActionCreators({loadCategories, removeCategory, updateCategory}, dispatch);

export default withRedux(Store, mapStateToProps, mapDispatchToProps)(
	withSaga(Index)
);

import {request} from '../../services/request'
import Config from '../../config/index'
import _ from 'lodash'

export default {
	getCategoryByPage: (_token, _page) => {
		// let _data = filterGet({page: _page});
		return request(`${Config.api}/api/category/index?page=${_page}`, null, {
			method: "GET"
		})
	},
	deleteAreaById: (_id) => {
		return request(`${Config.api}/api/category/destroy/${_id}`, null, {
			method: "POST"
		})
	},
	searchCategories: (_token, _keyword) => {
		return request(`${Config.api}/api/category/index?search=${_keyword}&type=Category`, _token, {
			method: "GET"
		})
	},

	getListCate: async (_token, _keyword) => {
		let categories = [];
		return request(`${Config.api}/api/category/index?search=${_keyword}&type=Category`, _token, {
			method: "GET"
		}).then((result) => {
			if (result && result.meta.status === 200 && result.response.data) {
				result = result.response.data;
				result.forEach((item, key) => {
					categories.push({value: item.id, label: item.title});
				})
			}
			return categories;
		})
	},

	createCategory: (_data) => {
		let initial = {
			"title": "string",
			"description": "string",
			"parent_id": null,
			"type": "Category",
			"status": 0,
			"thumbnail": ""
		};

		return request(`${Config.api}/api/category/create`, null, {
			method: "post",
			body: JSON.stringify({...initial, ..._data})
		}).then((data) => {
			if (data.meta.status !== 200) throw new Error(data.meta.message);
			return data;
		})
	},
	updateCategory: async (_data) => {
		// const initial = {
		// 	"parent_id": 0,
		// 	"title": "",
		// 	"description": "",
		// 	"type": "Category",
		// 	"thumbnail": "",
		// };

		const only = ['title', 'description', 'parent_id', 'type', 'thumbnail', 'status']
		const id = _data.id;
		_data = _.pick(_data, only);
		if (id) {
			return request(`${Config.api}/api/category/update/${id}`, null, {
				method: "POST",
				body: JSON.stringify(_data)
			}).then((data) => {
				if (data.meta.status !== 200) throw new Error(data.meta.message);
				return data;
			})
		}
	},

	getCategoryById: (id, token) => {
		return request(`${Config.api}/api/category/show/${id}`, token, {
			method: "GET"
		})
	}
}

import {ADD_CATEGORIES, LOAD_CATEGORIES, REMOVE_CATEGORY, UPDATE_CATEGORY} from './constants'


export function loadCategories(token, page) {
	return {
		type: LOAD_CATEGORIES,
		token: token,
		page: page || 1
	}
}

export function addCategory() {
	return {
		type: ADD_CATEGORIES,
		id: id
	}
}

export function removeCategory(id) {
	return {
		type: REMOVE_CATEGORY,
		id: id
	}
}

export function updateCategory(data) {
	return {
		type: UPDATE_CATEGORY,
		payload: data
	}
}

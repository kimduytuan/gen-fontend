import React from 'react'
import Container from '../../components/default'
import CategoryForm from './form'
import Service from './service'
import jsHttpCookie from 'cookie';

class Index extends React.Component {

	static async getInitialProps({query, req, store}) {
		const {page} = query;
		const isBrowser = typeof window !== 'undefined';
		const isServer = !!req && !isBrowser;
		let category = {};
		if (req && req.headers && isServer) {
			const cookies = req.headers.cookie;
			if (typeof cookies === 'string') {
				const cookiesJSON = jsHttpCookie.parse(cookies);
				category = await Service.getListCate(cookiesJSON.token, "")
			}
		} else {
			category = await Service.getListCate(null, "")
		}

		// if (req && req.headers && isServer) {
		// 	const cookies = req.headers.cookie;
		// 	if (typeof cookies === 'string') {
		// 		const cookiesJSON = jsHttpCookie.parse(cookies);
		// 		store.dispatch(loadCategories(cookiesJSON.token, page));
		// 	}
		// } else {
		// 	store.dispatch(loadCategories(null, page));
		// }
		return {lstCategories: category};
	}


	render() {
		return (
			<Container>
				<CategoryForm title_panel="Tạo chuyên mục" init={{
					title: '',
					description: '',
					type: 'Category',
					lstCategories: this.props.lstCategories,
					status: 0
				}}/>
			</Container>
		)
	}
}

export default Index

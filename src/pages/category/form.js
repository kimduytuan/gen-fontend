import React from 'react'
import {withFormik} from 'formik';
import Yup from 'yup';
import Panel from "../../components/panel/index"
import TextInput from "../../components/forms/Input"
import TextArea from "../../components/forms/TextArea"
import SelectInput from "../../components/forms/Select"
import Service from "./service"
import {notify} from "react-notify-toast"
import Router from "next/router"


const CategoryForm = props => {
	const {
		values,
		touched,
		errors,
		status,
		dirty,
		handleChange,
		handleBlur,
		handleSubmit,
		handleReset,
		isSubmitting,
		setFieldValue,
		setFieldTouched,
		setErrors
	} = props;

	return (

		<div className="row">
			<div className="col-md-6 col-md-push-3">
				<Panel title={props.title_panel} success={!!status && !status.success}
							 error={!!errors.submit} isSubmiting={isSubmitting}>
					<form onSubmit={handleSubmit}>
						<fieldset>
							<legend className="text-semibold">Nhập các thông tin vào ô bên dưới</legend>
							<TextInput
								id="title"
								className="form-control"
								type="text"
								label="Nhập tên chuyên mục:"
								placeholder=""
								error={touched.title && errors.title}
								value={values.title}
								onChange={handleChange}
								onBlur={handleBlur}
							/>

							<TextArea
								id="description"
								className="form-control"
								type="text"
								label="Nhập mô tả:"
								placeholder="Nhập mô tả"
								error={touched.description && errors.description}
								value={values.description}
								onChange={handleChange}
								onBlur={handleBlur}
							/>

							<SelectInput
								id="parent_id"
								value={values.parent_id}
								multi={false}
								onChange={setFieldValue}
								label="Chọn chuyên mục cha"
								placeholder="Chọn chuyên mục cha"
								onBlur={setFieldTouched}
								error={touched.parent_id && errors.parent_id}
								options={values.lstCategories}
								getValue={true}
								selectedValue={values.parent_id}
								touched={touched.parent_id}
							/>

							<SelectInput
								id="type"
								value={values.type}
								multi={false}
								onChange={setFieldValue}
								label="Phân loại chuyên mục: "
								placeholder="Chọn loại chuyên mục"
								onBlur={setFieldTouched}
								error={touched.type && errors.type}
								options={[
									{value: 'Category', label: 'Chuyên mục'},
									{value: 'Tag', label: 'Tag'}
								]}
								getValue={true}
								selectedValue={values.type}
								touched={touched.type}
							/>

							<SelectInput
								id="status"
								value={values.status}
								multi={false}
								onChange={setFieldValue}
								label="Trạng thái:"
								onBlur={setFieldTouched}
								error={touched.status && errors.status}
								options={[
									{value: '1', label: 'Active'},
									{value: '0', label: 'UnActive'}
								]}
								getValue={true}
								selectedValue={values.status}
								touched={touched.status}
							/>
							{
								(values && typeof values.id !== 'undefined' && values.id !== "") ? (
									<input type="hidden" value={values.id}/>) : null
							}
						</fieldset>
						<div className="text-right">
							<button disabled={isSubmitting} type="submit" className="btn btn-primary">Gửi lên<i
								className="icon-arrow-right14 position-right"/></button>
						</div>
					</form>
				</Panel>
			</div>
		</div>
	);
};


const HandleFormCategory = withFormik({
	validationSchema: Yup.object().shape({
		title: Yup.string()
			.min(3, "Tiêu đề phải tối thiểu 3 ký tự.")
			.required('Tiêu đề là bắt buộc.')
	}),
	mapPropsToValues: (props) => (props.init),
	handleSubmit: async (payload, {props, values, setSubmitting, setStatus, resetForm, setErrors}) => {
		// Thông tin đã nhập ở form xem ở đây
		console.log(payload);
		// Xử lý Request Update/Create ở đây
		try {
			if (_.isNumber(payload.id)) {
				await Service.updateCategory(payload);
			} else {
				await Service.createCategory(payload);
			}
			setStatus({success: true})
			resetForm({name: ''})
			Router.back();
		} catch (e) {
			notify.show(e.message || 'Lỗi không xác định!. Vui lòng liên hệ quản trị viên', 'error');
			setStatus({success: false})
			setSubmitting(false);
		}

		//
		// if (response && response.meta.status !== 200) {
		// 	notify.show(response.meta.message, 'error');
		// 	setStatus({success: false})
		// 	setSubmitting(false);
		// } else {
		// 	setStatus({success: true})
		// 	resetForm({name: ''})
		// 	Router.back();
		// }

		// else {
		// 	let response = await Service.createCategory(payload);
		// 	if (response.meta.status !== 200) {
		// 		notify.show(response.meta.message, 'error');
		// 		setStatus({success: false})
		// 		setTimeout(() => setSubmitting(false), 1000);
		// 	} else {
		// 		setStatus({success: true})
		// 		resetForm({name: ''})
		// 		Router.back();
		// 	}
		// }
	},
	displayName: 'CategoryForm',
})(CategoryForm);

export default HandleFormCategory

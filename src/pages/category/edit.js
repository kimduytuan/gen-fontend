import 'isomorphic-fetch'
import React from 'react'
import CategoryService from './service'
import Container from '../../components/default/index'
import Loading from '../../components/loadding'
import CategoryForm from './form'
import jsHttpCookie from "cookie"
import withRedux from "next-redux-wrapper";
import Store from "../../store"

class Index extends React.Component {
	static async getInitialProps({query, req, isServer}) {
		const {id} = query;
		let Category = {};
		let lstCategory = {};
		if (req && req.headers && isServer) {
			const cookies = req.headers.cookie;
			if (typeof cookies === 'string') {
				const cookiesJSON = jsHttpCookie.parse(cookies);
				lstCategory = await CategoryService.getListCate(cookiesJSON.token, "")
				Category = await CategoryService.getCategoryById(id, cookiesJSON.token);
			}
		} else {
			lstCategory = await CategoryService.getListCate(null, "")
			Category = await CategoryService.getCategoryById(id);
		}
		return {Category, lstCategory};
	}


	render() {
		const {Category, lstCategory} = this.props;
		console.log({...Category.response,lstCategories:lstCategory})
		return (
			<Container>
				{Category.meta.status !== 200 ? <Loading/> :
					<CategoryForm title_panel="Sửa chuyên mục" init={{...Category.response,lstCategories:lstCategory}}/>}
			</Container>
		)
	}
}

export default withRedux(Store)(Index);



import {ADD_CATEGORIES, LOAD_CATEGORIES, REMOVE_CATEGORY, UPDATE_CATEGORY} from './constants'

// The initial state of the App
const initialState = {
	loading: false,
	error: false,
	currentPage: 1,
	lastPage: 0,
	total: 0,
	list: [],
	perPage: 0
};
export default function (state = initialState, action) {
	const {type, payload, id} = action
	switch (type) {
		case LOAD_CATEGORIES:
			return {...state, loading: false}
		case ADD_CATEGORIES:
			return {
				...state,
				list: payload.list,
				total: payload.total,
				loading: true,
				currentPage: payload.page,
				lastPage: payload.lastPage
			}
		case REMOVE_CATEGORY:
			return {...state, list: state.list.filter(i => i.id !== id)}
		case UPDATE_CATEGORY:
			return {...state, list: state.list}
		default:
			return state
	}
}

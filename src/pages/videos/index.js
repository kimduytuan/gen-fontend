import React from "react";
import Wraper from "../../components/Shared/wraper";
import Video from "../../components/Components/videos";
import FeaturedVideo from "../../components/Components/featured-videos";
import sevices from "../../services/req_";


export default class VideoClass extends React.Component {
	static async getInitialProps() {
		const video = await sevices.getListVideo();
		const res_video = video && video.meta && video.meta.error !== 200 && video.response ? video.response[0] : [];
		const listVideo = video && video.meta && video.meta.error !== 200 && video.response ? video.response : [];
		const data = res_video ? res_video.video : null;
		const thumbnail = res_video ? res_video.thumbnail : null;
		return {data, thumbnail, listVideo};
	}


	constructor(props) {
		super(props);
		this.pageTitle = "Video";
		this.pageClass = 'video-page';
		this.pageHeader = { title:'Luxury Real Estate Videos',slogan:'A full glimpse into some of our most exclusive properties'};
		this.headerClasses = 'isSticky sticky';
	}

	render() {
		const {data, thumbnail, listVideo} = this.props;
		return (
			<Wraper pageClass={this.pageClass} pageHeader={this.pageHeader} headerClasses={this.headerClasses}>
				<Video data={data} thumbnail={thumbnail}/>
				<FeaturedVideo data = {listVideo}/>
			</Wraper>

		)
	}
}

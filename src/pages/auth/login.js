import React from 'react';
import Service from "../../services/index";
import Router from "next/router";
import "./login.scss";

const Auth = new Service();

class Login extends React.Component {

	static getInitialProps = ({store}) => {
		return {background: Math.floor(Math.random() * 8) + 1}
	}
	handleSubmit = (e) => {
		e.preventDefault()
		console.log(this.refs.email.value)
		// const email = this.refs.email.value;
		// const password = this.refs.password.value;
		const email = "ninhnv@eyeplus.vn";
		const password = "123456789";
		Auth.login(email, password).then((res) => {
			Router.push('/')
			console.log(res);
		}).catch((e) => console.log(e))
	}

	render() {
		return (
			<div className={`login-container bg-slate-800 skin-${this.props.background}`}>
				<div className="page-container">
					<div className="page-content">
						<div className="content-wrapper">
							<div className="content">
								<form onSubmit={this.handleSubmit}>
									<div className="panel panel-body login-form">
										<div className="text-center">
											<div className="icon-object border-warning-400 text-warning-400"><i className="icon-people"/>
											</div>
											<h5 className="content-group-lg">Login to your account <small className="display-block">Enter your
												credentials</small></h5>
										</div>
										<div className="form-group has-feedback has-feedback-left">
											<input type="text" className="form-control" placeholder="Email" ref="email"/>
											<div className="form-control-feedback">
												<i className="icon-user text-muted"/>
											</div>
										</div>
										<div className="form-group has-feedback has-feedback-left">
											<input type="password" className="form-control" placeholder="Password" ref="password"/>
											<div className="form-control-feedback">
												<i className="icon-lock2 text-muted"/>
											</div>
										</div>
										<div className="form-group login-options">
											<div className="row">
												<div className="col-sm-6">
													<label className="checkbox-inline">
														<input type="checkbox" className="styled" defaultChecked="checked"/>
														Remember
													</label>
												</div>
											</div>
										</div>
										<div className="form-group">
											<button type="submit" className="btn bg-blue btn-block">Login <i
												className="icon-circle-right2 position-right"/></button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default Login

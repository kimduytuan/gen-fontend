 import React from "react";
import "isomorphic-fetch";
import withRedux from "next-redux-wrapper";
import service from "../../services/req_";
import withSaga from "next-redux-saga";
import {bindActionCreators} from "redux";
import Store from "../../store";
import {loadPropertiesMap} from "../../containers/_Map/action";
import Wraper from "../../components/Shared/wraperProperties";
import Map from "../../containers/_Map/index";


class MapClass extends React.Component {
	static async getInitialProps({query, req, store, isServer}) {
		const {slug} = query;
		let area = await service.getArea();
		let firstAreaId = area.response && area.response[0].id ? area.response[0].id : '';
		await store.dispatch(loadPropertiesMap(firstAreaId, 1));
		return {slug, area, firstAreaId};
	}
	constructor(props) {
		super(props);
		this.pageClass = 'map-page';
		this.pageTitle = 'Map Properties';
		this.headerClasses = 'isSticky sticky';

	}

	render() {
		let {list, total} = this.props.PropertiesMap;
		let area = this.props.area;
		return (
			<Wraper pageClass={this.pageClass} headerClasses={this.headerClasses} pageTitle={this.pageTitle}>
				<Map data = {list} area = {area} total = {total} self={this}/>
			</Wraper>
		)
	}
}

const mapStateToProps = state => ({
	PropertiesMap: state.PropertiesMap
});

const mapDispatchToProps = dispatch =>
	bindActionCreators({loadPropertiesMap}, dispatch);

export default withRedux(Store, mapStateToProps, mapDispatchToProps)(
	withSaga(MapClass)
);

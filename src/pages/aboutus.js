import 'isomorphic-fetch'
import React from 'react'
import withRedux from 'next-redux-wrapper'
import Link from 'next/link';
import NavBar from '../containers/NavBar'
import SlideBar from '../containers/SlideBar'
import WrapContent from '../containers/WrapContent'

import initStore from '../store'

class AboutUs extends React.Component {
	// static async getInitialProps({store}) {
	// 	// Adding a default/initialState can be done as follows:
	// 	// store.dispatch({ type: 'ADD_TODO', text: 'It works!' });
	// 	const res = await fetch(
	// 		'https://api.github.com/repos/ooade/NextSimpleStarter'
	// 	)
	// 	let json = await res.json()
	// 	console.log(json)
	// 	return {stars: json.stargazers_count}
	// }

	constructor() {
		super();
		this.state = {
			showMenu: false
		}
	}

	handleClickMenu = (e) => {
		e.preventDefault();
		this.setState({showMenu: !this.state.showMenu});
	}

	render() {
		const {stars} = this.props
		return (
			<div className={this.state.showMenu ? "" : "sidebar-xs"}>
				<NavBar handleClick={this.handleClickMenu}/>
				<div className="page-container" style={{minHeight: ' calc(100vh - 48px)'}}>
					<div className="page-content">
						<SlideBar/>
						<WrapContent pro="1"/>
					</div>
				</div>
			</div>
		)
	}
}

export default AboutUs

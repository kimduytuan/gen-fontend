import React from "react";
import Wraper from "../../components/Shared/wraper";
import AssociatesDetail from "../../components/Associates/detail";
import "isomorphic-fetch";
import sevices from "../../services/req_";
import Properties from "../../containers/_Properties/index";

export default class AssociatesDetailClass extends React.Component {
	static async getInitialProps({query}) {
		let sl = query ? query.slug : null;
		console.log(sl);
		let slug = sl ? sl.split("_") : [];
		
		
		if (slug.length >= 1 && slug[0]) {
			console.log('slug', slug[0]);
			const associatesDetail = await sevices.getDetailAssociates(slug[0]);
			console.log(associatesDetail);
			const product = await sevices.getProductAssociates(slug[1]);
			return {associatesDetail, product};
			
		} else return false;
	}

	constructor(){
		super();
		this.pageTitle = "Associates Detail";
		this.pageClass = "associates-page";
		this.headerClasses = "isSticky sticky";
	}

	render() {
		const {associatesDetail, product} = this.props;
		return (
			<Wraper pageClass={this.pageClass} pageHeader={this.pageHeader} headerClasses={this.headerClasses} pageTitle={this.pageTitle}>
				{associatesDetail && associatesDetail.meta && associatesDetail.meta.error !== 200 ? <AssociatesDetail data={associatesDetail.response}/> : null}
				{product && product.meta && product.meta.status === 200 ? <Properties data={product.response} ref={(r) => this.listProperties = r}/> : null}
			</Wraper>
		)
	}
}

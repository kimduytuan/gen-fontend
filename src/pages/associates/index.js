 import React from "react";
import Wraper from "../../components/Shared/wraper";
import Associates from "../../containers/_Associates/index";
import "isomorphic-fetch";
import withRedux from "next-redux-wrapper";
import withSaga from "next-redux-saga";
import {bindActionCreators} from "redux";
import Store from "../../store";
import {loadAssociates} from '../../containers/_Associates/actions';


export class AssociatesClass extends React.Component {

	static getInitialProps({query, req, store, isServer}) {
		store.dispatch(loadAssociates(1));
	}

	constructor(props) {
		super(props);
		this.pageTitle = "Associates";
		this.pageClass = 'associates-page';
		this.pageHeader = {title:'Meet the Hilton & Hyland Team',slogan:'Dedicated Real Estate Professionals'};
		this.headerClasses = 'isSticky sticky';
	}

	render() {
		const {loading, list, currentPage, total} = this.props.Associates;
		return (
			<Wraper pageClass={this.pageClass} pageHeader={this.pageHeader} headerClasses={this.headerClasses}>
				{loading ? <Associates data={list}/> : null}
			</Wraper>
		)
	}
}


const mapStateToProps = state => ({
	Associates: state.Associates
});

const mapDispatchToProps = dispatch =>
	bindActionCreators({loadAssociates}, dispatch);

export default withRedux(Store, mapStateToProps, mapDispatchToProps)(
	withSaga(AssociatesClass)
);

import React from "react";
import WraperProperties from "../../components/Shared/wraper";
import Properties from "../../containers/_Properties/index";
import PropertiesHeaderList from "../../components/Properties/header-list";
import "isomorphic-fetch";
import withRedux from "next-redux-wrapper";
import service from "../../services/req_";
import withSaga from "next-redux-saga";
import {bindActionCreators} from "redux";
import Store from "../../store";
import {loadProperties} from "../../containers/_Properties/actions";

class PropertiesClass extends React.Component {

	static async getInitialProps({query, req, store, isServer}) {
		const {slug} = query;
		store.dispatch(loadProperties(slug, 1));
		let categoryFilter = await service.getCategoryFilter();
		return {slug, categoryFilter};
	}

	render() {
		const {list} = this.props.Properties;
		const categoryFilter = this.props.categoryFilter && this.props.categoryFilter.response ? this.props.categoryFilter.response: [];
		return (
			<WraperProperties pageClass={this.pageClass} pageHeader={this.pageHeader} headerClasses={this.headerClasses} pageTitle={this.pageTitle}>
				<PropertiesHeaderList area = {categoryFilter.area ? categoryFilter.area : []} lifeStyle = {categoryFilter.lifestyle ? categoryFilter.lifestyle : []} self={this}/>
				<Properties data={list} slug={this.props.slug} ref={(r) => this.listProperties = r}/>
			</WraperProperties>
		)
	}
}
const mapStateToProps = state => ({
	Properties: state.Properties
});

const mapDispatchToProps = dispatch =>
	bindActionCreators({loadProperties}, dispatch);

export default withRedux(Store, mapStateToProps, mapDispatchToProps)(
	withSaga(PropertiesClass)
);

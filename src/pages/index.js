import React from "react";
import Head from "next/head";
import Wraper from "../components/Shared/wraper";
import Home from "../containers/_Home/index";
import Services from '../services/req_';

class Index extends React.Component {

	static async getInitialProps({query, req, store, isServer}) {
		const {page} = query;
		const data = await Services.getHome();
		return {data}
	}

	constructor(props) {
		super(props);
		this.pageTitle = "Home";
		this.pageClass = 'home-page';
		this.headerClasses = 'isSticky sticky';
	}

	render() {
		const {data} = this.props;
		console.log(data);
		return (
			<div>Home Page GEN</div>

		)
	}
}

export default Index

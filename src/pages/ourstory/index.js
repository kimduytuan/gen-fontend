import React from "react";
import Wraper from "../../components/Shared/wraper";
import OurStory from "../../containers/_OurStory/index";
import "isomorphic-fetch";
import sevices from "../../services/req_";

export default class OurStoryClass extends React.Component {
	static async getInitialProps() {
		const config = await sevices.getListOurStory();
		return {config};
	}

	constructor(props) {
		super(props);
		this.pageClass = '';
		this.pageHeader = {
			title: 'Our Story',
			type: 'header-background',
			img: 'http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto/v72yei8jjdmwe3fir6zj'
		};
		this.headerClasses = '';
	}

	render() {
		const {config} = this.props;
		return (
			<Wraper pageClass={this.pageClass} pageHeader={this.pageHeader} headerClasses={this.headerClasses}>
				{config && config.meta && config.meta.error !== 200 ? <OurStory data={config.response}/> : null}
			</Wraper>
		)
	}
}


  import React from "react";
import Wraper from "../../components/Shared/wraper";
import PropertiesHeaderDetail from "../../components/Properties/header-detail";
import PropertiesDetail from "../../components/Properties/detail";
import "isomorphic-fetch";
import sevices from "../../services/req_";


export default class PropertiesDetailClass extends React.Component {

	static async getInitialProps({query, req, store, isServer}) {
		const {id} = query;
		const propertiesDetail = await sevices.getDetailProperties(id);
		return {propertiesDetail};
	}

	constructor(){
		super();
		this.pageTitle = "Properties Detail";
	}

	render() {
		const {propertiesDetail} = this.props;
		return (
			<Wraper pageClass={this.pageClass} pageHeader={this.pageHeader} headerClasses={this.headerClasses} pageTitle={this.pageTitle}>
				<PropertiesHeaderDetail data={propertiesDetail.response ? propertiesDetail.response : []}/>
				{propertiesDetail && propertiesDetail.meta && propertiesDetail.meta.error !== 200 ? <PropertiesDetail data={propertiesDetail.response}/> : null}
			</Wraper>
		)
	}
}

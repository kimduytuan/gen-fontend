import 'isomorphic-fetch'
import React from 'react'
import UserService from './service'
import Container from '../../components/default/index'
import UserForm from './form'
import jsHttpCookie from "cookie"
import withRedux from "next-redux-wrapper";
import Store from "../../store"

class Index extends React.Component {
	static async getInitialProps({query, req, isServer}) {
		const {id} = query;
		let user = {};
		if (req && req.headers && isServer) {
			const cookies = req.headers.cookie;
			if (typeof cookies === 'string') {
				const cookiesJSON = jsHttpCookie.parse(cookies);
				user = await UserService.getUserById(id, cookiesJSON.token);
			}
		} else {
			user = await UserService.getUserById(id);
		}
		return {user};
	}


	render() {
		const {user} = this.props;
		console.log(user)
		return (
			<Container>
				{user.meta.status !== 200 ? <UserForm title_panel="Edit User" init={null}/> : <UserForm init={user.response}/>}
			</Container>
		)
	}
}

export default withRedux(Store)(Index);



import {call, fork, put, takeLatest, select} from "redux-saga/effects";
import Services from './service';
import {LOAD_USERS, ADD_USER, REMOVE_USER} from "./constants";

function* fetchPosts(action) {
	const {token, page} = action;
	const currentState = yield select((state => state.users));
	if (!currentState.list[page]) {
		const posts = yield call(Services.getUserByPage, token, page);
		yield put({type: ADD_USER, payload: {list: posts.response, total: posts.meta.total, page: page}});
	} else {
		yield put({type: ADD_USER, payload: {list: currentState.list[page], total: currentState.total, page: page}});
	}
}

function* deleteUser(action) {
	yield call(Services.deleteUserById, action.payload);
}


function* watchFetchUsers() {
	yield takeLatest(LOAD_USERS, fetchPosts);
}

function* watchDeleteUser() {
	yield takeLatest(REMOVE_USER, deleteUser);
}

export default function* postsSagas() {
	yield fork(watchFetchUsers);
	yield fork(watchDeleteUser);
}


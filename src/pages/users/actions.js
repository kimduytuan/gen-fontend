import {LOAD_USERS, ADD_USER, REMOVE_USER} from './constants'


export function loadUsers(token, page) {
	return {
		type: LOAD_USERS,
		token: token,
		page: page || 1,
		action: 1
	}
}

export function addUser(text) {
	return {
		type: ADD_USER,
		payload: id
	}
}

export function removeUser(id) {
	return {
		type: REMOVE_USER,
		payload: id
	}
}

import {LOAD_USERS, ADD_USER, REMOVE_USER} from './constants'

// The initial state of the App
const initialState = {
	loading: false,
	error: false,
	currentPage: 1,
	total: 0,
	list: {}
};

export default function (state = initialState, action) {
	const {type, payload} = action
	switch (type) {
		case LOAD_USERS:
			return {...state}
		case ADD_USER:
			return {
				...state,
				list: {...state.list, [payload.page]: payload.list},
				total: payload.total,
				loading: true,
				currentPage: payload.page
			}
		case REMOVE_USER:
			return {...state, list: state.list.filter(i => i.id !== payload)}
		default:
			return state
	}

}

import React from 'react'
import Container from '../../components/default'
import UserForm from './form'

class Index extends React.Component {
	render() {
		return (
			<Container>
				<UserForm title_panel="Create User" init={{
					username: '', email: ''
				}}/>
			</Container>
		)
	}
}

export default Index

import 'isomorphic-fetch'
import React from 'react'
import jsHttpCookie from 'cookie';
import withRedux from "next-redux-wrapper";
import withSaga from "next-redux-saga";
import Container from '../../components/default/index'

import {loadUsers, removeUser} from "./actions"
import {bindActionCreators} from "redux"
import Store from "../../store"
import Panel from "../../components/panel"
import Link from "next/link"
import Pagination from "../../components/pagination"


class Index extends React.Component {

	static getInitialProps({req, store, isServer}) {
		if (req && req.headers && isServer) {
			console.log('server')
			const cookies = req.headers.cookie;
			if (typeof cookies === 'string') {
				const cookiesJSON = jsHttpCookie.parse(cookies);
				store.dispatch(loadUsers(cookiesJSON.token, 1));
			}
		} else {
			console.log('client')
			store.dispatch(loadUsers(null, 1));
		}
	}


	constructor() {
		super();
		console.log('contructor');
	}

	componentWillMount(){
		console.log('123123123')
	}

	componentDidMount(){
		console.log('did')
	}

	handleClickDelete = (index) => (e) => {
		e.preventDefault();
		this.props.removeUser(index)
	}

	handleSetPage = (page) => (e) => {
		e.preventDefault();
		if (page !== null) {
			this.props.loadUsers(null, page)
		}
	}

	render() {
		const {loading, list, currentPage, total} = this.props.users
		return (
			<Container>
				{loading === true ? (
					<div className="row">
						<div className="col-md-12">
							<Panel title="Small table" action={[{"add": "/users/create"}]}>
							Users List
								<table className="table table-sm">
									<thead>
									<tr>
										<th>#</th>
										<th>FullName</th>
										<th>Email</th>
										<th>Type</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
									</thead>
									<tbody>
									{
										list[currentPage] && list[currentPage].map((item, index) => {
											return (
												<tr key={item.id}>
													<td>{index}</td>
													<td>{item.name}</td>
													<td>{item.email}</td>
													<td><span
														className={item.type === 1 ? "label label-danger" : "label label-success"}>{item.status === 1 ? "admin" : "normal"}</span>
													</td>
													<td><a href="/news/status/"
																 className={item.status === 1 ? "label label-success" : "label label-danger"}>{item.status === 1 ? "Kích Hoạt" : "Khóa"}</a>
													</td>
													<td>
														<div className="btn-group">
															<Link prefetch href={`/users/edit?id=${item.id}`} as={`/users/edit/${item.id}`}>
																<a className="label label-info">Edit</a>
															</Link>
															<a onClick={this.handleClickDelete(item.id)}
																 className="label label-danger">Delete</a>
														</div>
													</td>
												</tr>
											)
										})
									}
									</tbody>
								</table>
								<Pagination total={total} currentPage={currentPage}
														setPage={(e) => this.handleSetPage(e)}/>
							</Panel>
						</div>
					</div>
				) : (
					<a href="">Load failed</a>
				)
				}
			</Container>
		)
	}
}

const mapStateToProps = state => ({
	users: state.users
});

const mapDispatchToProps = dispatch =>
	bindActionCreators({loadUsers, removeUser}, dispatch);

export default withRedux(Store, mapStateToProps, mapDispatchToProps)(
	withSaga(Index)
);

import React from "react";

import Wraper from "../../components/Shared/wraper";
import Blog from "../../containers/_Blog/index";
import "isomorphic-fetch";
import withRedux from "next-redux-wrapper";
import withSaga from "next-redux-saga";
import {bindActionCreators} from "redux";
import Store from "../../store";
import {loadBlog} from "../../containers/_Blog/actions";


export class BlogClass extends React.Component {

	static getInitialProps({query, req, store, isServer}) {
		const {page} = query;
		store.dispatch(loadBlog(page));
	}

	constructor(props) {
		super(props);
		this.pageTitle = "Blogs";
		this.pageClass = '';
		this.pageHeader = { title:'Luxury Casa Real Estate News',slogan:'Our most sought-after residential communities, encapsulating luxury casa and elegance here and abroad'};
		this.headerClasses = 'isSticky sticky';
	}

	render() {
		const {list, currentPage, total} = this.props.Blog;

		return (
			<Wraper pageClass={this.pageClass} pageHeader={this.pageHeader} headerClasses={this.headerClasses} pageTitle={this.pageTitle}>
				<Blog data={list}/>
			</Wraper>

		)
	}
}

const mapStateToProps = state => ({
	Blog: state.Blog
});

const mapDispatchToProps = dispatch =>
	bindActionCreators({loadBlog}, dispatch);

export default withRedux(Store, mapStateToProps, mapDispatchToProps)(
	withSaga(BlogClass)
);



import React from "react";
import Wraper from "../../components/Shared/wraper";
import BlogDetail from "../../components/Blog/detail";
import "isomorphic-fetch";
import sevices from "../../services/req_";


export default class BlogDetailClass extends React.Component {

	static async getInitialProps({query}) {
		const {slug} = query;
		const blogDetail = await sevices.getDetailBlogs(slug);
		const res_blogDetail = blogDetail && blogDetail.meta && blogDetail.meta.error !== 200 && blogDetail.response ? blogDetail.response[0] : [];
		return {res_blogDetail};
	}

	constructor(props) {
		super(props);
		this.pageTitle = "Blogs Detail";
		this.pageClass = '';
		this.pageHeader = {
			title: this.props.res_blogDetail && this.props.res_blogDetail.title ? this.props.res_blogDetail.title : '',
			type: 'header-background',
			img: 'http://res.cloudinary.com/luxuryp/image/upload/q_auto:good,f_auto,w_1600/iqcj1ytbo71hftcgyaas'
		};
		this.headerClasses = 'isSticky sticky';
	}

	render() {
		const {res_blogDetail} = this.props;
		return (
			<Wraper pageClass={this.pageClass} pageHeader={this.pageHeader} headerClasses={this.headerClasses} pageTitle={this.pageTitle}>
				{res_blogDetail ?	<BlogDetail data={res_blogDetail}/> : null}
			</Wraper>
		)
	}
}

FROM node:10.11-alpine

RUN apk add --no-cache libc6-compat
ENV NODE_ENV production
ENV PORT 3000
EXPOSE 3000

WORKDIR /home/source/web
RUN npm cache clean --force \
  && npm install

CMD [ "npm", "start" ]
